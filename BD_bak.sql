-- phpMyAdmin SQL Dump
-- version 4.0.0-beta3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2013 at 09:41 
-- Server version: 5.0.45-community-nt
-- PHP Version: 5.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bdvasoleche`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_BENEFICIARIO`(
  p_ncodigo int(10)
)
update beneficiario set cBenEstado = CASE WHEN
cBenEstado='A' then 'I' else 'A' end
where nBenID=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_CLUB`(
  p_ncodigo int(10)
)
update club set cCluEstado = CASE WHEN
cCluEstado='A' then 'I' else 'A' end
where nCluId=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_COMITE`(
  p_ncodigo int(10)
)
update comite set ccomestado = CASE WHEN
ccomestado='A' then 'I' else 'A' end
where ncomid=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_DESPACHO`(
  p_ncodigo int(10)
)
update despacho set nDesEstado = CASE WHEN
nDesEstado='1' then '0' else '1' end
where nDesId=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_DetalleDespacho`(
  p_ncodigo int(10)
)
update detalledespacho set nDetEstado = CASE WHEN
nDetEstado='1' then '0' else '1' end
where nDetDesId=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_INSUMO`(
  p_ncodigo int(10)
)
update insumos set cImsEstado = CASE WHEN
cImsEstado='1' then '0' else '1' end
where nImsId=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_D_PERSONA`(
  p_ncodigo int(10)
)
update persona set cPerEstado = CASE WHEN
cPerEstado='1' then '0' else '1' end
where nPerId=p_ncodigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_I_BENEFICIARIO`(
	-- IN nPerId int(11),
	-- IN v_cBenPartida varchar(50),
	IN v_nBenBeneficiario int,
	IN v_nSexo char(1),
	IN v_cFechaNacimiento char(20),
	IN n_nPerIdMadre int,
	IN n_nClubId int
	-- IN v_cPerEstado char(1)
	-- IN v_tPerFechaRegistro timestamp
	-- IN tPerFechaBaja timestamp,
-- cBenPartidaNacimiento, persona_nPerId, cBenSexo, cBenFechaNacimiento, cBenEstado, nPerIdMadre, nclubid
)
BEGIN
	/*INSERT INTO beneficiario(cBenPartidaNacimiento, persona_nPerId, cBenSexo, cBenFechaNacimiento, cBenEstado, nPerIdMadre, nclubid)
	VALUES(v_cBenPartida, v_nBenBeneficiario, v_nSexo, v_cFechaNacimiento, 'A', n_nPerIdMadre, n_nClubId);*/
  INSERT INTO beneficiario(persona_nPerId, cBenSexo, cBenFechaNacimiento, cBenEstado, nPerIdMadre, nclubid)
	VALUES(v_nBenBeneficiario, v_nSexo, v_cFechaNacimiento, 'A', n_nPerIdMadre, n_nClubId);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_I_Club`(
	IN v_cCluNombre varchar(150) ,
	IN v_cCluDireccion varchar(200) ,
	IN v_cCluTelefono varchar(15) ,
	IN v_nCluCantBeneficiados int(11) ,
	IN v_nCluCantidadLeche int(11) ,
	IN v_nCluCantidadAvena int(11) 
)
BEGIN
  INSERT INTO CLUB (cCluNombre, cCluDireccion, cCluTelefono, nCluCantBeneficiados, nCluCantidadLeche, nCluCantidadAvena)
  VALUES (v_cCluNombre, v_cCluDireccion, v_cCluTelefono, v_nCluCantBeneficiados, v_nCluCantidadLeche, v_nCluCantidadAvena);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_I_Comite`(
	IN v_cComTipo   varchar(2) ,
	IN v_nCluId int ,
	IN v_nPerID int
)
BEGIN
  INSERT INTO COMITE (cComTipo, nCluId, nPerId, cComEstado)
  VALUES (v_cComTipo, v_nCluId, v_nPerID, 'A');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_I_DESPACHO`(
	IN v_nDesResponsable int,
  IN v_nDesClub int,
  IN v_cDesMes varchar(45),
  IN V_cDesAnio varchar(45)
)
BEGIN
/*A = activo*/

INSERT INTO DESPACHO(nDesResponsable,nCluId,cDesMes,cDesAnio,tDesFechaRegistro,nDesEstado)
VALUES(v_nDesResponsable,v_nDesClub,v_cDesMes,V_cDesAnio,now(),'A');


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_I_INSUMO`(
	IN v_cImsNombre varchar(150) ,
	IN v_nImsUnidadMedida varchar(50) ,
	IN v_tImsFechaVencimiento varchar(20) ,
	IN v_cImsDescripcion varchar(250) ,
	IN v_nImsCantidad int(11)
)
BEGIN


INSERT INTO INSUMOS(cImsNombre,nImsUnidadMedida,tImsFechaRegistro,tImsFechaVencimiento,cImsDescripcion,nImsCantidad,cImsEstado)
VALUES(v_cImsNombre,v_nImsUnidadMedida,now(),v_tImsFechaVencimiento,v_cImsDescripcion,v_nImsCantidad,1);


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_I_PERSONA`(
	-- IN nPerId int(11),
	IN v_cPerNombres varchar(50),
	IN v_cPerApellidoPaterno varchar(50),
	IN v_cPerApellidoMaterno varchar(50),
	IN v_cPerDni char(8),
	IN v_cPerDireccion varchar(90),
	IN v_cPerTelefono varchar(20),
	IN v_cPerCelular varchar(11)
	-- IN v_cPerEstado char(1)
	-- IN v_tPerFechaRegistro timestamp
	-- IN tPerFechaBaja timestamp,
)
BEGIN
	INSERT INTO persona(cPerNombres, cPerApellidoPaterno, cPerApellidoMaterno, cPerDni, cPerDireccion, cPerTelefono, cPerCelular,cPerEstado)
	VALUES( v_cPerNombres,v_cPerApellidoPaterno,v_cPerApellidoMaterno,v_cPerDni,v_cPerDireccion,v_cPerTelefono,v_cPerCelular,'1');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_BENEFICIARIO`()
select b.*,concat(p.cPerNombres,' ',p.cPerApellidoPaterno)datosbeneficiado,p.cPerDni dnibeneficiado,
concat(ma.cPerNombres,' ',ma.cPerApellidoPaterno)datosMadre,
c.cCluNombre datosClub,
substring(b.cBenFechaNacimiento,1,2)as dia,
substring(b.cBenFechaNacimiento,4,2)as mes,
substring(b.cBenFechaNacimiento,7,4)as anio
,
b.cBenEstado as estadoben/*,
(year(now())-YEAR(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))*12
- (month(now())-month(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))
as edad*/

from beneficiario b
inner join persona p on p.nPerid=b.persona_nPerId
inner join persona ma on ma.nPerid=b.nPerIdMadre
inner join club c on b.nclubid=c.nCluId$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_BENEFICIARIOS_APTOS`()
select
substring(b.cBenFechaNacimiento,1,2)as dia,
substring(b.cBenFechaNacimiento,4,2)as mes,
substring(b.cBenFechaNacimiento,7,4)as anio
,
b.cBenEstado as estadoben/*,
(year(now())-YEAR(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))*12
- (month(now())-month(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))
as edad*/

from beneficiario b
where cBenEstado='A'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_BENEFICIARIO_GET`(
IN v_idben int)
begin
select * from beneficiario where nBenId=v_idben;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_BENEFICIARIO_LIMITE_EDAD`()
begin

declare totmeses integer;
declare cantanios integer;
declare cantmeses integer;

set totmeses=12*8;
/*set cantanios=select (year(now())-YEAR(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))*12 from beneficiario;*/



select b.cBenFechaNacimiento,now(),
(year(now())-YEAR(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))*12
- (month(now())-month(STR_TO_DATE(replace(b.cBenFechaNacimiento,'/',','),'%d,%m,%Y')))
 as tienemeses,
totmeses

from beneficiario b
inner join persona p on p.nPerid=b.persona_nPerId
inner join persona ma on ma.nPerid=b.nPerIdMadre
inner join club c on b.nclubid=c.nCluId;

end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_CLUB`()
select * from club
order by nCluId desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_CLUB_GET`(
IN v_idclub int)
begin
select * from club where nCluId=v_idclub;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_COMITE`()
select co.ncomid,co.ccomtipo,co.ncluid,co.nperid,co.ccomestado,cu.cclunombre, mu.cMulDescripcion as tipocomite,
concat(p.cPerNombres,' ',p.cPerApellidoPaterno,' ', p.cPerApellidoMaterno) as datospersona
from comite co
inner join club cu on co.ncluid=cu.ncluid
inner join multitabla mu on mu.nMulIdPadre=co.ccomtipo and nMulOrden =1 and nMulEstado='A'
inner join persona p on p.nperid=co.nperid
order by ncomid desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_COMITE_GET`(
IN v_idcomit int)
begin
select * from comite
where nComId=v_idcomit;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_Despacho`()
select
d.nDesId,
concat(p.cPerNombres,' ',p.cPerApellidoPaterno,' ', p.cPerApellidoMaterno) as datospersona,
cu.cCluNombre,d.tDesFechaRegistro,d.nDesEstado,
d.cDesMes,
d.cDesAnio
from despacho d
inner join club cu on d.nCluId=cu.ncluid
inner join persona p on p.nperid=d.nDesResponsable
order by d.tDesFechaRegistro desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_DESPACHO_GET`(
IN v_iddespacho int)
begin
/*select * from despacho where nDesId=v_iddespacho;*/
select
d.nDesId,
concat(p.cPerNombres,' ',p.cPerApellidoPaterno,' ', p.cPerApellidoMaterno) as datospersona,
cu.cCluNombre,d.tDesFechaRegistro,d.nDesEstado,
d.cDesMes,
d.cDesAnio
from despacho d
inner join club cu on d.nCluId=cu.ncluid
inner join persona p on p.nperid=d.nDesResponsable
where nDesId=v_iddespacho;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_DETALLEDESPACHO_cantant_GET`(
IN v_nDetDesId int
)
select
d.nDetcantidad as stockanterior

from detalledespacho d
where d.nDetDesId=v_nDetDesId$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_DetalleDespacho_GET`(
IN v_nDesID int
)
select Date_format(d.tDetFechaRegistro,'%d/%m/%Y') as fechaDetalle,
d.nDetcantidad, d.cDetdescripcion,
i.cImsNombre as nombreProducto,
d.nDetEstado,
d.nDetDesId,
i.nImsCantidad as stock

from detalledespacho d
inner join insumos i on i.nImsId=d.nImsId
where d.nDesId=v_nDesID
order by d.nDetDesId asc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_DetalleDespacho_GET_datos`(
IN v_nDetDesId int
)
select Date_format(d.tDetFechaRegistro,'%d/%m/%Y') as fechaDetalle,
d.nDesId as iddespacho,
d.nDetcantidad, d.cDetdescripcion,
i.nImsId as idinsumo,
i.cImsNombre as nombreProducto,
d.nDetEstado,
d.nDetDesId,
i.nImsCantidad as stock

from detalledespacho d
inner join insumos i on i.nImsId=d.nImsId
where d.nDetDesId=v_nDetDesId$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_INSUMO`()
select * from insumos
order by nImsId desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_INSUMO_GET`(
IN v_idinsumo int)
begin
select * from insumos where nImsId=v_idinsumo;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_PERSONAS`()
select *, concat(p.cPerNombres,' ',p.cPerApellidoPaterno,' ', p.cPerApellidoMaterno) as datospersona
from persona p
order by nperid desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_PERSONAS_ACTIVAS`()
select *, concat(p.cPerNombres,' ',p.cPerApellidoPaterno,' ', p.cPerApellidoMaterno) as datospersona
from persona p
where cPerEstado='1'
order by nperid desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_S_PERSONA_GET`(
IN v_idpersona int)
begin
select * from persona where nPerId=v_idpersona;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_BENEFICIARIO`(
	IN v_nBenBeneficiario int,
	IN v_nSexo char(1),
	IN v_cFechaNacimiento char(20),
	IN v_nPerIdMadre int,
	IN v_nClubId int,
  IN v_nBenid int(11)
)
BEGIN
  UPDATE beneficiario
  set
  persona_nPerId=v_nBenBeneficiario,
  cBenSexo=v_nSexo,
  cBenFechaNacimiento=v_cFechaNacimiento,
  nPerIdMadre=v_nPerIdMadre,
  nclubid=v_nClubId
 where nBenId=v_nBenid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_CLUB`(
	IN v_cCluNombre varchar(150) ,
	IN v_cCluDireccion varchar(200) ,
	IN v_cCluTelefono varchar(15) ,
	IN v_nCluCantBeneficiados int(11) ,
	IN v_nCluCantidadLeche int(11) ,
	IN v_nCluCantidadAvena int(11),
  IN v_nCluid int(11)
)
BEGIN
  UPDATE CLUB
  set cCluNombre=v_cCluNombre,
  cCluDireccion=v_cCluDireccion,
  cCluTelefono=v_cCluTelefono,
  nCluCantBeneficiados=v_nCluCantBeneficiados,
  nCluCantidadLeche=v_nCluCantidadLeche,
  nCluCantidadAvena=v_nCluCantidadAvena
 where nCluId=v_nCluid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_COMITE`(
	IN v_cComTipo char(2) ,
	IN v_nCluId int ,
  IN v_nPerId int ,
  IN v_nComId int
)
BEGIN
  UPDATE comite
  set cComTipo=v_cComTipo,
  nCluId=v_nCluId,
  nPerId=v_nPerId
 where nComId=v_nComId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_DESPACHO`(
  IN V_nDesId int,
	IN v_nDesResponsable int,
  IN v_nDesClub int,
  IN v_cDesMes varchar(45),
  IN V_cDesAnio varchar(45)

)
BEGIN
  UPDATE DESPACHO
  set nDesResponsable=v_nDesResponsable,
  nCluId=v_nDesClub,
  cDesMes=v_cDesMes,
  cDesAnio=V_cDesAnio
where
  nDesId=V_nDesId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_INSUMO`(
  IN v_nImsId int,
	IN v_cImsNombre varchar(150) ,
	IN v_nImsUnidadMedida varchar(50) ,
	IN v_tImsFechaVencimiento varchar(20) ,
	IN v_cImsDescripcion varchar(250) ,
	IN v_nImsCantidad int(11)

)
BEGIN
  UPDATE INSUMOS
  set cImsNombre=v_cImsNombre,
  nImsUnidadMedida=v_nImsUnidadMedida,
  tImsFechaVencimiento=v_tImsFechaVencimiento,
  cImsDescripcion=v_cImsDescripcion,
  nImsCantidad=v_nImsCantidad
where
  nImsId=v_nImsId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_INSUMO_STOCK`(
  IN v_nImsId int,
	IN v_nImsCantidad int(11)
)
BEGIN
  UPDATE INSUMOS
set  nImsCantidad=nImsCantidad-v_nImsCantidad
where
  nImsId=v_nImsId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_INSUMO_STOCK2`(
  IN v_nImsId int,
	IN v_nImsCantidad int(11),
  IN v_nDetidDespacho int(11),
  IN v_cDetObservacion varchar(250)
)
BEGIN
  UPDATE INSUMOS
set  nImsCantidad=nImsCantidad-v_nImsCantidad
where
  nImsId=v_nImsId;

  INSERT into detalledespacho(nImsId,nDesId,tDetFechaRegistro, nDetEstado, cDetdescripcion,nDetcantidad)
  values(v_nImsId,v_nDetidDespacho,now(),'1',v_cDetObservacion,v_nImsCantidad);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_INSUMO_STOCKxDetDespacho`(
  IN v_nImsId int,
  IN v_diferencia int(11),
	IN v_nImsCantidad int(11),
  IN v_nDetidDespacho int(11),
  IN v_cDetObservacion varchar(250)
)
BEGIN
  UPDATE INSUMOS
set  nImsCantidad=nImsCantidad-v_diferencia
where
  nImsId=v_nImsId;

  /*INSERT into detalledespacho(nImsId,nDesId,tDetFechaRegistro, nDetEstado, cDetdescripcion,nDetcantidad)
  values(v_nImsId,v_nDetidDespacho,now(),'1',v_cDetObservacion,v_nImsCantidad);*/
  update detalledespacho
  set nImsId=v_nImsId,
  cDetdescripcion=v_cDetObservacion,
  nDetcantidad=v_nImsCantidad,
  tDetFechaRegistro=now()
  where nDetDesId=v_nDetidDespacho;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_CLU_U_PERSONA`(
	IN v_cPerNombres varchar(50),
	IN v_cPerApellidoPaterno varchar(50),
	IN v_cPerApellidoMaterno varchar(50),
	IN v_cPerDni char(8),
	IN v_cPerDireccion varchar(90),
	IN v_cPerTelefono varchar(20),
	IN v_cPerCelular varchar(11),
  IN v_hdnPerId int
)
BEGIN
  UPDATE PERSONA
  set cPerNombres=v_cPerNombres,
  cPerApellidoPaterno=v_cPerApellidoPaterno,
  cPerApellidoMaterno=v_cPerApellidoMaterno,
  cPerDni=v_cPerDni,
  cPerDireccion=v_cPerDireccion,
  cPerTelefono=v_cPerTelefono,
  cPerCelular=v_cPerCelular
 where nPerId=v_hdnPerId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_IS_CRONOGRAMA`(
IN `p_nUsuCodigo` INT, IN `p_nImsId` INT,OUT p_cronoId INT
)
BEGIN 
    INSERT INTO cronograma(nUsuCodigo, nCroTotal, nImsId)
    VALUES(p_nUsuCodigo, 0, p_nImsId);
    SET p_cronoId = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_I_DETALLECRONOGRAMA`(
IN p_nCrodId INT ,IN p_tDetCroAnio INT ,IN p_tDetCroMes INT, 
IN p_tDetCroDia INT
)
BEGIN
    INSERT INTO detallecronograma(nDetCroId,nCroId,tDetCroAnio,tDetCroMes,tDetCroDia)
    VALUES(NULL,p_nCrodId,p_tDetCroAnio,p_tDetCroMes,p_tDetCroDia);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_PER_I`(
	-- IN nPerId int(11),
	IN v_cPerNombres varchar(50),
	IN v_cPerApellidoPaterno varchar(50),
	IN v_cPerApellidoMaterno varchar(50),
	IN v_cPerDni char(8),
	IN v_cPerDireccion varchar(90),
	IN v_cPerTelefono varchar(20),
	IN v_cPerCelular varchar(11)
	-- IN v_cPerEstado char(1)
	-- IN v_tPerFechaRegistro timestamp
	-- IN tPerFechaBaja timestamp,
)
BEGIN
	INSERT INTO persona(cPerNombres, cPerApellidoPaterno, cPerApellidoMaterno, cPerDni, cPerDireccion, cPerTelefono, cPerCelular)
	VALUES( v_cPerNombres,v_cPerApellidoPaterno,v_cPerApellidoMaterno,v_cPerDni,v_cPerDireccion,v_cPerTelefono,v_cPerCelular);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_PER_S`(
IN p_tipo varchar(5)
)
BEGIN
IF p_tipo='LPU' THEN -- Lista Personas Con Usuarios 
    SELECT p.nPerId,u.nUsuCodigo,p.cPerNombres,p.cPerApellidoPaterno,
        p.cPerApellidoMaterno,p.cPerDni
    from PERSONA p 
        INNER JOIN Usuario u ON p.nPerId = u.nPerId 
    WHERE p.cPerEstado ='1' and u.cUsuEstado = '1';
ELSE
    SELECT nPerId,cPerNombres,cPerApellidoPaterno,cPerApellidoMaterno,cPerDni,
    cPerDireccion,cPerTelefono,cPerCelular,cPerEstado from PERSONA;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_PER_S_dni`(
IN v_cPerDni varchar(8) 
)
BEGIN
    SELECT 
        p.nPerId,p.cPerNombres,p.cPerApellidoPaterno,p.cPerApellidoMaterno, p.cPerDni 
    FROM persona p where p.cPerDni =v_cPerDni and nPerId NOT IN (select nPerId from usuario) ;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `usp_pruebas_v`()
BEGIN   
  declare es int default 1;
  CREATE TEMPORARY TABLE deb (idx INT );
  while es < 12 do
    insert into deb(idx)values(es);
  end while;
  select * from deb;
  DROP TABLE deb;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_S_COMBOS_MULTITABLA`(
    IN v_nMulIdPadre int,
    IN v_Tipo VARCHAR(10)
)
BEGIN
    IF v_Tipo = 'LTU' THEN -- Lista Tipos de Usuarios
        select nMulId as codx,cMulDescripcion as dato from multitabla where nMulIdPadre = v_nMulIdPadre;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_S_CRONOGRAMAS`()
BEGIN
select
   c.nCroId,
   c.nImsId,
   c.tCroFechaRegistro,
   c.nCroTotal,
   c.cCroEstado,
   i.cImsNombre 
from
   cronograma c 
inner join
   insumos i 
      ON c.nImsId=i.nImsId 
    ORDER BY 1 DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_S_INSUMOS_CBO`()
BEGIN
SELECT
   nImsId,
   cImsNombre 
FROM
   insumos   
WHERE
   cImsEstado = 1 
   AND nImsId not in(
      SELECT
         nImsId 
      FROM
         cronograma 
      WHERE
         YEAR(tCroFechaRegistro)=YEAR(now())
    );
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_S_MEN`(
    IN p_opt VARCHAR(3),
    IN p_criterio INT
)
BEGIN
    IF p_opt='LXM' THEN -- Lista Menus de Modulo
        SELECT nMenId,cMenMenu,cMenUrl,cMenOrden FROM menu where nModId = p_criterio AND cMenActivo = 0 ;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_S_MOD`()
BEGIN
    SELECT nModId,cModModulo,nModOrden,cModIcono FROM MODULO ORDER BY nModOrden;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_S_PERMISOS`(
IN p_nUsuCodigo INT
)
BEGIN
    select nMenId from permiso where nUsuCodigo=p_nUsuCodigo AND cPermActivo=1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `USP_USU_I_REGISTRAR`(
IN v_nPerId INT,
IN v_cUsuUsuario varchar(100),
IN v_cUsuClave varchar(100),
IN v_cUsuTipo char(1)
)
BEGIN
-- IF v_UsuTipo

INSERT INTO `bdvasoleche`.`usuario`
(
`nPerId`,
`cUsuUsuario`,
`cUsuClave`,
`cUsuEstado`,
`cUsuTipo`
)
VALUES
(
v_nPerId,
v_cUsuUsuario,
v_cUsuClave,
'1',
v_cUsuTipo
);


END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `beneficiario`
--

CREATE TABLE IF NOT EXISTS `beneficiario` (
  `nBenId` int(11) NOT NULL auto_increment,
  `cBenPartidaNacimiento` varchar(200) collate utf8_spanish_ci default NULL,
  `persona_nPerId` int(11) NOT NULL,
  `cBenSexo` char(1) collate utf8_spanish_ci default NULL,
  `cBenFechaNacimiento` varchar(20) collate utf8_spanish_ci default NULL,
  `cBenEstado` char(1) collate utf8_spanish_ci default 'A',
  `nPerIdMadre` int(11) NOT NULL,
  `nclubid` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`nBenId`),
  KEY `fk_Beneficiario_persona1` (`persona_nPerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `beneficiario`
--

INSERT INTO `beneficiario` (`nBenId`, `cBenPartidaNacimiento`, `persona_nPerId`, `cBenSexo`, `cBenFechaNacimiento`, `cBenEstado`, `nPerIdMadre`, `nclubid`) VALUES
(14, NULL, 10, 'M', '08/04/2005', 'A', 9, 4),
(15, NULL, 2, 'M', '16/01/2013', 'A', 4, 5),
(16, NULL, 4, 'F', '20/06/1963', 'A', 9, 4);

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE IF NOT EXISTS `club` (
  `nCluId` int(11) NOT NULL auto_increment,
  `cCluNombre` varchar(150) collate utf8_spanish_ci NOT NULL,
  `cCluDireccion` varchar(200) collate utf8_spanish_ci NOT NULL,
  `cCluTelefono` varchar(15) collate utf8_spanish_ci default NULL,
  `nCluCantBeneficiados` int(11) NOT NULL,
  `tCluFechaRegistro` timestamp NULL default CURRENT_TIMESTAMP,
  `cCluEstado` char(2) collate utf8_spanish_ci NOT NULL default 'A',
  `tCluFechaCese` timestamp NULL default NULL,
  `nCluCantidadLeche` int(11) default NULL,
  `nCluCantidadAvena` int(11) default NULL,
  PRIMARY KEY  (`nCluId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`nCluId`, `cCluNombre`, `cCluDireccion`, `cCluTelefono`, `nCluCantBeneficiados`, `tCluFechaRegistro`, `cCluEstado`, `tCluFechaCese`, `nCluCantidadLeche`, `nCluCantidadAvena`) VALUES
(1, 'Jerusalen', 'Pasaje Sanz', '044251224', 55, '2013-05-03 02:56:42', 'A', NULL, 584, 2147483647),
(2, 'Corazon Sagrado', 'Pasaje Sanz2', '044251224', 55, '2013-05-03 21:26:32', 'A', NULL, 584, 2648),
(3, 'Corazon Bendito', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:35:49', 'I', NULL, 584, 26489),
(4, 'Todopoderoso', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:35:57', 'I', NULL, 584, 2648),
(5, 'Juan Bautista', 'Calle Ancash', '04411223', 77, '2013-05-06 23:36:01', 'A', NULL, 450, 368),
(6, 'Teresa', 'Jiron union 231', '044154874', 33, '2013-05-06 23:36:03', 'A', NULL, 666, 7878),
(7, 'Juan pablo II', 'Pasaje Condorcanqui', '044251411', 55, '2013-05-06 23:36:06', 'I', NULL, 584, 115),
(8, 'Miguel Angel', 'Pasaje Humala', '9858582838', 777, '2013-05-06 23:36:08', 'A', NULL, 111, 222),
(9, 'San Martin de Porres', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:36:52', 'A', NULL, 584, 2648),
(10, 'Maria Teresa', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:36:55', 'A', NULL, 584, 2648),
(11, 'Por todos', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:36:58', 'A', NULL, 584, 2648),
(12, 'Fe y Caridad', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:37:00', 'A', NULL, 584, 2648),
(13, 'Esperanza', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:34', 'A', NULL, 584, 2648),
(14, 'Victoria', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:37', 'A', NULL, 584, 2648),
(15, 'Siempre Contigo', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:39', 'A', NULL, 584, 2648),
(16, 'Abelardo Gamarra', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:42', 'A', NULL, 584, 2648),
(17, 'Ricardo Palma', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:44', 'A', NULL, 584, 2648),
(18, 'El Cantor', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:47', 'A', NULL, 584, 2648),
(19, 'Amauta', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:49', 'A', NULL, 584, 2648),
(20, 'Cesar Vallejo', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:51', 'A', NULL, 584, 2648),
(21, 'Milagros', 'Pasaje Sanz', '044251224', 55, '2013-05-06 23:51:53', 'A', NULL, 584, 2648),
(22, 'Pura fe', 'Payan 44112312', '0344234', 222, '2013-05-07 22:53:54', 'A', NULL, 422, 434),
(23, 'Recke', 'Inambari 3333', '04425178', 555, '2013-05-07 23:01:12', 'A', NULL, 454554, 89898),
(24, 'Milagros Saez', 'Pasaje Condorcanqui', '04425178', 222, '2013-05-16 03:51:54', 'I', NULL, 44, 222),
(25, 'San benitow', 'huamachuco 5553', '044261232', 2222, '2013-05-16 22:26:38', 'I', NULL, 2222, 111),
(26, 'Pedro Nolasco', 'Pasaje olaya 123', '044281411', 88, '2013-05-17 21:12:14', 'A', NULL, 88888, 25000);

-- --------------------------------------------------------

--
-- Table structure for table `comite`
--

CREATE TABLE IF NOT EXISTS `comite` (
  `nComId` int(11) NOT NULL auto_increment,
  `cComTipo` char(2) collate utf8_spanish_ci default NULL,
  `nCluId` int(11) NOT NULL,
  `nPerId` int(11) NOT NULL,
  `cComEstado` char(2) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`nComId`),
  KEY `fk_comite_club1` (`nCluId`),
  KEY `fk_comite_persona1` (`nPerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `comite`
--

INSERT INTO `comite` (`nComId`, `cComTipo`, `nCluId`, `nPerId`, `cComEstado`) VALUES
(1, '3', 15, 1, 'A'),
(2, '3', 5, 1, 'A'),
(3, '3', 5, 2, 'A'),
(4, '2', 5, 1, 'A'),
(5, '3', 5, 2, 'I'),
(6, '2', 11, 1, 'I'),
(7, '2', 26, 10, 'A'),
(8, '3', 26, 5, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `cronograma`
--

CREATE TABLE IF NOT EXISTS `cronograma` (
  `nCroId` int(11) NOT NULL auto_increment,
  `nUsuCodigo` int(11) NOT NULL,
  `tCroFechaRegistro` timestamp NULL default CURRENT_TIMESTAMP,
  `nCroTotal` int(11) default NULL,
  `cCroEstado` char(1) collate utf8_spanish_ci default 'A',
  `nImsId` int(11) NOT NULL,
  PRIMARY KEY  (`nCroId`),
  KEY `fk_nUsuId` (`nUsuCodigo`),
  KEY `fk_nImsId` (`nImsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=60 ;

--
-- Dumping data for table `cronograma`
--

INSERT INTO `cronograma` (`nCroId`, `nUsuCodigo`, `tCroFechaRegistro`, `nCroTotal`, `cCroEstado`, `nImsId`) VALUES
(33, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(34, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(35, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(36, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(37, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(38, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(39, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(40, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(41, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(42, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(43, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(44, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(45, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(46, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(47, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(48, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(49, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(50, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(51, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(52, 1, '2012-06-13 16:24:34', 0, 'A', 3),
(53, 1, '2012-06-13 16:24:34', 0, 'A', 1),
(54, 1, '2012-06-13 16:24:34', 0, 'A', 1),
(55, 1, '2012-06-13 16:24:34', 0, 'A', 1),
(56, 1, '2012-06-13 16:24:34', 0, 'A', 2),
(57, 1, '2013-06-13 22:50:50', 0, 'A', 2),
(58, 1, '2013-06-13 22:53:27', 0, 'A', 1),
(59, 1, '2013-06-13 22:58:34', 0, 'A', 4);

-- --------------------------------------------------------

--
-- Table structure for table `despacho`
--

CREATE TABLE IF NOT EXISTS `despacho` (
  `nDesId` int(11) NOT NULL auto_increment,
  `nDesResponsable` int(11) default NULL,
  `nDesDuracion` int(11) default NULL,
  `nDesEstado` char(1) collate utf8_spanish_ci default NULL,
  `tDesFechaRegistro` timestamp NULL default NULL,
  `nCluId` int(11) NOT NULL,
  `cDesMes` varchar(45) collate utf8_spanish_ci NOT NULL,
  `cDesAnio` varchar(45) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`nDesId`),
  KEY `fk_nCluId` (`nCluId`),
  KEY `fk_nDetCro` (`nDesId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `despacho`
--

INSERT INTO `despacho` (`nDesId`, `nDesResponsable`, `nDesDuracion`, `nDesEstado`, `tDesFechaRegistro`, `nCluId`, `cDesMes`, `cDesAnio`) VALUES
(1, 6, NULL, '1', '2013-06-13 21:29:30', 3, 'Agosto', '2013');

-- --------------------------------------------------------

--
-- Table structure for table `detallecronograma`
--

CREATE TABLE IF NOT EXISTS `detallecronograma` (
  `nDetCroId` int(11) NOT NULL auto_increment,
  `nCroId` int(11) NOT NULL,
  `tDetCroAnio` int(11) default NULL,
  `tDetCroMes` int(11) default NULL,
  `tDetCroDia` int(11) default NULL,
  `nDetMesCantidadMes` int(11) default '0',
  PRIMARY KEY  (`nDetCroId`),
  KEY `fk_nCroId` (`nCroId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=366 ;

--
-- Dumping data for table `detallecronograma`
--

INSERT INTO `detallecronograma` (`nDetCroId`, `nCroId`, `tDetCroAnio`, `tDetCroMes`, `tDetCroDia`, `nDetMesCantidadMes`) VALUES
(18, 33, 2013, 6, 18, 90),
(19, 33, 2013, 8, 23, 27),
(20, 33, 2013, 9, 13, 55),
(21, 33, 2013, 6, 13, 233),
(22, 33, 2013, 11, 13, 55),
(23, 33, 2013, 12, 13, 55),
(24, 33, 2014, 1, 13, 55),
(25, 33, 2014, 2, 13, 55),
(26, 33, 2014, 3, 13, 55),
(27, 33, 2014, 4, 13, 55),
(28, 33, 2014, 5, 13, 55),
(29, 33, 2014, 6, 13, 55),
(30, 33, 2014, 7, 13, 55),
(31, 34, 2013, 7, 13, 55),
(32, 34, 2013, 8, 13, 55),
(33, 34, 2013, 9, 13, 55),
(34, 34, 2013, 10, 13, 55),
(35, 34, 2013, 11, 13, 55),
(36, 34, 2013, 12, 13, 55),
(37, 34, 2014, 1, 13, 55),
(38, 34, 2014, 2, 13, 55),
(39, 34, 2014, 3, 13, 55),
(40, 34, 2014, 4, 13, 55),
(41, 34, 2014, 5, 13, 55),
(42, 34, 2014, 6, 13, 55),
(43, 34, 2014, 7, 13, 55),
(44, 35, 2013, 7, 13, 55),
(45, 35, 2013, 8, 13, 55),
(46, 35, 2013, 9, 13, 55),
(47, 35, 2013, 10, 13, 55),
(48, 35, 2013, 11, 13, 55),
(49, 35, 2013, 12, 13, 55),
(50, 35, 2014, 1, 13, 55),
(51, 35, 2014, 2, 13, 55),
(52, 35, 2014, 3, 13, 55),
(53, 35, 2014, 4, 13, 55),
(54, 35, 2014, 5, 13, 55),
(55, 35, 2014, 6, 13, 55),
(56, 35, 2014, 7, 13, 55),
(57, 36, 2013, 7, 13, 55),
(58, 36, 2013, 8, 13, 55),
(59, 36, 2013, 9, 13, 55),
(60, 36, 2013, 10, 13, 55),
(61, 36, 2013, 11, 13, 55),
(62, 36, 2013, 12, 13, 55),
(63, 36, 2014, 1, 13, 55),
(64, 36, 2014, 2, 13, 55),
(65, 36, 2014, 3, 13, 55),
(66, 36, 2014, 4, 13, 55),
(67, 36, 2014, 5, 13, 55),
(68, 36, 2014, 6, 13, 55),
(69, 36, 2014, 7, 13, 55),
(70, 37, 2013, 7, 13, 55),
(71, 37, 2013, 8, 13, 55),
(72, 37, 2013, 9, 13, 55),
(73, 37, 2013, 10, 13, 55),
(74, 37, 2013, 11, 13, 55),
(75, 37, 2013, 12, 13, 55),
(76, 37, 2014, 1, 13, 55),
(77, 37, 2014, 2, 13, 55),
(78, 37, 2014, 3, 13, 55),
(79, 37, 2014, 4, 13, 55),
(80, 37, 2014, 5, 13, 55),
(81, 37, 2014, 6, 13, 55),
(82, 37, 2014, 7, 13, 55),
(83, 38, 2013, 7, 13, 55),
(84, 38, 2013, 8, 13, 55),
(85, 38, 2013, 9, 13, 55),
(86, 38, 2013, 10, 13, 55),
(87, 38, 2013, 11, 13, 55),
(88, 38, 2013, 12, 13, 55),
(89, 38, 2014, 1, 13, 55),
(90, 38, 2014, 2, 13, 55),
(91, 38, 2014, 3, 13, 55),
(92, 38, 2014, 4, 13, 55),
(93, 38, 2014, 5, 13, 55),
(94, 38, 2014, 6, 13, 55),
(95, 38, 2014, 7, 13, 55),
(96, 39, 2013, 7, 13, 55),
(97, 39, 2013, 8, 13, 55),
(98, 39, 2013, 9, 13, 55),
(99, 39, 2013, 10, 13, 55),
(100, 39, 2013, 11, 13, 55),
(101, 39, 2013, 12, 13, 55),
(102, 39, 2014, 1, 13, 55),
(103, 39, 2014, 2, 13, 55),
(104, 39, 2014, 3, 13, 55),
(105, 39, 2014, 4, 13, 55),
(106, 39, 2014, 5, 13, 55),
(107, 39, 2014, 6, 13, 55),
(108, 39, 2014, 7, 13, 55),
(109, 40, 2013, 7, 13, 55),
(110, 40, 2013, 8, 13, 55),
(111, 40, 2013, 9, 13, 55),
(112, 40, 2013, 10, 13, 55),
(113, 40, 2013, 11, 13, 55),
(114, 40, 2013, 12, 13, 55),
(115, 40, 2014, 1, 13, 55),
(116, 40, 2014, 2, 13, 55),
(117, 40, 2014, 3, 13, 55),
(118, 40, 2014, 4, 13, 55),
(119, 40, 2014, 5, 13, 55),
(120, 40, 2014, 6, 13, 55),
(121, 40, 2014, 7, 13, 55),
(122, 41, 2013, 7, 13, 55),
(123, 41, 2013, 8, 13, 55),
(124, 41, 2013, 9, 13, 55),
(125, 41, 2013, 10, 13, 55),
(126, 41, 2013, 11, 13, 55),
(127, 41, 2013, 12, 13, 55),
(128, 41, 2014, 1, 13, 55),
(129, 41, 2014, 2, 13, 55),
(130, 41, 2014, 3, 13, 55),
(131, 41, 2014, 4, 13, 55),
(132, 41, 2014, 5, 13, 55),
(133, 41, 2014, 6, 13, 55),
(134, 41, 2014, 7, 13, 55),
(135, 42, 2013, 7, 13, 55),
(136, 42, 2013, 8, 13, 55),
(137, 42, 2013, 9, 13, 55),
(138, 42, 2013, 10, 13, 55),
(139, 42, 2013, 11, 13, 55),
(140, 42, 2013, 12, 13, 55),
(141, 42, 2014, 1, 13, 55),
(142, 42, 2014, 2, 13, 55),
(143, 42, 2014, 3, 13, 55),
(144, 42, 2014, 4, 13, 55),
(145, 42, 2014, 5, 13, 55),
(146, 42, 2014, 6, 13, 55),
(147, 42, 2014, 7, 13, 55),
(148, 43, 2013, 7, 15, 55),
(149, 43, 2013, 8, 13, 54),
(150, 43, 2013, 9, 13, 55),
(151, 43, 2013, 10, 13, 55),
(152, 43, 2013, 11, 13, 55),
(153, 43, 2013, 12, 13, 55),
(154, 43, 2014, 1, 13, 55),
(155, 43, 2014, 2, 13, 55),
(156, 43, 2014, 3, 13, 55),
(157, 43, 2014, 4, 13, 55),
(158, 43, 2014, 5, 13, 55),
(159, 43, 2014, 6, 13, 55),
(160, 43, 2014, 7, 13, 55),
(161, 44, 2013, 7, 19, 0),
(162, 44, 2013, 8, 13, 0),
(163, 44, 2013, 9, 13, 0),
(164, 44, 2013, 10, 13, 0),
(165, 44, 2013, 11, 13, 0),
(166, 44, 2013, 12, 13, 0),
(167, 44, 2014, 1, 13, 0),
(168, 44, 2014, 2, 13, 0),
(169, 44, 2014, 3, 13, 0),
(170, 44, 2014, 4, 13, 0),
(171, 44, 2014, 5, 13, 0),
(172, 44, 2014, 6, 13, 0),
(173, 44, 2014, 7, 13, 0),
(174, 45, 2013, 7, 13, 0),
(175, 45, 2013, 8, 13, 0),
(176, 45, 2013, 9, 13, 0),
(177, 45, 2013, 10, 13, 0),
(178, 45, 2013, 11, 13, 0),
(179, 45, 2013, 12, 13, 0),
(180, 45, 2014, 1, 13, 0),
(181, 45, 2014, 2, 13, 0),
(182, 45, 2014, 3, 13, 0),
(183, 45, 2014, 4, 13, 0),
(184, 45, 2014, 5, 13, 0),
(185, 45, 2014, 6, 13, 0),
(186, 45, 2014, 7, 13, 0),
(187, 46, 2013, 6, 12, 0),
(188, 46, 2013, 8, 13, 0),
(189, 46, 2013, 9, 13, 0),
(190, 46, 2013, 10, 13, 0),
(191, 46, 2013, 11, 13, 0),
(192, 46, 2013, 12, 13, 0),
(193, 46, 2014, 1, 13, 0),
(194, 46, 2014, 2, 13, 0),
(195, 46, 2014, 3, 13, 0),
(196, 46, 2014, 4, 13, 0),
(197, 46, 2014, 5, 13, 0),
(198, 46, 2014, 6, 13, 0),
(199, 46, 2014, 7, 13, 0),
(200, 47, 2013, 7, 13, 0),
(201, 47, 2013, 8, 13, 0),
(202, 47, 2013, 9, 13, 0),
(203, 47, 2013, 10, 13, 0),
(204, 47, 2013, 11, 13, 0),
(205, 47, 2013, 12, 13, 0),
(206, 47, 2014, 1, 13, 0),
(207, 47, 2014, 2, 13, 0),
(208, 47, 2014, 3, 13, 0),
(209, 47, 2014, 4, 13, 0),
(210, 47, 2014, 5, 13, 0),
(211, 47, 2014, 6, 13, 0),
(212, 47, 2014, 7, 13, 0),
(213, 48, 2013, 6, 13, 0),
(214, 48, 2013, 8, 13, 0),
(215, 48, 2013, 9, 13, 0),
(216, 48, 2013, 10, 13, 0),
(217, 48, 2013, 11, 13, 0),
(218, 48, 2013, 12, 13, 0),
(219, 48, 2014, 1, 13, 0),
(220, 48, 2014, 2, 13, 0),
(221, 48, 2014, 3, 13, 0),
(222, 48, 2014, 4, 13, 0),
(223, 48, 2014, 5, 13, 0),
(224, 48, 2014, 6, 13, 0),
(225, 48, 2014, 7, 13, 0),
(226, 49, 2013, 7, 13, 0),
(227, 49, 2013, 8, 13, 0),
(228, 49, 2013, 9, 13, 0),
(229, 49, 2013, 10, 13, 0),
(230, 49, 2013, 11, 13, 0),
(231, 49, 2013, 12, 13, 0),
(232, 49, 2014, 1, 13, 0),
(233, 49, 2014, 2, 13, 0),
(234, 49, 2014, 3, 13, 0),
(235, 49, 2014, 4, 13, 0),
(236, 49, 2014, 5, 13, 0),
(237, 49, 2014, 6, 13, 0),
(238, 49, 2014, 7, 13, 0),
(239, 50, 2013, 7, 13, 0),
(240, 50, 2013, 8, 13, 0),
(241, 50, 2013, 9, 13, 0),
(242, 50, 2013, 10, 13, 0),
(243, 50, 2013, 11, 13, 0),
(244, 50, 2013, 12, 13, 0),
(245, 50, 2014, 1, 13, 0),
(246, 50, 2014, 2, 13, 0),
(247, 50, 2014, 3, 13, 0),
(248, 50, 2014, 4, 13, 0),
(249, 50, 2014, 5, 13, 0),
(250, 50, 2014, 6, 13, 0),
(251, 50, 2014, 7, 13, 0),
(252, 51, 2013, 7, 13, 0),
(253, 51, 2013, 8, 13, 0),
(254, 51, 2013, 9, 13, 0),
(255, 51, 2013, 10, 13, 0),
(256, 51, 2013, 11, 13, 0),
(257, 51, 2013, 12, 13, 0),
(258, 51, 2014, 1, 13, 0),
(259, 51, 2014, 2, 13, 0),
(260, 51, 2014, 3, 13, 0),
(261, 51, 2014, 4, 13, 0),
(262, 51, 2014, 5, 13, 0),
(263, 51, 2014, 6, 13, 0),
(264, 51, 2014, 7, 13, 0),
(265, 52, 2013, 7, 18, 0),
(266, 52, 2013, 8, 13, 0),
(267, 52, 2013, 9, 13, 0),
(268, 52, 2013, 10, 13, 0),
(269, 52, 2013, 11, 13, 0),
(270, 52, 2013, 12, 13, 0),
(271, 52, 2014, 1, 13, 0),
(272, 52, 2014, 2, 13, 0),
(273, 52, 2014, 3, 13, 0),
(274, 52, 2014, 4, 13, 0),
(275, 52, 2014, 5, 13, 0),
(276, 52, 2014, 6, 13, 0),
(277, 52, 2014, 7, 13, 0),
(278, 53, 2013, 7, 13, 0),
(279, 53, 2013, 8, 13, 0),
(280, 53, 2013, 9, 13, 0),
(281, 53, 2013, 10, 13, 0),
(282, 53, 2013, 11, 13, 7543),
(283, 53, 2013, 12, 13, 0),
(284, 53, 2014, 1, 13, 0),
(285, 53, 2014, 2, 13, 0),
(286, 53, 2014, 3, 13, 0),
(287, 53, 2014, 4, 13, 0),
(288, 53, 2014, 5, 13, 0),
(289, 53, 2014, 6, 13, 0),
(290, 53, 2014, 7, 13, 0),
(291, 54, 2013, 8, 15, 465),
(292, 54, 2013, 8, 13, 0),
(293, 54, 2013, 9, 13, 0),
(294, 54, 2013, 10, 13, 0),
(295, 54, 2013, 11, 13, 0),
(296, 54, 2013, 12, 13, 0),
(297, 54, 2014, 1, 13, 0),
(298, 54, 2014, 2, 13, 0),
(299, 54, 2014, 3, 13, 0),
(300, 54, 2014, 4, 13, 0),
(301, 54, 2014, 5, 13, 0),
(302, 54, 2014, 6, 13, 0),
(303, 54, 2014, 7, 13, 0),
(304, 55, 2013, 7, 13, 55),
(305, 55, 2013, 8, 13, 345),
(306, 55, 2013, 9, 13, 0),
(307, 55, 2013, 10, 13, 0),
(308, 55, 2013, 11, 13, 0),
(309, 55, 2013, 12, 13, 0),
(310, 55, 2014, 1, 13, 0),
(311, 55, 2014, 2, 13, 0),
(312, 55, 2014, 3, 13, 0),
(313, 55, 2014, 4, 13, 0),
(314, 55, 2014, 5, 13, 0),
(315, 55, 2014, 6, 13, 0),
(316, 55, 2014, 7, 13, 0),
(317, 56, 2013, 7, 13, 0),
(318, 56, 2013, 8, 13, 0),
(319, 56, 2013, 9, 13, 0),
(320, 56, 2013, 10, 13, 0),
(321, 56, 2013, 11, 13, 0),
(322, 56, 2013, 12, 13, 0),
(323, 56, 2014, 1, 13, 0),
(324, 56, 2014, 2, 13, 0),
(325, 56, 2014, 3, 13, 0),
(326, 56, 2014, 4, 13, 0),
(327, 56, 2014, 5, 13, 0),
(328, 56, 2014, 6, 13, 24),
(329, 56, 2014, 7, 13, 0),
(330, 57, 2013, 7, 13, 0),
(331, 57, 2013, 8, 13, 0),
(332, 57, 2013, 9, 13, 0),
(333, 57, 2013, 10, 13, 0),
(334, 57, 2013, 11, 13, 0),
(335, 57, 2013, 12, 13, 0),
(336, 57, 2014, 1, 13, 0),
(337, 57, 2014, 2, 13, 0),
(338, 57, 2014, 3, 13, 0),
(339, 57, 2014, 4, 2, 76),
(340, 57, 2014, 5, 30, 76),
(341, 57, 2014, 6, 16, 8000),
(342, 58, 2013, 7, 13, 0),
(343, 58, 2013, 8, 13, 0),
(344, 58, 2013, 9, 13, 0),
(345, 58, 2013, 10, 13, 0),
(346, 58, 2013, 11, 13, 0),
(347, 58, 2015, 3, 19, 0),
(348, 58, 2014, 1, 13, 0),
(349, 58, 2014, 2, 13, 0),
(350, 58, 2014, 3, 13, 0),
(351, 58, 2014, 4, 13, 0),
(352, 58, 2014, 5, 13, 35),
(353, 58, 2014, 6, 20, 435),
(354, 59, 2013, 7, 13, 0),
(355, 59, 2013, 8, 13, 0),
(356, 59, 2013, 9, 13, 0),
(357, 59, 2013, 10, 13, 0),
(358, 59, 2013, 11, 13, 0),
(359, 59, 2013, 12, 13, 0),
(360, 59, 2014, 1, 13, 0),
(361, 59, 2014, 2, 13, 0),
(362, 59, 2014, 3, 13, 0),
(363, 59, 2014, 4, 13, 0),
(364, 59, 2014, 5, 13, 0),
(365, 59, 2014, 6, 13, 56);

-- --------------------------------------------------------

--
-- Table structure for table `detalledespacho`
--

CREATE TABLE IF NOT EXISTS `detalledespacho` (
  `nDetDesId` int(10) unsigned NOT NULL auto_increment,
  `nImsId` int(10) unsigned default NULL,
  `nDesId` varchar(45) collate utf8_spanish_ci default NULL,
  `tDetFechaRegistro` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `nDetEstado` varchar(1) collate utf8_spanish_ci default NULL,
  `cDetdescripcion` text collate utf8_spanish_ci,
  `nDetcantidad` int(10) unsigned default NULL,
  PRIMARY KEY  (`nDetDesId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `detalledespacho`
--

INSERT INTO `detalledespacho` (`nDetDesId`, `nImsId`, `nDesId`, `tDetFechaRegistro`, `nDetEstado`, `cDetdescripcion`, `nDetcantidad`) VALUES
(1, 1, '1', '2013-06-13 22:47:13', '1', 'dfd', 101),
(2, 4, '1', '2013-06-13 22:12:18', '1', 'ds', 25),
(3, 1, '1', '2013-06-13 22:24:45', '1', 'esdf', 164),
(4, 3, '1', '2013-06-13 22:48:25', '1', 'dsdfsfdsf', 5);

-- --------------------------------------------------------

--
-- Table structure for table `insumos`
--

CREATE TABLE IF NOT EXISTS `insumos` (
  `nImsId` int(11) NOT NULL auto_increment,
  `cImsNombre` varchar(150) collate utf8_spanish_ci default NULL,
  `nImsUnidadMedida` int(11) default NULL,
  `tImsFechaRegistro` timestamp NULL default CURRENT_TIMESTAMP,
  `tImsFechaVencimiento` varchar(20) collate utf8_spanish_ci default NULL,
  `cImsEstado` char(1) collate utf8_spanish_ci default 'A',
  `nImsTipo` int(11) default NULL,
  `cImsDescripcion` varchar(250) collate utf8_spanish_ci default NULL,
  `nImsCantidad` int(11) NOT NULL default '20',
  PRIMARY KEY  (`nImsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `insumos`
--

INSERT INTO `insumos` (`nImsId`, `cImsNombre`, `nImsUnidadMedida`, `tImsFechaRegistro`, `tImsFechaVencimiento`, `cImsEstado`, `nImsTipo`, `cImsDescripcion`, `nImsCantidad`) VALUES
(1, 'Leche2', 1, '2013-06-13 22:25:36', '17/10/2013', '1', 2, 'Leche de Soya ', 0),
(2, 'Cereales', 3, '2013-06-06 22:15:24', '29/11/2013', '1', 3, 'Hojuelas de Maiz reforzadas con actibio', 20),
(3, 'Menestras', 1, '2013-06-06 22:15:24', '31/12/2013', '1', 2, 'Frijoles', 15),
(4, 'Leche Evaporada', 0, '2013-06-13 21:20:18', '21/06/2013', '1', NULL, 'Leche en tarro', 775);

-- --------------------------------------------------------

--
-- Table structure for table `inventario`
--

CREATE TABLE IF NOT EXISTS `inventario` (
  `nInvID` int(11) NOT NULL auto_increment,
  `tInvFechaIngreso` timestamp NULL default CURRENT_TIMESTAMP,
  `tInvFechaSalida` timestamp NULL default NULL,
  `nInvCantidad` int(11) default NULL,
  `nImsId` int(11) NOT NULL,
  `nInvStockMinimo` int(11) default NULL,
  `nInvStockMaximo` varchar(45) collate utf8_spanish_ci default NULL,
  `nDesId` int(11) NOT NULL,
  `nInvStock` int(11) default NULL,
  PRIMARY KEY  (`nInvID`),
  KEY `nDesId` (`nDesId`),
  KEY `nImsId` (`nImsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `nMenId` int(11) NOT NULL auto_increment,
  `nModId` int(11) NOT NULL,
  `cMenMenu` varchar(60) collate utf8_spanish_ci NOT NULL,
  `cMenUrl` varchar(250) collate utf8_spanish_ci NOT NULL,
  `cMenOrden` tinyint(4) NOT NULL,
  `cMenActivo` char(1) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`nMenId`),
  KEY `nModId` (`nModId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`nMenId`, `nModId`, `cMenMenu`, `cMenUrl`, `cMenOrden`, `cMenActivo`) VALUES
(1, 1, 'djnf kisjd n weefk dsj', 'main', 1, '0'),
(3, 2, 'Insumos', 'insumo', 1, '0'),
(4, 2, 'Despacho', 'despacho', 2, '0'),
(5, 3, 'Gestion de Personas', 'persona', 1, '0'),
(6, 4, 'Gestion de Usuarios', 'usuario', 2, '0'),
(7, 5, 'Gestion de Club', 'club', 1, '0'),
(8, 6, 'Gestion de Comite', 'comite', 1, '0'),
(9, 7, 'Gestion de Beneficiarios', 'beneficiario', 1, '0'),
(10, 2, 'Generar Cronograma', 'cronograma', 3, '0');

-- --------------------------------------------------------

--
-- Table structure for table `modulo`
--

CREATE TABLE IF NOT EXISTS `modulo` (
  `nModId` int(11) NOT NULL auto_increment,
  `cModModulo` varchar(60) collate utf8_spanish_ci default NULL,
  `nModOrden` tinyint(4) default NULL,
  `cModIcono` varchar(45) collate utf8_spanish_ci default NULL,
  PRIMARY KEY  (`nModId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `modulo`
--

INSERT INTO `modulo` (`nModId`, `cModModulo`, `nModOrden`, `cModIcono`) VALUES
(1, 'Principal', 1, 'icomoon-icon-vcard'),
(2, 'Procesos', 2, 'icomoon-icon-stats-up'),
(3, 'Personas', 3, 'icomoon-icon-vcard'),
(4, 'Usuarios', 7, 'icomoon-icon-user-4'),
(5, 'Clubes', 4, 'icomoon-icon-home-8 '),
(6, 'Comite', 5, 'icomoon-icon-users '),
(7, 'Beneficiarios', 6, 'icomoon-icon-man');

-- --------------------------------------------------------

--
-- Table structure for table `multitabla`
--

CREATE TABLE IF NOT EXISTS `multitabla` (
  `nMulId` int(11) NOT NULL auto_increment,
  `nMulIdPadre` int(11) default NULL,
  `cMulDescripcion` varchar(250) collate utf8_spanish_ci NOT NULL,
  `dMulFechaRegistro` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `nMulOrden` tinyint(4) NOT NULL,
  `nMulEstado` char(1) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`nMulId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `multitabla`
--

INSERT INTO `multitabla` (`nMulId`, `nMulIdPadre`, `cMulDescripcion`, `dMulFechaRegistro`, `nMulOrden`, `nMulEstado`) VALUES
(1, 1, 'TIPO COMITE', '2013-05-08 22:21:55', 0, 'A'),
(2, 1, 'PRESIDENTA', '2013-06-13 23:01:47', 1, 'A'),
(3, 1, 'SECRETARIA', '2013-06-13 23:01:47', 1, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `permiso`
--

CREATE TABLE IF NOT EXISTS `permiso` (
  `nPermId` int(11) NOT NULL auto_increment,
  `nUsuCodigo` int(11) NOT NULL,
  `nMenId` int(11) NOT NULL,
  `dPermFechaInicio` timestamp NULL default CURRENT_TIMESTAMP,
  `dPermFechaFin` datetime default NULL,
  `cPermActivo` char(1) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`nPermId`),
  KEY `nUsuCodigo` (`nUsuCodigo`),
  KEY `nMenId` (`nMenId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=49 ;

--
-- Dumping data for table `permiso`
--

INSERT INTO `permiso` (`nPermId`, `nUsuCodigo`, `nMenId`, `dPermFechaInicio`, `dPermFechaFin`, `cPermActivo`) VALUES
(1, 1, 1, '2013-04-30 00:09:54', NULL, '1'),
(4, 1, 3, '2013-04-30 02:03:03', NULL, '1'),
(5, 1, 4, '2013-04-30 02:03:03', NULL, '1'),
(6, 1, 5, '2013-05-01 22:54:59', NULL, '1'),
(7, 1, 6, '2013-05-01 22:54:59', NULL, '1'),
(8, 1, 7, '2013-05-01 23:11:06', NULL, '1'),
(9, 1, 9, '2013-05-01 23:11:06', NULL, '1'),
(10, 1, 8, '2013-05-01 23:11:06', NULL, '1'),
(14, 2, 3, '2013-05-29 17:29:15', '2013-05-29 14:27:02', '0'),
(15, 2, 6, '2013-05-29 18:39:36', '2013-05-29 14:26:07', '0'),
(16, 2, 3, '2013-05-29 19:20:53', '2013-05-29 14:27:02', '0'),
(17, 2, 4, '2013-05-29 19:20:53', '2013-05-29 14:27:02', '0'),
(18, 2, 5, '2013-05-29 19:20:53', '2013-05-29 14:29:38', '0'),
(19, 2, 5, '2013-05-29 19:21:56', '2013-05-29 14:29:38', '0'),
(20, 2, 5, '2013-05-29 19:24:05', '2013-05-29 14:29:38', '0'),
(21, 2, 6, '2013-05-29 19:24:05', '2013-05-29 14:26:07', '0'),
(22, 2, 5, '2013-05-29 19:26:07', '2013-05-29 14:29:38', '0'),
(23, 2, 1, '2013-05-29 19:26:35', '2013-05-29 14:29:38', '0'),
(24, 2, 3, '2013-05-29 19:26:35', '2013-05-29 14:27:02', '0'),
(25, 2, 4, '2013-05-29 19:26:35', '2013-05-29 14:27:02', '0'),
(26, 2, 5, '2013-05-29 19:27:02', '2013-05-29 14:29:38', '0'),
(27, 2, 3, '2013-05-29 19:29:38', NULL, '1'),
(28, 2, 4, '2013-05-29 19:29:38', NULL, '1'),
(29, 1, 10, '2013-06-03 03:36:56', NULL, '1'),
(30, 2, 5, '2013-06-13 04:54:43', NULL, '1'),
(31, 4, 1, '2013-06-13 23:04:01', '2013-06-13 18:04:07', '0'),
(32, 4, 3, '2013-06-13 23:04:01', '2013-06-13 18:04:07', '0'),
(33, 4, 4, '2013-06-13 23:04:01', '2013-06-13 18:04:07', '0'),
(34, 4, 10, '2013-06-13 23:04:01', '2013-06-13 18:04:07', '0'),
(35, 4, 5, '2013-06-13 23:04:01', '2013-06-13 18:04:20', '0'),
(36, 4, 6, '2013-06-13 23:04:01', '2013-06-13 18:04:20', '0'),
(37, 4, 7, '2013-06-13 23:04:01', '2013-06-13 18:04:20', '0'),
(38, 4, 8, '2013-06-13 23:04:01', '2013-06-13 18:04:20', '0'),
(39, 4, 9, '2013-06-13 23:04:01', '2013-06-13 18:04:20', '0'),
(40, 4, 1, '2013-06-13 23:05:18', NULL, '1'),
(41, 4, 3, '2013-06-13 23:05:18', NULL, '1'),
(42, 4, 4, '2013-06-13 23:05:18', NULL, '1'),
(43, 4, 10, '2013-06-13 23:05:18', NULL, '1'),
(44, 4, 5, '2013-06-13 23:05:18', NULL, '1'),
(45, 4, 6, '2013-06-13 23:05:18', NULL, '1'),
(46, 4, 7, '2013-06-13 23:05:18', NULL, '1'),
(47, 4, 8, '2013-06-13 23:05:18', NULL, '1'),
(48, 4, 9, '2013-06-13 23:05:18', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `nPerId` int(11) NOT NULL auto_increment,
  `cPerNombres` varchar(50) collate utf8_spanish_ci NOT NULL,
  `cPerApellidoPaterno` varchar(50) collate utf8_spanish_ci NOT NULL,
  `cPerApellidoMaterno` varchar(50) collate utf8_spanish_ci NOT NULL,
  `cPerDni` char(8) collate utf8_spanish_ci NOT NULL,
  `cPerDireccion` varchar(90) collate utf8_spanish_ci NOT NULL,
  `cPerTelefono` varchar(20) collate utf8_spanish_ci default NULL,
  `cPerCelular` varchar(11) collate utf8_spanish_ci default NULL,
  `cPerEstado` char(1) collate utf8_spanish_ci NOT NULL,
  `tPerFechaRegistro` timestamp NULL default CURRENT_TIMESTAMP,
  `tPerFechaBaja` timestamp NULL default NULL,
  PRIMARY KEY  (`nPerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`nPerId`, `cPerNombres`, `cPerApellidoPaterno`, `cPerApellidoMaterno`, `cPerDni`, `cPerDireccion`, `cPerTelefono`, `cPerCelular`, `cPerEstado`, `tPerFechaRegistro`, `tPerFechaBaja`) VALUES
(1, 'Juan', 'Perez', 'Rodriguez', '24520569', 'Las Flores', '95629843', '244577888', '1', '2013-04-29 23:36:04', NULL),
(2, 'Eduardo', 'Torres', 'Maquiabelo', '46087784', 'Las Quintanas 151', '94876598', NULL, '1', '2013-04-29 23:36:04', NULL),
(3, 'Cristofer2', 'Paredes2', 'Bour2', '22222222', 'los cerezos 3333333333', '333333333', '999999999', '', '2013-05-16 23:14:59', NULL),
(4, 'Silvia', 'Caceres', 'Contreras', '54231232', 'Los naranjos 343', '3434343', '9044433222', '1', '2013-05-16 23:19:02', NULL),
(5, 'Fernando', 'Golman', 'Turry', '43212254', 'Los Pajares 343', '044234122', '983433222', '1', '2013-05-16 23:20:01', NULL),
(6, 'Julio', 'Scharader', 'Tux', '45324123', 'Los manzanos 3433', '044234122', '9533222112', '1', '2013-05-16 23:20:57', NULL),
(7, 'Pedro', 'Ortega', 'Sanchez', '45312453', 'Los tulipanes 433', '', '', '0', '2013-05-16 23:21:27', NULL),
(8, 'Miguel', 'Torres', 'Alva', '11111111', 'Los jazmines 1212', '5443323', '', '0', '2013-05-16 23:24:48', NULL),
(9, 'Fiidel', 'Torres', 'Alva', '45323211', 'Las lunas 431', '04432121', '', '1', '2013-05-16 23:26:17', NULL),
(10, 'Antony', 'Enriquez', 'Inkas', '46088874', 'Los Ojales 4334', '04425874', '9471485465', '1', '2013-05-17 21:07:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `nUsuCodigo` int(11) NOT NULL auto_increment,
  `nPerId` int(11) NOT NULL,
  `cUsuUsuario` varchar(100) collate utf8_spanish_ci NOT NULL,
  `cUsuClave` varchar(100) collate utf8_spanish_ci NOT NULL,
  `cUsuEstado` char(1) collate utf8_spanish_ci NOT NULL,
  `cUsuTipo` char(1) collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`nUsuCodigo`),
  KEY `fk_Usuario_Persona1` (`nPerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`nUsuCodigo`, `nPerId`, `cUsuUsuario`, `cUsuClave`, `cUsuEstado`, `cUsuTipo`) VALUES
(1, 1, 'eduardors', '865bedd2fba8fe20b828ed07600c64a4', '1', '1'),
(2, 4, '54231232', '865bedd2fba8fe20b828ed07600c64a4', '1', '1'),
(3, 2, '46087784', '84e68eb7323f5ad60e45b2668d164b7f', '1', '1'),
(4, 9, '45323211', 'c8ffe9a587b126f152ed3d89a146b445', '1', '2');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beneficiario`
--
ALTER TABLE `beneficiario`
  ADD CONSTRAINT `fk_Beneficiario_persona1` FOREIGN KEY (`persona_nPerId`) REFERENCES `persona` (`nPerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comite`
--
ALTER TABLE `comite`
  ADD CONSTRAINT `fk_comite_club1` FOREIGN KEY (`nCluId`) REFERENCES `club` (`nCluId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comite_persona1` FOREIGN KEY (`nPerId`) REFERENCES `persona` (`nPerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `detallecronograma`
--
ALTER TABLE `detallecronograma`
  ADD CONSTRAINT `fk_nCroId` FOREIGN KEY (`nCroId`) REFERENCES `cronograma` (`nCroId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `nDesId` FOREIGN KEY (`nDesId`) REFERENCES `despacho` (`nDesId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `nImsId` FOREIGN KEY (`nImsId`) REFERENCES `insumos` (`nImsId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `nModId` FOREIGN KEY (`nModId`) REFERENCES `modulo` (`nModId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `permiso`
--
ALTER TABLE `permiso`
  ADD CONSTRAINT `nMenId` FOREIGN KEY (`nMenId`) REFERENCES `menu` (`nMenId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `nUsuCodigo` FOREIGN KEY (`nUsuCodigo`) REFERENCES `usuario` (`nUsuCodigo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_Usuario_Persona1` FOREIGN KEY (`nPerId`) REFERENCES `persona` (`nPerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
