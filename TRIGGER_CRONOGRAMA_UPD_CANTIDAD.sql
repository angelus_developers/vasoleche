DELIMITER $$
CREATE TRIGGER upd_total_cronograma AFTER UPDATE ON detallecronograma
  FOR EACH ROW
  BEGIN
    DECLARE resto INT;
    DECLARE v_nCroId INT;
    DECLARE cantidadNueva INT;
    IF NEW.nDetMesCantidadMes <> OLD.nDetMesCantidadMes THEN
    	IF NEW.nDetMesCantidadMes > OLD.nDetMesCantidadMes THEN
    		sumartotal:BEGIN
    		-- Definimos el Resto a sumar
    		SET resto = NEW.nDetMesCantidadMes - OLD.nDetMesCantidadMes ;
    		-- Obtenemos el Id del cronograma
    		SET v_nCroId = ( select nCroId from detallecronograma where nDetCroId = OLD.nDetCroId );
    		SET cantidadNueva = ( select  SUM(nDetMesCantidadMes) as cantidad from detallecronograma where nCroId= v_nCroId GROUP BY nCroId );
    		END sumartotal;
    	ELSEIF NEW.nDetMesCantidadMes < OLD.nDetMesCantidadMes THEN
    		restartotal:BEGIN
    		-- Definimos el Resto a restar
    		SET resto = OLD.nDetMesCantidadMes - NEW.nDetMesCantidadMes ;
    		-- Obtenemos el Id del cronograma
    		SET v_nCroId = ( select nCroId from detallecronograma where nDetCroId = OLD.nDetCroId );
    		SET cantidadNueva = ( select  SUM(nDetMesCantidadMes) as cantidad from detallecronograma where nCroId= v_nCroId GROUP BY nCroId );
    		END restartotal;
    	END IF;
    	UPDATE cronograma SET nCroTotal = cantidadNueva where nCroId = v_nCroId;
    END IF;
  END;$$
  DELIMITER ;