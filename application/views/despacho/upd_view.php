<script type="text/javascript" src="<?php echo URL_JS ?>despacho/jsDespachoUpd.js"></script>
<div class="box">
    <div class="title">
        <h4> 
            <span>Actualizar Datos</span>
        </h4>
    </div>
    <?php
    //print_r($informacion);
    //exit;
    ?>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmDespachoUpd" name="frmDespachoUpd">
            <input type="hidden" name="hdnidDespacho_upd" id="hdnidDespacho_upd" value="<?php echo $informacion[0]['nDesId']; ?>"/>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Responsable</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaUpd" id="nTipoPersonaUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Persona">
                                <option></option>
                                <?php foreach ($persona as $persona) { ?>
                                    <?php if ($persona["nPerId"] == $informacion[0]['nDesResponsable']) { ?>
                                        <option value="<?php echo $persona["nPerId"] ?>" selected="selected"><?php echo $persona["datospersona"] ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Club</label>
                        <div class="span8 controls">   
                            <select name="nTipoClubUpd" id="nTipoClubUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Club">
                                <option></option>
                                <?php foreach ($tipoClub as $tipoClub) { ?>
                                    <?php if ($tipoClub["nCluId"] == $informacion[0]['nCluId']) { ?>
                                        <option value="<?php echo $tipoClub["nCluId"] ?>" selected="selected"><?php echo $tipoClub["cCluNombre"] ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
<?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');     ?>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Mes</label>
                        <div class="span8 controls">   
                            <select name="nDesMesUpd" id="nDesMesUpd" class="nostyle" style="width:20%;" placeholder="Selecciona Mes">
                                <?php if ($informacion[0]['cDesMes'] == "Enero") { ?>
                                    <option value="Enero" selected="selected">Enero</option>
                                <?php } else { ?>
                                    <option value="Enero">Enero</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Febrero") { ?>
                                    <option value="Febrero" selected="selected">Febrero</option>
                                <?php } else { ?>
                                    <option value="Febrero">Febrero</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Marzo") { ?>
                                    <option value="Marzo" selected="selected">Marzo</option>
                                <?php } else { ?>
                                    <option value="Marzo">Marzo</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Abril") { ?>
                                    <option value="Abril" selected="selected">Abril</option>
                                <?php } else { ?>
                                    <option value="Abril">Abril</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Mayo") { ?>
                                    <option value="Mayo" selected="selected">Mayo</option>
                                <?php } else { ?>
                                    <option value="Mayo">Mayo</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Junio") { ?>
                                    <option value="Junio" selected="selected">Junio</option>
                                <?php } else { ?>
                                    <option value="Junio">Junio</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Julio") { ?>
                                    <option value="Julio" selected="selected">Julio</option>
                                <?php } else { ?>
                                    <option value="Julio">Julio</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Agosto") { ?>
                                    <option value="Agosto" selected="selected">Agosto</option>
                                <?php } else { ?>
                                    <option value="Agosto">Agosto</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Setiembre") { ?>
                                    <option value="Setiembre" selected="selected">Setiembre</option>
                                <?php } else { ?>
                                    <option value="Setiembre">Setiembre</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Octubre") { ?>
                                    <option value="Octubre" selected="selected">Octubre</option>
                                <?php } else { ?>
                                    <option value="Octubre">Octubre</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Noviembre") { ?>
                                    <option value="Noviembre" selected="selected">Noviembre</option>
                                <?php } else { ?>
                                    <option value="Noviembre">Noviembre</option>
                                <?php } ?>

                                <?php if ($informacion[0]['cDesMes'] == "Diciembre") { ?>
                                    <option value="Diciembre" selected="selected">Diciembre</option>
                                <?php } else { ?>
                                    <option value="Diciembre">Diciembre</option>
<?php } ?>

                            </select>
                        </div> 
                    </div>
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Año</label>
                        <div class="span8 controls">   
                            <select name="nDesAnioUpd" id="nDesAnioUpd" class="nostyle" style="width:20%;" placeholder="Selecciona Anio">
                                <?php for ($i = 1980; $i <= date("Y") + 5; $i++) { ?>
                                    <?php if ($i == $informacion[0]['cDesAnio']) { ?>
                                        <option selected="selected" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
<?php } ?>
                            </select>
                        </div> 
                    </div>
                </div>

            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjDespacho">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        //set_Date("txtFechaRegistroDespacho",'ALL');
        $("#nTipoClubUpd").select2();
        $("#nTipoPersonaUpd").select2();
    })
    
    //$("#select2").select2();
</script>