<script type="text/javascript" src="<?php echo URL_JS; ?>despacho/jsDespachoQry.js" charset=UTF-8"></script>
<!-- Build page from here: Usual with <div class="row-fluid"></div> -->
<?php //echo size($informacion);
//$total_records = mysql_num_rows($informacion);
//echo "cant:".$total_records;
?>
<?php if($informacion){ ?>
<div class="row-fluid">
    <div class="span12">
        <div class="box gradient">
            <div class="content noPad clearfix">
                <!--<table id="Bandejaclub" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">-->
                <table id="BandejaDespacho" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Responsable</th>
                            <th>Club</th>
                            <th>Mes</th>
                            <th>Año</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($informacion as $data) { ?>
                            <tr>
                                <td><?php echo $data["nDesId"]; ?></td>
                                <td><?php echo $data["datospersona"]; ?></td>
                                <td><?php echo $data["cCluNombre"]; ?></td>
                                <td><?php echo $data["cDesMes"]; ?></td>
                                <td><?php echo $data["cDesAnio"]; ?></td>
                                <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('despacho/panel_detalleDespacho', 'Detalle-Despacho', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nDesId" => $data['nDesId']))); ?>', '')">
                                        <img title="Agregar Detalle" alt="A" src="<?php echo URL_IMG; ?>despacho.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('despacho/panel_updDespacho', 'Editar Despacho', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nDesId" => $data['nDesId']))); ?>', '')">
                                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <?php if ($data["nDesEstado"] == 1) { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Despacho','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDespacho(<?php echo $data['nDesId']; ?>)')" />
                                        </a>
                                    <?php } else { ?>
                                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Despacho','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDespacho(<?php echo $data['nDesId']; ?>)')" />
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Responsable</th>
                            <th>Club</th>
                            <th>Mes</th>
                            <th>Año</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div><!-- End .row-fluid -->
<?php }else{
    echo "Sin datos disponible";
}
?>