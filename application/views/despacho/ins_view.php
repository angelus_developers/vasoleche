<script type="text/javascript" src="<?php echo URL_JS ?>despacho/jsDespachoIns.js"></script>
<div class="box">
    <div class="title">
        <h4> 
            <span>Ingresar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmDespachoIns" name="frmDespachoIns">
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Responsable</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersona" id="nTipoPersona" class="nostyle" style="width:100%;" placeholder="Selecciona Persona">
                                <option></option>
                                <?php foreach ($persona as $persona) {
                                    ?>
                                    <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                <?php } ?>
                            </select>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Club</label>
                        <div class="span8 controls">   
                            <select name="nTipoClub" id="nTipoClub" class="nostyle" style="width:100%;" placeholder="Selecciona Club">
                                <option></option>
                                <?php foreach ($tipoClub as $tipoClub) {
                                    ?>
                                    <option value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Mes</label>
                        <div class="span8 controls">   
                            <select name="nDesMes" id="nDesMes" class="nostyle" style="width:20%;" placeholder="Selecciona Mes">
                                <option value="Enero">Enero</option>
                                <option value="Febrero">Febrero</option>
                                <option value="Marzo">Marzo</option>
                                <option value="Abril">Abril</option>
                                <option value="Mayo">Mayo</option>
                                <option value="Junio">Junio</option>
                                <option value="Julio">Julio</option>
                                <option value="Agosto">Agosto</option>
                                <option value="Setiembre">Setiembre</option>
                                <option value="Octubre">Octubre</option>
                                <option value="Noviembre">Noviembre</option>
                                <option value="Diciembre">Diciembre</option>
                            </select>
                        </div> 
                    </div>
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Año</label>
                        <div class="span8 controls">   
                            <select name="nDesAnio" id="nDesAnio" class="nostyle" style="width:20%;" placeholder="Selecciona Anio">
                                <?php for ($i = 1980; $i <= date("Y") + 5; $i++) { ?>
                                    <option><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div> 
                    </div>
                </div>

            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-info">Registrar</button>
            </div>

        </form>
    </div>
    <div id="msjDespacho">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        set_Date("txtFechaRegistroDespacho",'ALL');
        $("#nTipoClub").select2();
        $("#nTipoPersona").select2();
    })
    
    //$("#select2").select2();
</script>