<?php
//echo "id del idDespacho:".$idDespacho;
//print_r($idInsumo);
//nImsId, cImsNombre, nImsUnidadMedida, tImsFechaRegistro, tImsFechaVencimiento, cImsEstado, nImsTipo, cImsDescripcion, nImsCantidad
?>
<script type="text/javascript" src="<?php echo URL_JS ?>despacho/detalleDespacho/jsDetalleDespachoIns.js"></script>
<style>
    #idDespachoDetCabecera span{font-weight: bold;font-size: small; color: #003399;}
</style>
<?php
//print_p($getDespacho);
//exit;
?>


<?php if ($idDespacho != "") { ?>
    <div class="box">
        <div class="title">
            <h4> 
                <span>Ingresar Detalle</span>
            </h4>
        </div>
        <div class="content" id="idDespachoDetCabecera">
            <span>Responsable: </span><?php echo $getDespacho[0]['datospersona'] ?><br/>
            <span>Club: </span><?php echo $getDespacho[0]['cCluNombre'] ?><br/>
            <span>Fecha registro: </span><?php echo $getDespacho[0]['tDesFechaRegistro'] ?><br/>
            <span>Mes:</span> <?php echo $getDespacho[0]['cDesMes'] ?>
            <span>Anio:</span> <?php echo $getDespacho[0]['cDesAnio'] ?><br/>
        </div>
        <div class="content">
            <form class="form-horizontal" action="#" id="frmDetalleDespachoIns" name="frmDetalleDespachoIns" method="post">
                <input type="hidden" id="idDespacho_ins" name="idDespacho_ins" value="<?php echo $idDespacho; ?>" />
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2">Insumo</label>
                            <div class="span8 controls">   
                                <select name="nTipoInsumo_ins" id="nTipoInsumo_ins" style="width:40%;" placeholder="Selecciona Insumo">
                                    <?php foreach ($Insumo as $Insumo) {
                                        ?>
                                        <option value="<?php echo $Insumo["nImsId"] ?>"><?php echo $Insumo["cImsNombre"] ?></option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>
                    </div> 
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtCantidadDespacho_ins">Cantidad</label>
                            <input class="span10" type="text" id="txtCantidadDespacho_ins" name="txtCantidadDespacho_ins" size="" placeholder="Ejm: 250 unidades" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtObservacionesDespacho_ins">Observaciones</label>
                            <textarea cols="20" rows="10" id="txtObservacionesDespacho_ins" name="txtObservacionesDespacho_ins" style="width: 615px;max-width: 615px;min-width: 615px;height: 80px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info">Registrar</button>
                </div>

            </form>
        </div>
        <div id="msjDetalleDespacho">

        </div>
    </div>

    <div class="box">
        <div class="title">
            <h4> 
                <span>Listado-Detalle</span>
            </h4>
        </div>
        <div class="content" id="listadoDetalleDespacho">
            
            <?php if($getDetalleDespacho>0){ ?>
            <table id="BandejaDespacho" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Insumo</th>
                        <th>Cantidad</th>
                        <th>Observacion</th>
                        <th>Fecha</th>
                        <th>Stock</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($getDetalleDespacho as $data) { ?>
                        <tr>
                            <td><?php echo $data["nDetDesId"]; ?></td>
                            <td><?php echo $data["nombreProducto"]; ?></td>
                            <td><?php echo $data["nDetcantidad"]; ?></td>
                            <td><?php echo $data["cDetdescripcion"]; ?></td>
                            <td><?php echo $data["fechaDetalle"]; ?></td>
                            <?php if($data["stock"]=="0"){
                                echo "<td style='background-color: red'>".$data['stock']."</td>";
                            }else{
                                echo "<td>".$data['stock']."</td>";
                            }
                            ?>
                            <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                            <td>
                                <a style="cursor: pointer;" onclick="set_popup2('despacho/panel_updDetalleDespacho', 'Editar Detalle-Despacho', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nDetDesId" => $data['nDetDesId']))); ?>', '')">
                                    <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                </a>
                            </td>
                            <td>
                                <?php if ($data["nDetEstado"] == 1) { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                    <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Despacho','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDetalleDespacho(<?php echo $data['nDetDesId']; ?>)')" />
                                    </a>
                                <?php } else { ?>
                                    <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Despacho','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDetalleDespacho(<?php echo $data['nDetDesId']; ?>)')" />
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{
                echo "no hay datos";
            }
?>
        </div>
    </div>

<?php
} else {
    echo "No se pudo realizar el despacho porque no hay una cabecera de despacho";
}
?>