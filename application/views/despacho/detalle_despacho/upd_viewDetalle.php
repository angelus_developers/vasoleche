<?php
//print_r($getdatosDetalleDespacho);
?>
<script type="text/javascript" src="<?php echo URL_JS ?>despacho/detalleDespacho/jsDetalleDespachoUpd.js"></script>
<?php if(isset($getdatosDetalleDespacho[0]['nDetDesId']) and isset($getdatosDetalleDespacho[0]['iddespacho'])){?>
<div class="box">
    <div class="content">
        <form class="form-horizontal" action="#" id="frmDetalleDespachoUpd" name="frmDetalleDespachoUpd" method="post">
            <input type="hidden" id="hdnidDetalleDesp_upd" name="hdnidDetalleDesp_upd" value="<?php echo $getdatosDetalleDespacho[0]['nDetDesId'];?>" />
            <input type="hidden" id="hdnidDespacho_upd" name="hdnidDespacho_upd" value="<?php echo $getdatosDetalleDespacho[0]['iddespacho'];?>" />
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2">Insumo</label>
                        <div class="span8 controls">   
                            <select name="nTipoInsumo_upd" id="nTipoInsumo_upd" style="width:40%;" placeholder="Selecciona Insumo">
                                <?php foreach ($Insumo as $Insumo) {
                                    ?>
                                    <?php if ($Insumo["nImsId"] == $getdatosDetalleDespacho[0]['idinsumo']) { ?>
                                        <option value="<?php echo $Insumo["nImsId"] ?>" selected ><?php echo $Insumo["cImsNombre"] ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $Insumo["nImsId"] ?>"><?php echo $Insumo["cImsNombre"] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtCantidadDespacho_upd">Cantidad</label>
                        <input class="span10" type="text" id="txtCantidadDespacho_upd" name="txtCantidadDespacho_upd" value="<?php echo $getdatosDetalleDespacho[0]['nDetcantidad']; ?>" size="" placeholder="Ejm: 250 unidades" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtObservacionesDespacho_upd">Observaciones</label>
                        <textarea cols="20" rows="10" id="txtObservacionesDespacho_upd" name="txtObservacionesDespacho_upd" style="width: 615px;max-width: 615px;min-width: 615px;height: 80px;"><?php echo $getdatosDetalleDespacho[0]['cDetdescripcion']; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjDetalleDespachoUPD">

    </div>
</div>
<?php }else{
    echo "falta el id asociado";
}
?>