<script type="text/javascript" src="<?php echo URL_JS ?>despacho/detalleDespacho/jsDetalleDespachoIns.js"></script>

<table id="BandejaDespacho" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>Insumo</th>
            <th>Cantidad</th>
            <th>Observacion</th>
            <th>Fecha</th>
            <th>Stock</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($getDetalleDespacho as $data) { ?>
            <tr>
                <td><?php echo $data["nDetDesId"]; ?></td>
                <td><?php echo $data["nombreProducto"]; ?></td>
                <td><?php echo $data["nDetcantidad"]; ?></td>
                <td><?php echo $data["cDetdescripcion"]; ?></td>
                <td><?php echo $data["fechaDetalle"]; ?></td>
                <?php
                if ($data["stock"] == "0") {
                    echo "<td style='background-color: red'>" . $data['stock'] . "</td>";
                } else {
                    echo "<td>" . $data['stock'] . "</td>";
                }
                ?>
                <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                <td>
                    <a style="cursor: pointer;" onclick="set_popup2('despacho/panel_updDetalleDespacho', 'Editar Despacho', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nDetDesId" => $data['nDetDesId']))); ?>', '')">
                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                    </a>
                </td>
                <td>
    <?php if ($data["nDetEstado"] == 1) { ?>
                                <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Despacho','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDetalleDespacho(<?php echo $data['nDetDesId']; ?>)')" />
                        </a>
    <?php } else { ?>
                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Despacho','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDetalleDespacho(<?php echo $data['nDetDesId']; ?>)')" />
                        </a>
    <?php } ?>
                </td>
            </tr>
<?php } ?>
    </tbody>
</table>