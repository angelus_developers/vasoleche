<script type="text/javascript" src="<?php echo URL_JS; ?>beneficiario/jsBeneficiarioQry.js" charset=UTF-8"></script>
<!-- Build page from here: Usual with <div class="row-fluid"></div> -->
<!--<script type="text/javascript" src="<?php //echo URL_JS; ?>club/jsClubQry.js" charset=UTF-8"></script>-->
<!-- Build page from here: Usual with <div class="row-fluid"></div> -->

<div class="row-fluid">
    <div class="span12">
        <div class="box gradient">
            <div class="content noPad clearfix">
                <!--<table id="Bandejaclub" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">-->
                <table id="Bandejabeneficiario" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="2">Id</th>
                            <th rowspan="2">Beneficiario</th>
                            <th rowspan="2">Sexo</th>
                            <th rowspan="2">Dni Benef.</th>
                            <th colspan="3">Fecha Nacimiento</th>
                            <th rowspan="2">Edad</th>
                            <th rowspan="2">Madre</th>
                            <th rowspan="2">Club</th>
                            <th rowspan="2"></th>
                            <th rowspan="2"></th>
                        </tr>
                        <tr>
                            <th>Dia</th>
                            <th>Mes</th>
                            <th>Año</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($informacion as $data) { ?>
                            <tr>
                                <td><?php echo $data["nBenId"]; ?></td>
                                <td><?php echo $data["datosbeneficiado"]; ?></td>
                                <td><?php echo $data["cBenSexo"]; ?></td>
                                <td><?php echo $data["dnibeneficiado"]; ?></td>
                                <td><?php echo $data["dia"]; ?></td>
                                <td><?php echo $data["mes"]; ?></td>
                                <td><?php echo $data["anio"]; ?></td>
                                <!--<td><php echo retornaedad("1989-12-10"); ?></td>-->
                                <td><?php 
                                //print_r(retornaedad($data["anio"]."-".$data["mes"]."-".$data["dia"])); 
                                $cadena_fecha=retornaedad($data["anio"]."-".$data["mes"]."-".$data["dia"]);
                                print_r($cadena_fecha[0]);
                                //print_r(retornaedad("2005-04-08")); 
                                //print_r(retornaedad($cadena_fecha)); 
                                //print_r($edad);?>
                                </td>
                                <td><?php echo $data["datosMadre"]; ?></td>
                                <td><?php echo $data["datosClub"]; ?></td>
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('beneficiario/panel_updBeneficiario', 'Editar Beneficiario', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nBenId" => $data['nBenId']))); ?>', '')">
                                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <?php if ($data["estadoben"] == "A") { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Beneficiario','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarBeneficiario(<?php echo $data['nBenId']; ?>)')" />
                                        </a>
                                    <?php } else { ?>
                                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Beneficiario','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarBeneficiario(<?php echo $data['nBenId']; ?>)')" />                                   
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
<!--                    <tfoot>
                        <tr>
                            <th rowspan="2">Id</th>
                            <th rowspan="2">Beneficiario</th>
                            <th rowspan="2">Sexo</th>
                            <th rowspan="2">Dni Benef.</th>
                            <th colspan="3">Fecha Nacimiento</th>
                            <th rowspan="2">Edad</th>
                            <th rowspan="2">Madre</th>
                            <th rowspan="2">Club</th>
                            <th rowspan="2"></th>
                            <th rowspan="2"></th>
                        </tr>
                        <tr>
                            <th>Dia</th>
                            <th>Mes</th>
                            <th>Año</th>
                        </tr>
                    </tfoot>-->
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div><!-- End .row-fluid -->