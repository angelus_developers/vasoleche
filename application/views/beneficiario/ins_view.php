<script type="text/javascript" src="<?php echo URL_JS ?>beneficiario/jsBeneficiarioIns.js"></script>

<div class="box">
    <div class="title">
        <h4> 
            <span>Ingresar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmBeneficiarioIns" name="frmBeneficiarioIns">
<!--            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtPartidaNacimientoBen">Partida de Nacimiento</label>
                        <input class="span10" type="text" id="txtPartidaNacimientoBen" name="txtPartidaNacimientoBen" size="" placeholder="Ejm: XDC-x32334343" />
                    </div>
                </div>
            </div>-->
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--<label class="form-label span2" for="txtDireccionClub">Niño Beneficiado</label>-->
                        <label class="form-label span2" for="checkboxes">Niño Beneficiado</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaNino" id="nTipoPersonaNino" class="nostyle" style="width:100%;" placeholder="Selecciona Beneficiario">
                                <option></option>
                                <?php foreach ($persona as $persona) {
                                    ?>
                                    <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--<label class="form-label span2" for="txtDireccionClub">Niño Beneficiado</label>-->
                        <label class="form-label span2">Sexo</label>
                        <div class="span4 controls">
                            <select id="cboSexoBen" name="cboSexoBen">
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--                        <label class="form-label span2" for="txtFechaNacBen">Fecha de Nacimiento</label>
                                                <input class="span10" type="text" id="txtFechaNacBen" name="txtFechaNacBen" style="width: 150px;" placeholder="Ejm: 10/08/1996" />-->



    <!--<input type="text" name="combined-picker" id="combined-picker" value="" />-->
                        <label class="form-label span2" for="txtFechaNacBen">Fecha de Nacimiento</label>
                        <input class="span10" type="text" id="txtFechaNacBen" name="txtFechaNacBen" style="width: 150px;" placeholder="Ejm: 10/08/1996" />
                        <!--<input type="text" name="datepicker" id="datepicker" value="" />-->

                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--<label class="form-label span2" for="txtDireccionClub">Niño Beneficiado</label>-->
                        <label class="form-label span2" for="checkboxes">Madre</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaMadre" id="nTipoPersonaMadre" class="nostyle" style="width:100%;" placeholder="Selecciona Madre">
                                <option></option>
                                <?php foreach ($personamadre as $personamadre) {
                                    ?>
                                    <option value="<?php echo $personamadre["nPerId"] ?>"><?php echo $personamadre["datospersona"] ?></option>
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Club</label>
                        <div class="span8 controls">   
                            <select name="nTipoClub" id="nTipoClub" class="nostyle" style="width:100%;" placeholder="Selecciona Club">
                                <option></option>
                                <?php foreach ($tipoClub as $tipoClub) {
                                    ?>
                                    <option value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div> 
            </div>


            <div class="form-actions">
                <button type="submit" class="btn btn-info">Registrar</button>
            </div>

        </form>
    </div>
    <div id="msjClub">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        //$("#select1").select2();
        $("#nTipoClub").select2();
        $("#nTipoPersonaNino").select2();
        $("#nTipoPersonaMadre").select2();
        
        //if($('#combined-picker').length) {
        //$('#combined-picker').datetimepicker();
        //}
        /*$("#txtFechaNacBen").datepicker({
            showOtherMonths:true
        });*/
        
        set_Date("txtFechaNacBen",'ALL');
        
        
    })
    
    //$("#select2").select2();
</script>