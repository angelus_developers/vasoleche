<script type="text/javascript" src="<?php echo URL_JS ?>beneficiario/jsBeneficiarioUpd.js"></script>

<div class="box">
    <div class="title">
        <h4> 
            <span>Actualizar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmBeneficiarioUpd" name="frmBeneficiarioUpd">
            <input type="hidden" id="hdnidBenUpd" name="hdnidBenUpd" value="<?php echo $informacion[0]['nBenId']?>" />
            <!--            <div class="form-row row-fluid">
                            <div class="span12">
                                <div class="row-fluid">
                                    <label class="form-label span2" for="txtPartidaNacimientoBen">Partida de Nacimiento</label>
                                    <input class="span10" type="text" id="txtPartidaNacimientoBen" name="txtPartidaNacimientoBen" size="" placeholder="Ejm: XDC-x32334343" />
                                </div>
                            </div>
                        </div>-->
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--<label class="form-label span2" for="txtDireccionClub">Niño Beneficiado</label>-->
                        <label class="form-label span2" for="checkboxes">Niño Beneficiado</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaNinoUpd" id="nTipoPersonaNinoUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Beneficiario">
                                <option></option>
                                <?php foreach ($persona as $persona) {
                                    if ($persona["nPerId"] == $informacion[0]["persona_nPerId"]) {
                                        ?>
                                <option selected="selected" value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--<label class="form-label span2" for="txtDireccionClub">Niño Beneficiado</label>-->
                        <label class="form-label span2">Sexo</label>
                        <div class="span4 controls">
                            <select id="cboSexoBenUpd" name="cboSexoBenUpd">
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtFechaNacBen">Fecha de Nacimiento</label>
                        <input class="span10" type="text" id="txtFechaNacBenUpd" name="txtFechaNacBenUpd" style="width: 150px;" placeholder="Ejm: 10/08/1996" value="<?php echo $informacion[0]["cBenFechaNacimiento"];?>" />
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <!--<label class="form-label span2" for="txtDireccionClub">Niño Beneficiado</label>-->
                        <label class="form-label span2" for="checkboxes">Madre</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaMadreUpd" id="nTipoPersonaMadreUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Madre">
                                <option></option>
                                <?php foreach ($personamadre as $personamadre) {
                                    ?>
                                    <?php if($personamadre["nPerId"]==$informacion[0]["nPerIdMadre"]){ ?>
                                <option value="<?php echo $personamadre["nPerId"] ?>" selected><?php echo $personamadre["datospersona"] ?></option>
                                    <?php }?>
                                    <option value="<?php echo $personamadre["nPerId"] ?>"><?php echo $personamadre["datospersona"] ?></option>
                            <?php } ?>
                            </select>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Club</label>
                        <div class="span8 controls">   
                            <select name="nTipoClubUpd" id="nTipoClubUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Club">
                                <option></option>
                                <?php foreach ($tipoClub as $tipoClub) {
                                    ?>
                                <?php if($tipoClub["nCluId"]==$informacion[0]["nclubid"]){ ?>
                                    <option value="<?php echo $tipoClub["nCluId"] ?>" selected><?php echo $tipoClub["cCluNombre"] ?></option>
                                    <?php }else{ ?>
                                    <option value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                    <?php } ?>
                            <?php } ?>
                            </select>
                        </div> 
                    </div>
                </div> 
            </div>


            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjBeneficiario">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        //$("#select1").select2();
        $("#nTipoClubUpd").select2();
        $("#nTipoPersonaNinoUpd").select2();
        $("#nTipoPersonaMadreUpd").select2();
        
        //if($('#combined-picker').length) {
        //$('#combined-picker').datetimepicker();
        //}
        /*$("#txtFechaNacBen").datepicker({
            showOtherMonths:true
        });*/
        
        set_Date("txtFechaNacBenUpd",'ALL');
        
        
    })
    
    //$("#select2").select2();
</script>