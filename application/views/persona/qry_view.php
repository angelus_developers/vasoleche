<script type="text/javascript" src="<?php echo URL_JS; ?>persona/jsPersonaQry.js" charset=UTF-8"></script>
<!-- Build page from here: Usual with <div class="row-fluid"></div> -->

<div class="row-fluid">
    <div class="span12">
        <div class="box gradient">
            <div class="content noPad clearfix">
                <!--<table id="Bandejaclub" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">-->
                <table id="BandejaPersona" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Persona</th>
                            <th>Dni</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($informacion as $data) { ?>
                            <tr>
                                <td><?php echo $data["nPerId"]; ?></td>
                                <td><?php echo $data["datospersona"]; ?></td>
                                <td><?php echo $data["cPerDni"]; ?></td>
                                <td><?php echo $data["cPerDireccion"]; ?></td>
                                <td><?php echo $data["cPerTelefono"]." ".$data["cPerCelular"]; ?></td>
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('persona/panel_updPersona', 'Editar Persona', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nPerId" => $data['nPerId']))); ?>', '')">
                                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <?php if ($data["cPerEstado"] == "1") { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Persona','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarPersona(<?php echo $data['nPerId']; ?>)')" />
                                        </a>
                                    <?php } else { ?>
                                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Persona','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarPersona(<?php echo $data['nPerId']; ?>)')" />                                   
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Persona</th>
                            <th>Dni</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div><!-- End .row-fluid -->