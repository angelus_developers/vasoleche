<script type="text/javascript" src="<?php echo URL_JS ?>cronograma/jsCronogramaIns.js" ></script>
<div class="box">
	<div class="content">
		<form class="form-horizontal" action="#" id="frmUsuarioIns" name="frmPersonaInsa">
			<div class="form-row row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<label class="form-label span2" for="txtcPerDni" >Insumo </label>
						<div id="c_cbo_insumos" class="span5 controls">
							<select name="cbo_insumos" id="cbo_insumos">
								<?php foreach ($cboInsumos as $fila) { ?>
								<option value="<?php echo $fila['nImsId']; ?>"><?php echo $fila['cImsNombre']; ?></option>
								<?php } ?>
							</select>
						</div>
						<button id="btn_generar_cronograma" class="btn-primary btn">Generar</button>
					</div>
				</div>
				<div id="grv_cronograma"></div>
			</div>
		</form>
	</div>
	<div id="msjPersona"></div>
	<div id="grvListadetalle"></div>
</div>