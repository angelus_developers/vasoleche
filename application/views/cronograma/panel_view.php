<script type="text/javascript" src="<?php echo URL_JS; ?>cronograma/jsPanel.js"></script>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header">
            <h4>Generar Cronogramas por Insumo</h4>
        </div>
        <div style="margin-bottom: 20px;">
            <ul id="ListasUsers" class="nav nav-tabs pattern">
                <li class="active"><a href="#newCronograma" data-toggle="tab">Nueva Cronograma</a></li>
                <li><a href="#verCronograma" data-toggle="tab" id="listacronos" >Ver Cronogramas</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade" id="verCronograma"></div>
                <div class="tab-pane fade in active" id="newCronograma">
                    <?php $this->load->view('cronograma/ins_view'); ?>
                </div>
            </div>
        </div>

    </div><!-- End .span6 -->  
</div>
<div id="modaldetacrono" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="lbldetalle" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="lbldetalle">Detalle del Cronograma</h3>
  </div>
  <div id="modaldetacrono_body" class="modal-body">
    <p>One fine body…</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    <!-- <button class="btn btn-primary">Save changes</button> -->
  </div>
</div>