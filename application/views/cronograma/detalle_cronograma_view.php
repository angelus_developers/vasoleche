<!-- Editable  -->
<script type="text/javascript" src="<?php echo URL_JS; ?>bootstrap-editable/js/bootstrap-editable.js"></script>
<link rel="stylesheet" href="<?php echo URL_JS; ?>bootstrap-editable/css/bootstrap-editable.css">
<script type="text/javascript" src="<?php echo URL_JS; ?>bootstrap-editable/js/es-datepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo URL_JS; ?>cronograma/jsCronogramaQry.js"></script>
<!-- <div id="listaPermisos"></div> -->
<table id="qry_detalle_crono" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Fecha</th>
            <th>Cantidad</th>
            <!-- <th>Acciones</th> -->
        </tr>
    </thead>
    <tbody id="qry_detalle_crono_body">
        <?php 
        foreach ($detaCrono as $fila) {
            $fecha = $fila['tDetCroDia'].'/'.$fila['tDetCroMes'].'/'.$fila['tDetCroAnio'];
            ?>
            <tr>           
                <td><?php echo $fila['nDetCroId']; ?></td>
                <td>
                    <a href="#"  id="fecha" data-type="date" data-viewformat="dd/mm/yyyy" data-placeholder="Required"  data-original-title="Select Fecha Entrega" data-pk="<?php echo $fila['nDetCroId']; ?>" ><?php echo$fecha  ?></a>
                </td>
                <td>
                    <a href="#" id="nDetMesCantidadMes" data-type="text"  title="Editar Cantidad" data-pk="<?php echo$fila['nDetCroId']; ?>" ><?php echo $fila['nDetMesCantidadMes'] ?></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Codigo</th>
                <th>Fecha</th>
                <th>Cantidad</th>
            </tr>
        </tfoot>
    </table>
    <!-- <button class="btn btn-primary">Confirmar</button> -->