<script type="text/javascript" src="<?php echo URL_JS; ?>cronograma/jsCronograma.js"></script>
<table id="qry_crono" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Insumo</th>
            <th>Total</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody id="qry_crono_body">
        <?php foreach ($cronos as $fila) { ?>
        <tr data-codins="<?php echo $fila['nImsId']; ?>" data-crono="<?php echo $fila['nCroId']; ?>" >
            <td><?php echo $fila['nCroId']; ?></td>
            <td>
                <?php echo $fila['cImsNombre'] ?>
            </td>
            <td>
                <?php echo $fila['nCroTotal'] ?>
            </td>
            <td>
                <a id="btnoptEditar" class="optEditarCrono" style="cursor: pointer;"  title="Cambiar Clave">
                    <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Codigo</th>
            <th>Fecha</th>
            <th>Cantidad</th>
            <th>Acciones</th>
        </tr>
    </tfoot>
</table>