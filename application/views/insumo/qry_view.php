<script type="text/javascript" src="<?php echo URL_JS; ?>insumo/jsInsumoQry.js" charset=UTF-8"></script>
<!-- Build page from here: Usual with <div class="row-fluid"></div> -->

<div class="row-fluid">
    <div class="span12">
        <div class="box gradient">
            <div class="content noPad clearfix">
                <!--<table id="Bandejaclub" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">-->
                <table id="BandejaInsumo" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>U.Medida</th>
                            <th>F.Vencimiento</th>
                            <th>Descripcion</th>
                            <th>Cantidad</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($informacion as $data) { ?>
                            <tr>
                                <td><?php echo $data["nImsId"]; ?></td>
                                <td><?php echo $data["cImsNombre"]; ?></td>
                                <td><?php echo $data["nImsUnidadMedida"]; ?></td>
                                <td><?php echo $data["tImsFechaVencimiento"]; ?></td>
                                <td><?php echo $data["cImsDescripcion"]; ?></td>
                                <td><?php echo $data["nImsCantidad"]; ?></td>
                                <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('insumo/panel_updInsumo', 'Editar Insumo', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nImsId" => $data['nImsId']))); ?>', '')">
                                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <?php if ($data["cImsEstado"] == 1) { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Club','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarInsumo(<?php echo $data['nImsId']; ?>)')" />
                                        </a>
                                    <?php } else { ?>
                                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Club','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarInsumo(<?php echo $data['nImsId']; ?>)')" />
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>U.Medida</th>
                            <th>F.Vencimiento</th>
                            <th>Descripcion</th>
                            <th>Cantidad</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div><!-- End .row-fluid -->