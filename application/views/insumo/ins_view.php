<script type="text/javascript" src="<?php echo URL_JS ?>insumo/jsInsumoIns.js"></script>

<div class="box">
    <div class="title">
        <h4> 
            <span>Ingresar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmInsumoIns" name="frmInsumoIns">
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtNombreInsumo">Nombre</label>
                        <input class="span10" type="text" id="txtNombreInsumo" name="txtNombreInsumo" size="" placeholder="Ejm: Trigo" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtUnidadInsumo">Unidad de Medida</label>
                        <input class="span10" type="text" id="txtUnidadInsumo" name="txtUnidadInsumo" size="" placeholder="Ejm: Kg" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtFechaVencimientoInsumo">Fecha de Vencimiento</label>
                        <input class="span10" type="text" id="txtFechaVencimientoInsumo" name="txtFechaVencimientoInsumo" size="" placeholder="Ejm: 04/08/2015" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtDescripcionInsumo">Descripcion</label>
                        <input class="span10" type="text" id="txtDescripcionInsumo" name="txtDescripcionInsumo" size="" placeholder="Ejm: Productos Ricos en Vitaminas" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtCantidadInsumo">Cantidad</label>
                        <input class="span10" type="text" id="txtCantidadInsumo" name="txtCantidadInsumo" size="" placeholder="Ejm: 850" />
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-info">Registrar</button>
            </div>

        </form>
    </div>
    <div id="msjInsumo">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        set_Date("txtFechaRegistroInsumo",'ALL');
        set_Date("txtFechaVencimientoInsumo",'ALL');
        
    })
    
    //$("#select2").select2();
</script>