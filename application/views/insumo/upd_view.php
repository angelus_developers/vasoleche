<script type="text/javascript" src="<?php echo URL_JS ?>insumo/jsInsumoUpd.js"></script>

<div class="box">
    <div class="title">
        <h4> 
            <span>Actualizar Datos</span>
        </h4>
    </div>
    <div class="content">

        <form class="form-horizontal" action="#" id="frmInsumoUpd" name="frmInsumoUpd">
            <input type="hidden" name="hdnidInsumo_upd" id="hdnidInsumo_upd" value="<?php echo $informacion[0]['nImsId'];?>"/>
            <!--<input type="text" name="hdnidClub_upd" id="hdnidClub_upd" value="<php echo $informacion[0]['nCluId']; ?>"/>-->
            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtupd_upd_NombreInsumo">Nombre</label>
                        <input class="span10" value="<?php echo $informacion[0]['cImsNombre']; ?>" type="text" id="txtupd_NombreInsumo" name="txtupd_NombreInsumo" size="" placeholder="Ejm: Trigo" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtupd_UnidadInsumo">Unidad de Medida</label>
                        <input class="span10" value="<?php echo $informacion[0]['nImsUnidadMedida']; ?>" type="text" id="txtupd_UnidadInsumo" name="txtupd_UnidadInsumo" size="" placeholder="Ejm: Kg" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtupd_FechaVencimientoInsumo">Fecha de Vencimiento</label>
                        <input class="span10" type="text" value="<?php echo $informacion[0]['tImsFechaVencimiento']; ?>" id="txtupd_FechaVencimientoInsumo" name="txtupd_FechaVencimientoInsumo" size="" placeholder="Ejm: 04/08/2015" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtupd_DescripcionInsumo">Descripcion</label>
                        <input class="span10" type="text" value="<?php echo $informacion[0]['cImsDescripcion']; ?>" id="txtupd_DescripcionInsumo" name="txtupd_DescripcionInsumo" size="" placeholder="Ejm: Productos Ricos en Vitaminas" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtupd_CantidadInsumo">Cantidad</label>
                        <input class="span10" type="text" value="<?php echo $informacion[0]['nImsCantidad']; ?>" id="txtupd_CantidadInsumo" name="txtupd_CantidadInsumo" size="" placeholder="Ejm: 850" />
                    </div>
                </div>
            </div>
            

            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjClub">

    </div>
</div>

<script type="text/javascript">
    $(function(){
        set_Date("txtupd_FechaVencimientoInsumo",'ALL');
        //set_Date("txtFechaVencimientoInsumo",'ALL');
        
    })
    
    //$("#select2").select2();
</script>