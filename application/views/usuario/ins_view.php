<script type="text/javascript" src="<?php echo URL_JS ?>usuario/jsUsuarioIns.js" ></script>
<div class="box">
	<div class="content">
		<form class="form-horizontal" action="#" id="frmUsuarioIns" name="frmPersonaInsa">
			<div class="form-row row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<label class="form-label span2" for="txtcPerDni" >Buscar Persona </label>
						<input class="span4" type="text" maxlength="8" id="txtcPerDni" name="txtcPerDni" placeholder="Ejm: Juan Valera" />
						<input type="hidden" id="txt_nPerId" name="txt_nPerId">
						<button id="btn_fndPerson" class="btn-primary btn">Buscar</button>
					</div>
				</div>
				<div id="infopersona"></div>
			</div>
			<div class="form-row row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<label class="form-label span2" for="txtUsuario" >Usuario </label>
						<input class="span6" type="text" id="txtUsuario" name="txtUsuario" disabled />
					</div>
				</div>
			</div>
			<div class="form-row row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<label class="form-label span2" for="txtClave" >Clave </label>
						<input class="span6" type="password" id="txtClave" name="txtClave" placeholder="Ejm: Flores" />
					</div>
				</div>
			</div>
			<div class="form-row row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<label class="form-label span2" for="cboTipoUser" >Tipo Usuario </label>
						<div class="span6 controls">
							<select name="cboTipoUser" id="cboTipoUser">
								<?php 
								foreach ($cbo as $key => $value) {
									?>
									<option value="<?php echo $value['codx']; ?>"><?php echo $value['dato']; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>	
			<div class="form-actions">
				<button type="submit" id="btnRegistrarUsuario" class="btn btn-info">Registrar</button>
				<button type="reset" id="btnCancelar" class="btn btn-default">cancelar</button>
				<div id="message"></div>
			</div>
		</form>
	</div>
	<div id="msjPersona"></div>
</div>