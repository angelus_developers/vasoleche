<script type="text/javascript" src="<?php echo URL_JS; ?>comite/jsComiteQry.js" charset=UTF-8"></script>

<div class="row-fluid">
    <div class="span12">
        <div class="box gradient">
            <div class="content noPad clearfix">
                <table id="Bandejacomite" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre<br/>Club</th>
                            <th>Persona</th>
                            <th>Relacion<br/>Comite</th>                            
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($informacion as $data) { ?>
                            <tr>
                                <td><?php echo $data["ncomid"]; ?></td>
                                <td><?php echo $data["cclunombre"]; ?></td>
                                <td><?php echo $data["datospersona"]; ?></td>
                                <td><?php echo $data["tipocomite"]; ?></td>
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('comite/panel_updComite', 'Editar Comite', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nComId" => $data['ncomid']))); ?>', '')">
                                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <?php if ($data["ccomestado"] == "A") { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Comite','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarComite(<?php echo $data['ncomid']; ?>)')" />
                                        </a>
                                    <?php } else { ?>
                                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Comite','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarComite(<?php echo $data['ncomid']; ?>)')" />                                   
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nombre<br/>Club</th>
                            <th>Persona</th>
                            <th>Relacion<br/>Comite</th>                            
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div><!-- End .row-fluid -->