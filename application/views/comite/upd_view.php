<script type="text/javascript" src="<?php echo URL_JS ?>comite/jsComiteUpd.js"></script>
<div class="box">
    <div class="title">
        <h4>
            <span>Actualizar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmComiteUpd" name="frmComiteUpd">
            <input type="hidden" name="hdnidComite_upd" id="hdnidComite_upd" value="<?php echo $informacion[0]['nComId'];?>" />
            <!--<input type="text" name="hdnidClub_upd" id="hdnidClub_upd" value="<php echo $informacion[0]['nCluId']; ?>"/>-->
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="nTipoComiteUpd">Tipo Persona</label>
                        <div class="span4 controls">
                            <select name="nTipoComiteUpd" id="nTipoComiteUpd" required="required" data-original-title="Seleccione un Tipo Comite">
                                <?php foreach ($tipoComite as $tipoComite) {
                                    if($informacion[0]['cComTipo']==$tipoComite["nMulIdPadre"]){ ?>
                                <option selected="selected" value="<?php echo $tipoComite["nMulIdPadre"] ?>"><?php echo $tipoComite["cMulDescripcion"] ?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $tipoComite["nMulIdPadre"] ?>"><?php echo $tipoComite["cMulDescripcion"] ?></option>
                                    <?php }
                                    ?>
                                
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoComite', $TipoComite, '', 'id="nTipoComite"  '); ?>
                        </div> 
                    </div>
                </div> 
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Club</label>
                        <div class="span8 controls">   
                            <select name="nTipoClubUpd" id="nTipoClubUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Club">
                                <option></option>
                                <?php foreach ($tipoClub as $tipoClub) {
                                    if($informacion[0]['nCluId'] == $tipoClub["nCluId"]){ ?>
                                <option selected="selected" value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                <?php } ?>
                                    <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div> 
            </div>
            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Persona</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaUpd" id="nTipoPersonaUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Persona">
                                <option></option>
                                <?php foreach ($persona as $persona) {
                                    if($informacion[0]['nPerId'] == $persona["nPerId"]){
                                    ?>
                                <option selected="selected" value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                <?php } ?>    
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div> 
            </div>
            
            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjClub">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        //$("#select1").select2();
        $("#nTipoClubUpd").select2();
        $("#nTipoPersonaUpd").select2();
        
    })
    
    //$("#select2").select2();
</script>