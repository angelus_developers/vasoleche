<script type="text/javascript" src="<?php echo URL_JS; ?>comite/jsComitePanel.js" charset=UTF-8"></script>

<div class="row-fluid">

    <div class="span12">
        <div class="page-header">
            <h4>Gestion de Comites</h4>
        </div>
        <div style="margin-bottom: 20px;">
            <ul id="myTab" class="nav nav-tabs pattern">
                <li class="active"><a href="#home" data-toggle="tab">Crear Comite</a></li>
                <li><a href="#profile" data-toggle="tab" id="tabqry">Buscar Comite</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">
                    <?php $this->load->view("comite/ins_view"); ?>
                </div>
                <div class="tab-pane fade" id="profile">
                    <?php //$this->load->view("club/qry_view"); ?>
                </div>
            </div>
        </div>

    </div>
</div>

