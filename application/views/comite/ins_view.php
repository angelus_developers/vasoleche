<script type="text/javascript" src="<?php echo URL_JS ?>comite/jsComiteIns.js"></script>
<?php
$TipoComite[''] = "Seleccione una Opcion";
foreach ($tipoComite as $tipoComite) {
    $TipoComite[$tipoComite["nMulIdPadre"]] = $tipoComite["cMulDescripcion"];
}
//$Persona[''] = "Seleccione una Opcion";
//foreach ($persona as $persona) {
//    $Persona[$persona["nPerId"]] = $persona["datosPersona"];
//}
?>    

<?php
/*
  $TipoClub[''] = "Seleccione una Opcion";
  foreach ($tipoClub as $tipoClub) {
  $TipoClub[$tipoClub["nCluId"]] = $tipoClub["cCluNombre"];
  }

 */
?>    
<pre>
    <?php //   print_r($TipoClub); ?>
</pre>

<div class="box">
    <div class="title">
        <h4> 
            <span>Ingresar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmComiteIns" name="frmComiteIns">
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="nTipoComite">Tipo Persona</label>
                        <div class="span4 controls">   
                            <?php echo form_dropdown('nTipoComite', $TipoComite, '', 'id="nTipoComite"  required="required" data-original-title="Seleccione un Comite"'); ?>
                        </div> 
                    </div>
                </div> 
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Club</label>
                        <div class="span8 controls">   
                            <select name="nTipoClub" id="nTipoClub" class="nostyle" style="width:100%;" placeholder="Selecciona Club">
                                <option></option>
                                <?php foreach ($tipoClub as $tipoClub) {
                                    ?>
                                    <option value="<?php echo $tipoClub["nCluId"] ?>"><?php echo $tipoClub["cCluNombre"] ?></option>
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div> 
            </div>
            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Persona</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersona" id="nTipoPersona" class="nostyle" style="width:100%;" placeholder="Selecciona Persona">
                                <option></option>
                                <?php foreach ($persona as $persona) {
                                    ?>
                                    <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                <?php } ?>
                            </select>
                            <?php //echo form_dropdown('nTipoClub', $TipoClub, '', 'id="nTipoClub"  required="required" class="nostyle" style="width:100%;" placeholder="Selecciona Club"');  ?>
                        </div> 
                    </div>
                </div> 
            </div>

           <div class="form-actions">
                <button type="submit" class="btn btn-info">Registrar</button>
            </div>

        </form>
    </div>
    <div id="msjClub">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        //$("#select1").select2();
        $("#nTipoClub").select2();
        $("#nTipoPersona").select2();
        
    })
    
    //$("#select2").select2();
</script>