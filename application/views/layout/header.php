<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php $title = isset($title) ? $title : "Sistema de Gestion de Vaso de Leche" ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <meta name="author" content="SuggeElson" />
        <!-- <meta name="description" content="Supr admin template" /> -->
        <!-- <meta name="keywords" content="admin, admin template" /> -->
        <!-- <meta name="application-name" content="Supr admin template" /> -->

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- Le styles -->
        <!-- Use new way for google web fonts 
        http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
        <!-- Headings -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />  -->
        <!-- Text -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> --> 
        <!--[if lt IE 9]>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
        <![endif]-->
        <script  type="text/javascript" src="<?php echo URL_JS; ?>jquery-1.83.min.js"></script>
        <!-- Core stylesheets do not remove -->
        <link href="<?php echo URL_CSS; ?>bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL_CSS; ?>bootstrap/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL_CSS; ?>supr-theme/jquery.ui.supr.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL_CSS; ?>icons.css" rel="stylesheet" type="text/css" />

        <!-- Plugins stylesheets -->
        <link href="<?php echo URL_JS_AD; ?>plugins/misc/qtip/jquery.qtip.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL_JS_AD; ?>plugins/misc/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL_JS_AD; ?>plugins/misc/search/tipuesearch.css" type="text/css" rel="stylesheet" />

        <link href="<?php echo URL_JS_AD; ?>plugins/forms/uniform/uniform.default.css" type="text/css" rel="stylesheet" />
        
        <link href="<?php echo URL_JS_AD; ?>plugins/forms/select/select2.css" type="text/css" rel="stylesheet" />
        
        

        <link href="<?php echo URL_JS_AD; ?>plugins/tables/dataTables/jquery.dataTables.css" type="text/css" rel="stylesheet" />

        <!-- Main stylesheets -->
        <link href="<?php echo URL_CSS; ?>main.css" rel="stylesheet" type="text/css" /> 

        <!-- Custom stylesheets ( Put your own changes here ) -->
        <link href="<?php echo URL_CSS; ?>custom.css" rel="stylesheet" type="text/css" /> 

        <!-- Validate para Formularios -->
        <link rel="stylesheet" href='<?php echo URL_JS_AD ?>plugins/forms/validate/validate.css' />
        <script type="text/javascript" src='<?php echo URL_JS_AD ?>plugins/forms/validate/jquery.validate.js'></script>        

        <script type="text/javascript" src="<?php echo URL_JS; ?>jsGeneral.js" charset=UTF-8"></script>
        <script type="text/javascript" src="<?php echo URL_JS; ?>jsValidacionGeneral.js" charset=UTF-8"></script>
            <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
            <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->

            <!-- Le fav and touch icons -->
            <link rel="shortcut icon" href="<?php echo URL_IMG; ?>favicon.ico" />
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo URL_IMG; ?>apple-touch-icon-144-precomposed.png" />
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo URL_IMG; ?>apple-touch-icon-114-precomposed.png" />
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo URL_IMG; ?>apple-touch-icon-72-precomposed.png" />
            <link rel="apple-touch-icon-precomposed" href="<?php echo URL_IMG; ?>apple-touch-icon-57-precomposed.png" />


            <link href="<?php echo URL_JS_AD ?>plugins/misc/pnotify/jquery.pnotify.default.css" type="text/css" rel="stylesheet" />
            <script type="text/javascript" src="<?php echo URL_JS_AD ?>plugins/misc/pnotify/jquery.pnotify.min.js"></script>

            <link href="<?php echo URL_JS_AD ?>plugins/forms/color-picker/color-picker.css" type="text/css" rel="stylesheet" />
            <script type="text/javascript" src="<?php echo URL_JS_AD ?>plugins/forms/color-picker/colorpicker.js"></script>
            
            
            
            <script type="text/javascript" src="<?php echo URL_JS_AD ?>plugins/forms/timeentry/jquery.timeentry.min.js"></script>
            <script type="text/javascript" src="<?php echo URL_JS ?>cargaInicio.js"></script>
            
            <script type="text/javascript">
                //adding load class to body and hide page
                document.documentElement.className += 'loadstate';
            </script>
            <script type="text/javascript">
                var txt=" ..::<?php echo $title; ?>::.. ";
                var espera=200;
                var refresco=null;
                function rotulo_title() {
                    document.title=txt;
                    txt=txt.substring(1,txt.length)+txt.charAt(0);
                    refresco=setTimeout("rotulo_title()",espera);
                }
                rotulo_title();
            </script>
    </head>

    <body>
        <!-- loading animation -->
        <div id="qLoverlay"></div>
        <div id="qLbar"></div>

        <div id="header">

            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container-fluid">
                        <a class="brand" href="dashboard.html">FM
                            <!-- <span class="slogan">admin</span> -->
                            <img src="<?php echo URL_IMG?>logo.png" alt="Florencia de Mora" />
                        </a>
                        <div class="nav-no-collapse">
                            <ul class="nav">
                                <li class="active">
                                    <a href="dashboard.html">
                                        <span class="icon16 icomoon-icon-screen-2"></span> Dashboard
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="icon16 icomoon-icon-cog"></span> Configuración
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="menu">
                                            <ul>
                                                <li>                                                    
                                                    <a href="#"><span class="icon16 icomoon-icon-equalizer"></span>Configuración Sitio</a>
                                                </li>
                                                <li>                                                    
                                                    <a href="#"><span class="icon16 icomoon-icon-wrench"></span>Complemento</a>
                                                </li>
                                                <li>
                                                    <a href="#"><span class="icon16 icomoon-icon-picture-2"></span>Temas</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="icon16 icomoon-icon-mail-3"></span>Mensajes <span class="notification">8</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="menu">
                                            <ul class="messages">    
                                                <li class="header"><strong>Mensajes</strong> (10) emails y (2) PM</li>
                                                <li>
                                                    <span class="icon"><span class="icon16 icomoon-icon-user-3"></span></span>
                                                    <span class="name"><a data-toggle="modal" href="#myModal1"><strong>Sammy Morerira</strong></a><span class="time">35 min ago</span></span>
                                                    <span class="msg">Solicitud de Registro nuevo club ...</span>
                                                </li>
                                                <li>
                                                    <span class="icon avatar"><img src="<?php echo URL_IMG; ?>avatar.jpg" alt="" /></span>
                                                    <span class="name"><a data-toggle="modal" href="#myModal1"><strong>George Michael</strong></a><span class="time">1 hour ago</span></span>
                                                    <span class="msg">Necesito urgente reunirme con el proveedor ...</span>
                                                </li>
                                                <li>
                                                    <span class="icon"><span class="icon16 icomoon-icon-mail-3"></span></span>
                                                    <span class="name"><a data-toggle="modal" href="#myModal1"><strong>Ivanovich</strong></a><span class="time">1 day ago</span></span>
                                                    <span class="msg">Cotización de proveedores enviada...</span>
                                                </li>
                                                <li class="view-all"><a href="#">Ver todos los mensajes <span class="icon16 icomoon-icon-arrow-right-8"></span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="nav pull-right usernav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="icon16 icomoon-icon-bell-2"></span><span class="notification">3</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="menu">
                                            <ul class="notif">
                                                <li class="header"><strong>Notificaciones</strong> (3) items</li>
                                                <li>
                                                    <a href="#">
                                                        <span class="icon"><span class="icon16 icomoon-icon-user-3"></span></span>
                                                        <span class="event">1 Pedido</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span class="icon"><span class="icon16 icomoon-icon-comments-4"></span></span>
                                                        <span class="event">1 Pecosa en Espera </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span class="icon"><span class="icon16 icomoon-icon-new-2"></span></span>
                                                        <span class="event">6 Despachos atendidos</span>
                                                    </a>
                                                </li>
                                                <li class="view-all"><a href="#">Ver Todas las notificaciones<span class="icon16 icomoon-icon-arrow-right-8"></span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Datos del Usuario -->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
                                        <img src="<?php echo URL_IMG; ?>avatar.jpg" alt="" class="image" /> 
                                        <span class="txt"> <?php echo $this->session->userdata('Nombres');
        ;
        ?> </span>
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="menu">
                                            <ul>                                       
                                                <li>
                                                    <a href="<?php echo base_url() . "usuario/edit/" . $this->session->userdata('IdUsuario');
                                            ;
        ?>"><span class="icon16 icomoon-icon-user-3"></span>Editar perfil</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() . "usuario/mensajes/" . $this->session->userdata('IdUsuario');
                                            ;
        ?>"><span class="icon16 icomoon-icon-comments-2"></span>Mensajes</a>
                                                </li>
                                                <!-- 
                                                <li>
                                                    <a href="#"><span class="icon16 icomoon-icon-plus-2"></span>Add user</a>
                                                </li>
                                                -->
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>usuario/logout">
                                        <span class="icon16 icomoon-icon-exit"></span> Logout
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div><!-- /navbar --> 

        </div><!-- End #header -->

        <div id="wrapper">

            <!--Responsive navigation button-->  
            <div class="resBtn">
                <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
            </div>

            <!--Left Sidebar collapse button-->  
            <div class="collapseBtn leftbar">
                <a href="#" class="tipR" title="Hide Left Sidebar"><span class="icon12 minia-icon-layout"></span></a>
            </div>

            <!--Sidebar background-->
            <div id="sidebarbg"></div>
            <!--Sidebar content-->
            <div id="sidebar">

                <div class="shortcuts">
                    <ul>
                        <li><a href="support.html" title="Support section" class="tip"><span class="icon24 icomoon-icon-support"></span></a></li>
                        <li><a href="#" title="Database backup" class="tip"><span class="icon24 icomoon-icon-database"></span></a></li>
                        <li><a href="charts.html" title="Sales statistics" class="tip"><span class="icon24 icomoon-icon-pie-2"></span></a></li>
                        <li><a href="#" title="Write post" class="tip"><span class="icon24 icomoon-icon-pencil"></span></a></li>
                    </ul>
                </div><!-- End search -->            

                <div class="sidenav">

                    <div class="sidebar-widget" style="margin: -1px 0 0 0;">
                        <h5 class="title" style="margin-bottom:0">Navigation</h5>
                    </div><!-- End .sidenav-widget -->

                    <div class="mainnav">
                        <ul>
                            <!--                 <li>
                                                <a href="#" class="hasUl drop">
                                                    <span class="icon16 icomoon-icon-grid"></span>Tables
                                                    <span class="hasDrop icon16 icomoon-icon-arrow-down-2"></span>
                                                </a>
                                                <ul class="sub" style="display: block;">
                                                    <li>
                                                        <a href="tables.html">
                                                            <span class="icon16 icomoon-icon-arrow-right-2"></span>Static
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="data-table.html">
                                                            <span class="icon16 icomoon-icon-arrow-right-2"></span>Data table
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li> -->                
                            <?php
                            $opciones = $this->loaders->get_menu();
                            // print_p($opciones);
                            $count = count($opciones);
                            for ($i = 0; $i < $count; $i++) {
                                // $OptSubmenu = ( !isset($opciones[$i]["active"])  ?"none":"block" );
                                $SubMenu = ( ( $opciones[$i]["active"] === 'block' ) ? " hasUl drop" : "" );
                                ;
                                ?>
                                <li >
                                    <a href="#" class="<?php echo $SubMenu;
                                   ;
                                ?> ">
                                        <span class="icon16 <?php echo $opciones[$i]["icon"];
                                          ;
                                          ?>"></span>
                                    <?php echo $opciones[$i]["menu"];
                                    ;
                                    ?>
                                        <!-- <span class="icon16 icomoon-icon-arrow-down-2"></span> -->
                                        <!-- <span class="hasDrop icon16 icomoon-icon-arrow-down-2"></span> -->
                                    </a>
                                        <?php $count2 = count($opciones[$i]["datos"]);
                                        ;
                                        ?>
                                    <ul class="sub" style="display:<?php echo $opciones[$i]["active"];
                                    ;
                                        ?>;">
                                        <?php
                                        for ($j = 0; $j < $count2; $j++) {
                                            ;
                                            ?>                             
                                            <li>
                                                <a href="<?php echo $opciones[$i]["datos"][$j]["url"]; ?>"><span class="icon16 icomoon-icon-arrow-right-2" ></span><?php echo $opciones[$i]["datos"][$j]["value"];
                            ;
                                            ?></a>
                                            </li>
    <?php };
    ?>
                                    </ul>
                                </li>
    <?php
}
;
?>
                        </ul>
                    </div>
                </div><!-- End sidenav -->

                <div class="sidebar-widget">
                    <h5 class="title">Presupuesto Comprometido</h5>
                    <div class="content">
                        <span class="icon16 icomoon-icon-loop left"></span>
                        <div class="progress progress-mini progress-danger left tip" title="87%">
                            <div class="bar" style="width: 87%;"></div>
                        </div>
                        <span class="percent">87%</span>
                        <div class="stat">19419.94 / 12000 $</div>
                    </div>

                </div><!-- End .sidenav-widget -->

                <!--<div class="sidebar-widget">
                    <h5 class="title">Meta Mensual Entrega</h5>
                    <div class="content">
                        <span class="icon16 icomoon-icon-drive left"></span>
                        <div class="progress progress-mini progress-success left tip" title="16%">
                            <div class="bar" style="width: 16%;"></div>
                        </div>
                        <span class="percent">25%</span>
                        <div class="stat">200/800 Ord.</div>
                    </div>

                </div>
            -->
            <div class="sidebar-widget">
                    <h5 class="title">Mes <?php echo date("m"); ?></h5>
                    <div class="content">
                        <div class="stats">
                            <div class="item" style="cursor:pointer;width: 80% !important;" onclick="set_popup('beneficiario/detalle_Beneficiario', 'Detalle Niños Edad Limite', '600', '600', '', '')">
                                <div class="head clearfix">
                                    <div class="txt">Niños Limite de edad</div>
                                </div>
                                <span class="icon16 ic  omoon-icon-eye left"></span>
                                <div class="number" id="div_ninos_edad_limite">
<?php //echo $this->loaders->ninosLimite(); ?>
                                </div>
                                <span id="stat1" class="spark"></span>
                            </div>
                        </div>

                    </div>

                </div><!-- End .sidenav-widget -->

                <div class="sidebar-widget">
                    <h5 class="title">Estadisticas de Sensibilización</h5>
                    <div class="content">

                        <div class="stats">
                            <div class="item">
                                <div class="head clearfix">
                                    <div class="txt">Nutricionista</div>
                                </div>
                                <span class="icon16 icomoon-icon-eye left"></span>
                                <div class="number">21,501</div>
                                <div class="change">
                                    <span class="icon24 icomoon-icon-arrow-up-2 green"></span>
                                    5%
                                </div>
                                <span id="stat1" class="spark"></span>
                            </div>
                            <div class="item">
                                <div class="head clearfix">
                                    <div class="txt">Productos</div>
                                </div>
                                <span class="icon16 icomoon-icon-thumbs-up left"></span>
                                <div class="number">308</div>
                                <div class="change">
                                    <span class="icon24 icomoon-icon-arrow-down-2 red"></span>
                                    8%
                                </div>
                                <span id="stat2" class="spark"></span>
                            </div>
                            <div class="item">
                                <div class="head clearfix">
                                    <div class="txt">Despachos</div>
                                </div>
                                <span class="icon16 icomoon-icon-heart left"></span>
                                <div class="number">4%</div>
                                <div class="change">
                                    <span class="icon24 icomoon-icon-arrow-down-2 red"></span>
                                    1%
                                </div>
                                <span id="stat3" class="spark"></span>
                            </div>
                            <div class="item">
                                <div class="head clearfix">
                                    <div class="txt">Meta</div>
                                </div>
                                <span class="icon16 icomoon-icon-coin left"></span>
                                <div class="number">$376</div>
                                <div class="change">
                                    <span class="icon24 icomoon-icon-arrow-up-2 green"></span>
                                    26%
                                </div>
                                <span id="stat4" class="spark"></span>
                            </div>
                        </div>

                    </div>

                </div><!-- End .sidenav-widget -->

                <div class="sidebar-widget">
                    <h5 class="title">Ahora Mismo</h5>
                    <div class="content">
                        <div class="rightnow">
                            <ul class="unstyled">
                                <li><span class="number">34</span><span class="icon16 icomoon-icon-new-2"></span>Stock</li>
                                <li><span class="number">7</span><span class="icon16 icomoon-icon-file"></span>Ordenes</li>
                                <li><span class="number">14</span><span class="icon16 icomoon-icon-list-view"></span>Clubes</li>
                                <li><span class="number">201</span><span class="icon16 icomoon-icon-tag"></span>Beneficiarios</li>
                            </ul>
                        </div>
                    </div>

                </div><!-- End .sidenav-widget -->

            </div><!-- End #sidebar -->

            <!--Body content-->
            <div id="content" class="clearfix">
                <div class="contentwrapper"><!--Content wrapper-->
                    <div class="heading">

                        <h3> <?php echo ucwords(strtolower((isset($modulo) ? $modulo : $this->uri->segment(1)))); ?> </h3>

                        <div class="resBtnSearch">
                            <a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
                        </div>

                        <div class="search">

                            <form id="searchform" action="search.html">
                                <input type="text" id="tipue_search_input" class="top-search" placeholder="Search here ..." />
                                <input type="submit" id="tipue_search_button" class="search-btn" value=""/>
                            </form>

                        </div><!-- End search -->

                        <ul class="breadcrumb">
                            <li>Tu te encuentras Aquí:</li>
                            <li>
                                <a href="#" class="tip" title="back to dashboard">
                                    <span class="icon16 icomoon-icon-screen-2"></span>
                                </a> 
                                <span class="divider">
                                    <span class="icon16 icomoon-icon-arrow-right-2"></span>
                                </span>
                            </li>
                            <li class="active"> <?php echo ucwords(strtolower(isset($opcion) ? $opcion : $this->uri->segment(2) )); ?></li>
                        </ul>

                    </div><!-- End .heading-->        