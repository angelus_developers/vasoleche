<script type="text/javascript" src="<?php echo URL_JS?>club/jsClubIns.js"></script>
    
    <div class="box">
        <div class="title">
            <h4> 
                <span>Ingresar Datos</span>
            </h4>
        </div>
        <div class="content">
            <form class="form-horizontal" action="#" id="frmClubIns" name="frmClubIns">
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtNombreClub">Nombre</label>
                            <input class="span10" type="text" id="txtNombreClub" name="txtNombreClub" size="" placeholder="Ejm: Maria de los Angeles" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtDireccionClub">Direccion</label>
                            <input class="span10" type="text" id="txtDireccionClub" name="txtDireccionClub" size="" placeholder="Ejm: Pasaje Olaya #132" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtTelefonoClub">Telefono</label>
                            <input class="span10" type="text" id="txtTelefonoClub" name="txtTelefonoClub" style="width: 150px;" placeholder="Ejm: 044-248741" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtCantBeneficiariosClub">Cantidad Beneficarios</label>
                            <input class="span10" type="text" id="txtCantBeneficiariosClub" name="txtCantBeneficiariosClub" style="width: 50px;" placeholder="52" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtCantidadLeche">Cantidad Leche</label>
                            <input class="span10" type="text" id="txtCantidadLeche" name="txtCantidadLeche" style="width: 200px;" size="" placeholder="Ejm: 452 bolsitarros x 410 gramos" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtCantidadAvena" >Cantidad Avena</label>
                            <input class="span10" type="text" id="txtCantidadAvena" name="txtCantidadAvena" style="width: 200px;" size="" placeholder="Ejm: 2550 Hojuelas x 1.0 kg" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info">Registrar</button>
                </div>

            </form>
        </div>
        <div id="msjClub">
            
        </div>
    </div>