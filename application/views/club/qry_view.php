<script type="text/javascript" src="<?php echo URL_JS; ?>club/jsClubQry.js" charset=UTF-8"></script>
<!-- Build page from here: Usual with <div class="row-fluid"></div> -->

<div class="row-fluid">
    <div class="span12">
        <div class="box gradient">
            <div class="content noPad clearfix">
                <!--<table id="Bandejaclub" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">-->
                <table id="Bandejaclub" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th>Beneficiarios</th>
                            <th>Cantidad Leche</th>
                            <th>Cantidad Avena</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($informacion as $data) { ?>
                            <tr>
                                <td><?php echo $data["nCluId"]; ?></td>
                                <td><?php echo $data["cCluNombre"]; ?></td>
                                <td><?php echo $data["cCluDireccion"]; ?></td>
                                <td><?php echo $data["cCluTelefono"]; ?></td>
                                <td><?php echo $data["nCluCantBeneficiados"]; ?></td>
                                <td><?php echo $data["nCluCantidadLeche"]; ?></td>
                                <td><?php echo $data["nCluCantidadAvena"]; ?></td>
                                <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                                <td>
                                    <a style="cursor: pointer;" onclick="set_popup('club/panel_updClub', 'Editar Club', '600', '600', '<?php echo htmlspecialchars(json_encode(array("nClubId" => $data['nCluId']))); ?>', '')">
                                        <img title="Editar" alt="x" src="<?php echo URL_IMG; ?>iconedit.png" width="20" height="20" />
                                    </a>
                                </td>
                                <td>
                                    <?php if ($data["cCluEstado"] == "A") { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Club','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarClub(<?php echo $data['nCluId']; ?>)')" />
                                        </a>
                                    <?php } else { ?>
                                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Club','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarClub(<?php echo $data['nCluId']; ?>)')" />                                   
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th>Beneficiarios</th>
                            <th>Cantidad Leche</th>
                            <th>Cantidad Avena</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div><!-- End .row-fluid -->