<script type="text/javascript" src="<?php echo URL_JS ?>club/jsClubUpd.js"></script>

<div class="box">
    <div class="title">
        <h4> 
            <span>Actualizar Datos</span>
        </h4>
    </div>
    <div class="content">

        <form class="form-horizontal" action="#" id="frmClubUpd" name="frmClubUpd">
            <input type="hidden" name="hdnidClub_upd" id="hdnidClub_upd" value="<?php echo $informacion[0]['nCluId'];?>"/>
            <!--<input type="text" name="hdnidClub_upd" id="hdnidClub_upd" value="<php echo $informacion[0]['nCluId']; ?>"/>-->
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtNombreClub_upd">Nombre</label>
                        <input class="span10" value="<?php echo $informacion[0]['cCluNombre']; ?>" type="text" id="txtNombreClub_upd" name="txtNombreClub_upd" size="" placeholder="Ejm: Maria de los Angeles" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtDireccionClub_upd">Direccion</label>
                        <input class="span10" value="<?php echo $informacion[0]['cCluDireccion']; ?>" type="text" id="txtDireccionClub_upd" name="txtDireccionClub_upd" size="" placeholder="Ejm: Pasaje Olaya #132" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtTelefonoClub_upd">Telefono</label>
                        <input class="span10" value="<?php echo $informacion[0]['cCluTelefono']; ?>" type="text" id="txtTelefonoClub_upd" name="txtTelefonoClub_upd" style="width: 150px;" placeholder="Ejm: 044-248741" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtCantBeneficiariosClub_upd">Cantidad Beneficarios</label>
                        <input class="span10" value="<?php echo $informacion[0]['nCluCantBeneficiados']; ?>" type="text" id="txtCantBeneficiariosClub_upd" name="txtCantBeneficiariosClub_upd" style="width: 50px;" placeholder="52" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtCantidadLeche">Cantidad Leche</label>
                        <input class="span10" value="<?php echo $informacion[0]['nCluCantidadLeche']; ?>" type="text" id="txtCantidadLeche_upd" name="txtCantidadLeche_upd" style="width: 200px;" size="" placeholder="Ejm: 452 bolsitarros x 410 gramos" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtCantidadAvena" >Cantidad Avena</label>
                        <input class="span10" value="<?php echo $informacion[0]['nCluCantidadAvena']; ?>" type="text" id="txtCantidadAvena_upd" name="txtCantidadAvena_upd" style="width: 200px;" size="" placeholder="Ejm: 2550 Hojuelas x 1.0 kg" />
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjClub">

    </div>
</div>