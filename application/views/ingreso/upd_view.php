<script type="text/javascript" src="<?php echo URL_JS ?>ingreso/jsIngresoUpd.js"></script>
<div class="box">
    <div class="title">
        <h4> 
            <span>Actualizar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmIngresoUpd" name="frmIngresoUpd">
            <input type="hidden" name="hdnidIngreso_upd" id="hdnidIngreso_upd" value="<?php echo $informacion[0]['nIngId']; ?>"/>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Responsable</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersonaUpd" id="nTipoPersonaUpd" class="nostyle" style="width:100%;" placeholder="Selecciona Persona">
                                <option></option>
                                <?php foreach ($persona as $persona) { ?>
                                    <?php if ($persona["nPerId"] == $informacion[0]['nPerId']) { ?>
                                        <option value="<?php echo $persona["nPerId"] ?>" selected="selected"><?php echo $persona["datospersona"] ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtFechaUpd">Fecha de Ingreso</label>
                        <input class="span10" type="text" id="txtFechaUpd" name="txtFechaUpd" size="" placeholder="Ejm: 04/08/2015" value="<?php echo $informacion[0]['tIngFechaIngreso'];?>" />
                    </div>
                </div>
            </div>
            

            <div class="form-actions">
                <button type="submit" class="btn btn-info">Actualizar</button>
            </div>

        </form>
    </div>
    <div id="msjActualizar">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        set_Date("txtFechaUpd",'ALL');
        $("#nTipoPersonaUpd").select2();
    })
    
    //$("#select2").select2();
</script>