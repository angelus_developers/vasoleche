<?php
//echo "id del idIngreso:".$idIngreso;
//nImsId, cImsNombre, nImsUnidadMedida, tImsFechaRegistro, tImsFechaVencimiento, cImsEstado, nImsTipo, cImsDescripcion, nImsCantidad
?>
<script type="text/javascript" src="<?php echo URL_JS ?>ingreso/detalleIngreso/jsDetalleIngresoIns.js"></script>
<style>
    #idDespachoDetCabecera span{font-weight: bold;font-size: small; color: #003399;}
</style>
<?php
//print_p($getDespacho);
//exit;
?>


<?php if ($idIngreso != "") { ?>
    <div class="box">
        <div class="title">
            <h4> 
                <span>Ingresar Detalle</span>
            </h4>
        </div>
        <div class="content" id="idDespachoDetCabecera">
            <span>IdIngreso: </span><?php echo $idIngreso; ?><br/>
            <span>Responsable: </span><?php echo $getIngreso[0]['datospersona'] ?><br/>
            <span>Fecha Ingreso: </span><?php echo $getIngreso[0]['tIngFechaIngreso'] ?><br/>
        </div>
        <div class="content">
            <form class="form-horizontal" action="#" id="frmDetalleIngresoIns" name="frmDetalleIngresoIns" method="post">
                <input type="hidden" id="idIngreso_ins" name="idIngreso_ins" value="<?php echo $idIngreso; ?>" />
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2">Insumo</label>
                            <div class="span8 controls">   
                                <select name="nTipoInsumo_ins" id="nTipoInsumo_ins" style="width:40%;" placeholder="Selecciona Insumo">
                                    <?php foreach ($Insumo as $Insumo) {
                                        ?>
                                        <option value="<?php echo $Insumo["nImsId"] ?>"><?php echo $Insumo["cImsNombre"] ?></option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>
                    </div> 
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtCantidadIngreso_ins">Cantidad</label>
                            <input class="span10" type="text" id="txtCantidadIngreso_ins" name="txtCantidadIngreso_ins" size="" placeholder="Ejm: 250 unidades" />
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span2" for="txtObservacionesIngreso_ins">Observaciones</label>
                            <textarea cols="20" rows="10" id="txtObservacionesIngreso_ins" name="txtObservacionesIngreso_ins" style="width: 615px;max-width: 615px;min-width: 615px;height: 80px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info">Registrar</button>
                </div>

            </form>
        </div>
        <div id="msjDetalleIngreso">

        </div>
    </div>

    <div class="box">
        <div class="title">
            <h4> 
                <span>Listado-Detalle</span>
            </h4>
        </div>
        <div class="content" id="listadoDetalleIngreso">
            
            <?php if($getDetalleIngreso){ ?>
            <table id="BandejaIngreso" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Insumo</th>
                        <th>Cantidad</th>
                        <th>Observacion</th>
                        <th>Fecha</th>
                        <th>Stock</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($getDetalleIngreso as $data) { ?>
                        <tr>
                            <td><?php echo $data["nDetIngId"]; ?></td>
                            <td><?php echo $data["nombreProducto"]; ?></td>
                            <td><?php echo $data["nDetcantidad"]; ?></td>
                            <td><?php echo $data["cDetdescripcion"]; ?></td>
                            <td><?php echo $data["fechaRegistroDetalle"]; ?></td>
                            <?php if($data["stock"]=="0"){
                                echo "<td style='background-color: red'>".$data['stock']."</td>";
                            }else{
                                echo "<td>".$data['stock']."</td>";
                            }
                            ?>
                            <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                            <td>
                                <?php if ($data["nDetEstado"] == 1) { ?>
                                        <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                                    <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Desactivar','<center><span>Desea Desactivar el registro seleccionado?</span></center>','eliminarDetalleIngresoQuita(<?php echo $data['nDetIngId']; ?>,<?php echo $data['nDetcantidad']; ?>,<?php echo $data['nImsId']; ?>)')" />
                                    </a>
                                <?php } else { ?>
                                    <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Activar','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDetalleIngresoAgrega(<?php echo $data['nDetIngId']; ?>,<?php echo $data['nDetcantidad']; ?>,<?php echo $data['nImsId']; ?>)')" />
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{
                echo "no hay datos";
            }
?>
        </div>
    </div>

<?php
} else {
    echo "No se pudo realizar el detalle-ingreso porque no existe un ingreso previo";
}
?>