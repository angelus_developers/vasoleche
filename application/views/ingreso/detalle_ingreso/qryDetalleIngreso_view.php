<script type="text/javascript" src="<?php echo URL_JS ?>ingreso/detalleIngreso/jsDetalleIngresoIns.js"></script>

<table id="BandejaIngreso" cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>Insumo</th>
            <th>Cantidad</th>
            <th>Observacion</th>
            <th>Fecha</th>
            <th>Stock</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($getDetalleIngreso as $data) { ?>
            <tr>
                <td><?php echo $data["nDetIngId"]; ?></td>
                <td><?php echo $data["nombreProducto"]; ?></td>
                <td><?php echo $data["nDetcantidad"]; ?></td>
                <td><?php echo $data["cDetdescripcion"]; ?></td>
                <td><?php echo $data["fechaRegistroDetalle"]; ?></td>
                <?php
                if ($data["stock"] == "0") {
                    echo "<td style='background-color: red'>" . $data['stock'] . "</td>";
                } else {
                    echo "<td>" . $data['stock'] . "</td>";
                }
                ?>
                <!--<td><a class="toolmensajito" title="Editar Obj.Estrategico" id="Convocatoria-Lista-VerDetalles" onclick="show_popup_objestrategico_editar('<php echo $dato->idObjEstrategico ?>');"><img title="Editar Objetivo Especifico" alt="x" src="<php echo base_url() ?>imagenes/images/iconedit.png" width="20" height="20" /></a></td>-->
                <td>
    <?php if ($data["nDetEstado"] == 1) { ?>
                                <!--<a style="cursor: pointer;"><img title='Activo' src='<php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="ConfirmaquitaInstitucion(<php echo $data['nCluId']; ?>)" />-->
                        <a style="cursor: pointer;"><img title='Activo' src='<?php echo URL_IMG; ?>iconok.png' width='20' height='20' onClick="confirmar('Desactivar','<center><span>Desea Desactivar el registro seleccionado?</span></center>','eliminarDetalleIngresoQuita(<?php echo $data['nDetIngId']; ?>,<?php echo $data['nDetcantidad']; ?>,<?php echo $data['nImsId']; ?>)')" />
                        </a>
    <?php } else { ?>
                        <a style="cursor: pointer;"><img title='Inactivo' src='<?php echo URL_IMG; ?>iconquit.png' width='20' height='20' onClick="confirmar('Activar','<center><span>Desea Activar el registro seleccionado?</span></center>','eliminarDetalleIngresoAgrega(<?php echo $data['nDetIngId']; ?>,<?php echo $data['nDetcantidad']; ?>,<?php echo $data['nImsId']; ?>)')" />
                        </a>
    <?php } ?>
                </td>
            </tr>
<?php } ?>
    </tbody>
</table>