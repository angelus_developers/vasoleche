<script type="text/javascript" src="<?php echo URL_JS ?>ingreso/jsIngresoIns.js"></script>
<div class="box">
    <div class="title">
        <h4> 
            <span>Ingresar Datos</span>
        </h4>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#" id="frmIngresoIns" name="frmIngresoIns">
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="checkboxes">Responsable</label>
                        <div class="span8 controls">   
                            <select name="nTipoPersona" id="nTipoPersona" class="nostyle" style="width:100%;" placeholder="Selecciona Persona">
                                <option></option>
                                <?php foreach ($persona as $persona) {
                                    ?>
                                    <option value="<?php echo $persona["nPerId"] ?>"><?php echo $persona["datospersona"] ?></option>
                                <?php } ?>
                            </select>
                        </div> 
                    </div>
                </div> 
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span2" for="txtFechaIng">Fecha de Ingreso</label>
                        <input class="span10" type="text" id="txtFechaIng" name="txtFechaIng" size="" placeholder="Ejm: 04/08/2015" />
                    </div>
                </div>
            </div>
            

            <div class="form-actions">
                <button type="submit" class="btn btn-info">Registrar</button>
            </div>

        </form>
    </div>
    <div id="msjIngreso">

    </div>
</div>
<script type="text/javascript">
    $(function(){
        set_Date("txtFechaIng",'ALL');
        $("#nTipoPersona").select2();
    })
    
    //$("#select2").select2();
</script>