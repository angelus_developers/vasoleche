<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Persona extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //Do your magic here
        $this->load->model('persona_model', 'ObjPersona');
    }

    public function index() {
        $this->loaders->verificaacceso();
        // $data['title'] = "Gestion y registro de Personas ";
        $this->load->view('layout/header');
        $this->load->view('persona/panel_view');
        $this->load->view('layout/footer');
    }

    public function registrarIns() {
        $this->ObjPersona->set_cPerNombres($this->input->post('txtcPerNombres'));
        $this->ObjPersona->set_cPerApellidoPaterno($this->input->post('txtcPerApellidoPaterno'));
        $this->ObjPersona->set_cPerApellidoMaterno($this->input->post('txtcPerApellidoMaterno'));
        $this->ObjPersona->set_cPerDni($this->input->post('txtcPerDni'));
        $this->ObjPersona->set_cPerDireccion($this->input->post('txtcPerDireccion'));
        $this->ObjPersona->set_cPerTelefono($this->input->post('txtcPerTelefono'));
        $this->ObjPersona->set_cPerCelular($this->input->post('txtcPerCelular'));

        $rs = $this->ObjPersona->registrarPersonaIns();
        if ($rs) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function qryPersona(){
        $data['informacion'] = $this->ObjPersona->qryPersona();        
        $this->load->view("persona/qry_view",$data);
    }
    function eliminarPersona() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->ObjPersona->eliminarPersona($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }
    public function panel_updPersona(){ //falta
        $algo= json_decode($_POST["json"]);
        //$idclub = $algo->nClubId;
        $nPerId = $algo->nPerId;
        $data["informacion"] = $this->ObjPersona->getDatos($nPerId);
        //$data["tipoComite"] =$this->comite_model->getCboTipoComite();
        //$data["tipoClub"] =$this->club_model->getCboTipoClub();
        //$data["persona"]= $this->persona_model->getCboPersonas();
        //print_r($data);
        //
        $this->load->view("persona/upd_view",$data);
    }
    public function updPersona(){
        //print_r($_POST);
        $this->ObjPersona->set_cPerNombres($this->input->post('txtcPerNombresUpd'));
        $this->ObjPersona->set_cPerApellidoPaterno($this->input->post('txtcPerApellidoPaternoUpd'));
        $this->ObjPersona->set_cPerApellidoMaterno($this->input->post('txtcPerApellidoMaternoUpd'));
        $this->ObjPersona->set_cPerDni($this->input->post('txtcPerDniUpd'));
        $this->ObjPersona->set_cPerDireccion($this->input->post('txtcPerDireccionUpd'));
        $this->ObjPersona->set_cPerTelefono($this->input->post('txtcPerTelefonoUpd'));
        $this->ObjPersona->set_cPerCelular($this->input->post('txtcPerCelularUpd'));
        
        //$resultado = $this->club_model->updClub($hdnidClub);
        $resultado = $this->ObjPersona->updPersona($this->input->post('hdnidPersona_upd'));
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function buscarxDni(){
        $this->ObjPersona->set_cPerDni( $this->input->post('dni') );
        $data['person'] = $this->ObjPersona->buscaxDniGet();
        $this->load->view('usuario/qry_personas_select', $data);
    }    
  

}

/* End of file persona.php */
/* Location: ./application/controllers/persona.php */
?>