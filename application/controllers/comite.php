<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comite extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('club_model');
        $this->load->model('comite_model');
        $this->load->model('persona_model');
    }

    public function index() {
        $this->load->view('layout/header');
        
        $data["hi"] = "hola";
        $data["title"] = "Gestion de Comite";
        $data["modulo"] = "Comite";
        $data["opcion"] = $data["title"];
        
        //$data["tipoComite"] =$this->crearObras_model->vistapreviaTramos($nobIdObra);
        $data["tipoComite"] =$this->comite_model->getCboTipoComite();
        $data["tipoClub"] =$this->club_model->getCboTipoClub();
        $data["persona"]= $this->persona_model->getCboPersonas();
        
        //$data['tipodocumento'] = $this->crearDocumento_model->get_s_cbo_tipodocumento_simple($accion);
        
        $this->load->view("comite/panel_view",$data);
        
        $this->load->view('layout/footer');
    }
    public function insComite(){
        $this->comite_model->set_cComTipo($this->input->post("nTipoComite"));
        $this->comite_model->set_nCluId($this->input->post("nTipoClub"));
        $this->comite_model->set_nPerId($this->input->post("nTipoPersona"));
        $resultado = $this->comite_model->insComite();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function qryComite(){
        //$data['informacion'] = $this->club_model->qryClub();
        $data['informacion'] = $this->comite_model->qryComite();
        $this->load->view("comite/qry_view",$data);
    }
    public function panel_updComite(){ //falta
        $algo= json_decode($_POST["json"]);
        //$idclub = $algo->nClubId;
        $idcomite = $algo->nComId;
        $data["informacion"] = $this->comite_model->getDatos($idcomite);
        $data["tipoComite"] =$this->comite_model->getCboTipoComite();
        $data["tipoClub"] =$this->club_model->getCboTipoClub();
        $data["persona"]= $this->persona_model->getCboPersonas();
        //print_r($data);
        $this->load->view("comite/upd_view",$data);
    }
    public function updComite(){ //falta
        $hdnidComite = $this->input->post("hdnidComite_upd");
        
        //print_r($_POST);
        //exit;
        //$txtNombreClub = $this->input->post("txtNombreClub");
        
        $this->comite_model->set_cComTipo($this->input->post("nTipoComiteUpd"));
        $this->comite_model->set_nCluId($this->input->post("nTipoClubUpd"));
        $this->comite_model->set_nPerId($this->input->post("nTipoPersonaUpd"));

        $resultado = $this->comite_model->updComite($hdnidComite);
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    //
    function eliminarComite() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->club_model->eliminarClub($ncodigo);
        $datos = $this->comite_model->eliminarComite($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }
    
}
?>