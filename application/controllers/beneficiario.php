<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Beneficiario extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('beneficiario_model');
        $this->load->model('club_model');
        $this->load->model('persona_model');
    }

    public function index() {
        //$this->loaders->verificaacceso();
        $this->load->view('layout/header');
        
        echo "hola benefiarios";

        $data["title"] = "Gestion de Beneficiarios";
        $data["modulo"] = "Beneficiarios";
        $data["opcion"] = $data["title"];
        $data["persona"]= $this->persona_model->getCboPersonas();
        $data["personamadre"]= $this->persona_model->getCboPersonas();
        $data["tipoClub"] =$this->club_model->getCboTipoClub();
        
        $this->load->view("beneficiario/panel_view",$data);
        
        $this->load->view('layout/footer');
    }
    public function insBeneficiario(){
        //print_r($_POST);
        
        //$this->beneficiario_model->set_cBenPartidaNacimiento($this->input->post("txtPartidaNacimientoBen"));
        $this->beneficiario_model->set_persona_nPerId($this->input->post("nTipoPersonaNino"));//nperid beneficiario
        $this->beneficiario_model->set_cBenSexo($this->input->post("cboSexoBen"));//nperid beneficiario
        $this->beneficiario_model->set_cBenFechaNacimiento($this->input->post("txtFechaNacBen"));//nperid beneficiario
        $this->beneficiario_model->set_nPerIdMadre($this->input->post("nTipoPersonaMadre"));//nperid beneficiario
        $this->beneficiario_model->set_nClubId($this->input->post("nTipoClub"));//nperid beneficiario
        
        $resultado = $this->beneficiario_model->insBeneficiario();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function qryBeneficiario(){
        //print_r(retornaedad("2005-04-08"));
        //print_r(retornaedad("2012-01-16"));
        //print_r(retornaedad("2012-01-16"));
        $data['informacion'] = $this->beneficiario_model->qryBeneficiario();
        $this->load->view("beneficiario/qry_view",$data);
    }
    public function panel_updBeneficiario(){
        $algo= json_decode($_POST["json"]);
        $idben = $algo->nBenId;
        $data["informacion"] = $this->beneficiario_model->getDatos($idben);
        //print_r($data);
        $data["persona"]= $this->persona_model->getCboPersonas();
        $data["personamadre"]= $this->persona_model->getCboPersonas();
        $data["tipoClub"] =$this->club_model->getCboTipoClub();
        
        $this->load->view("beneficiario/upd_view",$data);
    }
    public function updBeneficiario(){
        //print_r($_POST);
        $this->beneficiario_model->set_nBenId($this->input->post("hdnidBenUpd"));
        $this->beneficiario_model->set_persona_nPerId($this->input->post("nTipoPersonaNino"));//
        $this->beneficiario_model->set_cBenSexo($this->input->post("cboSexoBen"));//nperid beneficiario
        $this->beneficiario_model->set_cBenFechaNacimiento($this->input->post("txtFechaNacBen"));//nperid beneficiario
        $this->beneficiario_model->set_nPerIdMadre($this->input->post("nTipoPersonaMadre"));//nperid beneficiario
        $this->beneficiario_model->set_nClubId($this->input->post("nTipoClub"));//nperid beneficiario
        //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
        $resultado = $this->beneficiario_model->updBeneficiario();
        //echo $resultado;
        //exit;
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    function eliminarBeneficiario() {
        $ncodigo = $this->input->post('ncodigo');
        $datos = $this->beneficiario_model->eliminarBeneficiario($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }
    
    function cantEdadNinosLimite(){
        $data['informacion'] = $this->beneficiario_model->get_ninosLimite();
        //$cantidadLimite
        $cant=0;
        foreach($data['informacion'] as $datito){
            //print_r($datito);
            
            $cadena_fecha=retornaedad($datito["anio"]."-".$datito["mes"]."-".$datito["dia"]);
            
            if($cadena_fecha[0]>=8 or ($cadena_fecha[0]==7 and $cadena_fecha[1]==11)){
                $cant++;
            }
            /*echo "<br/>";
            print_r($cadena_fecha[0]);
            echo " ";
            print_r($cadena_fecha[1]);
            echo "<br/>";*/
        }
        echo "<h2>".$cant."</h2>";
        //print_r($data['informacion'][0]["cantidad"]);
        //$this->load->view("beneficiario/qry_view",$data);
    }
    function detalle_Beneficiario(){
        $data['informacion'] = $this->beneficiario_model->qryBeneficiario();
        $this->load->view("beneficiario/qry_view_edadLimite",$data);
    }
    
    
    
    
}
?>