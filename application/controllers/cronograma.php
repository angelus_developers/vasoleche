<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronograma extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cronograma_model','objCronograma');
		$this->load->model('insumo_model','objInsumos');
	}

	public function index()
	{
		$this->loaders->verificaacceso();
		$this->load->view('layout/header');
		$data['cboInsumos'] = $this->objInsumos->getCboInsumosAnio();
		$this->load->view('cronograma/panel_view',$data);
		$this->load->view('layout/footer');
	}
	public function trt(){
		// print_p($this->objInsumos->getCboInsumos());
		list($a,$m,$d) = explode('-', date('Y-m-d',strtotime('2012-5-23'))) ;
		print_p($a);
		print_p($m);
		print_p($d);
		// $hoy = $fecha['mon']+1;
	}
	public function generarCronogramaIns(){
		$info['codinsumo'] = $this->input->post('insum');
		$info['codusu'] = $this->session->userdata('IdUsuario');
		$rs = $this->objCronograma->cronogramaIns($info);
		// print_p($rs);
		$this->session->set_userdata($rs);
		$info['idcrono'] = $rs['idcrono'];
		$fecha = getdate();
		$mes = $fecha['mon']+1;$anio = $fecha['year'];$day = $fecha['mday'];
		for ($i=0; $i < 12; $i++) { 
			if ($mes > 12) {
				$mes = 1;
				$anio++;
			}
			// print $i;
			$this->objCronograma->insertaDetalle($info['idcrono'],$anio,$mes,$day);
			$mes++;
		}
		$rs['detaCrono'] = $this->objCronograma->listadetallescronogr($info['idcrono']);
		$this->load->view('cronograma/detalle_cronograma_view', $rs);
	}
	public function updateDetalle(){
		$name = $this->input->post('name');
		$value = $this->input->post('value');
		$pk = $this->input->post('pk');
		$sets="";
		switch ($name) {
			case 'fecha':
			list($anio,$mes,$dia) = explode('-', $value);
			$sets = " tDetCroAnio=$anio,tDetCroMes=$mes,tDetCroDia=$dia ";
			break;
			default:
			$sets = " $name = $value ";
			break;
		}
		$sql = "UPDATE detallecronograma SET ".$sets." where nDetCroId = $pk ";
		$this->objCronograma->actualizaDetalles($sql);
		// echo $sql;
	}
	public function qryCronograma(){
		$data['cronos'] = $this->objCronograma->listaCronogramas();
		// print_p($data);
		$this->load->view('cronograma/cronograma_qry_view', $data);
	}
	public function verDetalleCronograma(){
		$info['codinsumo'] = $this->input->post('codInsumo');
		$info['idcrono'] = $this->input->post('codCronograma');
		$this->session->set_userdata('idcrono',$info['idcrono']);

		$rs['detaCrono'] = $this->objCronograma->listadetallescronogr($info['idcrono']);
		$this->load->view('cronograma/detalle_cronograma_view', $rs);
	}
	public function comboGet(){
		$cboInsumos = $this->objInsumos->getCboInsumosAnio();
		// var_dump($cboInsumos);
		if ($cboInsumos) {
			$html = '<select name="cbo_insumos" id="cbo_insumos">';
			foreach ($cboInsumos as $fila) {
				$html.="<option value=\"".$fila['nImsId']."\">".$fila['cImsNombre']."</option>";
			}
			$html.='</select>';
		}else{
			$html = '<select name="cbo_insumos" id="cbo_insumos">';
				$html.="<option value=\"x\">Sin Insumos</option>";
			$html.='</select>';			
		}		
		echo $html;
	}
}

/* End of file cronograma.php */
/* Location: ./application/controllers/cronograma.php */
?>