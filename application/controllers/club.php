<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Club extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('club_model');
    }

    public function index() {
        //$this->loaders->verificaacceso();
        $this->load->view('layout/header');
        
        $data["hi"] = "hola";
        $data["title"] = "Gestion de Clubes";
        $data["modulo"] = "Clubes";
        $data["opcion"] = $data["title"];
        $this->load->view("club/panel_view",$data);
        
        $this->load->view('layout/footer');
    }
    public function insClub(){
//        print_r($_POST);
        
        $txtNombreClub = $this->input->post("txtNombreClub");
        $txtDireccionClub = $this->input->post("txtDireccionClub");
        $txtTelefonoClub = $this->input->post("txtTelefonoClub");
        $txtCantBeneficiariosClub = $this->input->post("txtCantBeneficiariosClub");
        $txtCantidadLeche = $this->input->post("txtCantidadLeche");
        $txtCantidadAvena = $this->input->post("txtCantidadAvena");
        
        //$this->club_model->
                
        $this->club_model->set_cCluNombre($txtNombreClub);
        $this->club_model->set_cCluDireccion($txtDireccionClub);
        $this->club_model->set_cCluTelefono($txtTelefonoClub);
        $this->club_model->set_nCluCantBeneficiados($txtCantBeneficiariosClub);
        $this->club_model->set_nCluCantidadLeche($txtCantidadLeche);
        $this->club_model->set_nCluCantidadAvena($txtCantidadAvena);

        //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
        $resultado = $this->club_model->insClub();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function qryClub(){
        
        //$datos = $this->club_model->verListaObjEstrategico();
        //echo "hola";
        $data['informacion'] = $this->club_model->qryClub();
        /*echo "<pre>";
        print_r($datos);
        echo "</pre>";*/
        
        $this->load->view("club/qry_view",$data);
        //$data['informacion'] = $datos;
        //$this->load->view('procesos/moduloObjEstrategicosv/listados/verListaObjEstrategico', $data);
    }
    public function panel_updClub(){
        //print_r($_POST["json"]);
        $algo= json_decode($_POST["json"]);
        $idclub = $algo->nClubId;
        //print_r($algo->nClubId);
        
        $data["informacion"] = $this->club_model->getDatos($idclub);
        //print_r($data);
        $this->load->view("club/upd_view",$data);
    }
    public function updClub(){
//        print_r($_POST);
        $hdnidClub = $this->input->post("hdnidClub");
        $txtNombreClub = $this->input->post("txtNombreClub");
        $txtDireccionClub = $this->input->post("txtDireccionClub");
        $txtTelefonoClub = $this->input->post("txtTelefonoClub");
        $txtCantBeneficiariosClub = $this->input->post("txtCantBeneficiariosClub");
        $txtCantidadLeche = $this->input->post("txtCantidadLeche");
        $txtCantidadAvena = $this->input->post("txtCantidadAvena");
        
        //$this->club_model->
                
        $this->club_model->set_cCluNombre($txtNombreClub);
        $this->club_model->set_cCluDireccion($txtDireccionClub);
        $this->club_model->set_cCluTelefono($txtTelefonoClub);
        $this->club_model->set_nCluCantBeneficiados($txtCantBeneficiariosClub);
        $this->club_model->set_nCluCantidadLeche($txtCantidadLeche);
        $this->club_model->set_nCluCantidadAvena($txtCantidadAvena);

        //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
        $resultado = $this->club_model->updClub($hdnidClub);
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    //
    function eliminarClub() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->club_model->eliminarClub($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }
    
}
?>