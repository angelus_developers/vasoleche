<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Despacho extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('despacho_model');
        $this->load->model('insumo_model');
        $this->load->model('club_model');
        $this->load->model('persona_model');
    }

    public function index() {
        //$this->loaders->verificaacceso();
        $this->load->view('layout/header');

        $data["hi"] = "hola";
        $data["title"] = "Gestion de Despacho";
        $data["modulo"] = "Despacho";
        $data["opcion"] = $data["title"];
        //
        $data["tipoClub"] = $this->club_model->getCboTipoClub();
        $data["persona"] = $this->persona_model->getCboPersonas();
        $this->load->view("despacho/panel_view", $data);

        $this->load->view('layout/footer');
    }

    public function registrarIns() {
        //print_r($_POST);
        extract($_POST);
        //echo "tenemos: nombre:".$txtNombreInsumo." unidad:".$txtUnidadInsumo;
        $this->despacho_model->set_nDesResponsable($nTipoPersona);
        $this->despacho_model->set_nCluId($nTipoClub);
        $this->despacho_model->setCDesMes($nDesMes);
        $this->despacho_model->setCDesAnio($nDesAnio);
        $resultado = $this->despacho_model->insDespacho();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function qryDespacho() {
        $data['informacion'] = $this->despacho_model->qryDespacho();
        /* echo "<pre>";
          print_r($datos);
          echo "</pre>"; */
        $this->load->view("despacho/qry_view", $data);
    }

    function eliminarDespacho() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->despacho_model->eliminarDespacho($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }

    public function panel_updDespacho() {
        //print_r($_POST["json"]);
        $algo = json_decode($this->input->post("json"));
        $iddespacho = $algo->nDesId;
        //print_r($algo->nClubId);
        $data["tipoClub"] = $this->club_model->getCboTipoClub();
        $data["persona"] = $this->persona_model->getCboPersonas();

        $data["informacion"] = $this->despacho_model->getDatosDespacho($iddespacho);
        //print_r($data);
        //exit;

        $this->load->view("despacho/upd_view", $data);
    }

    public function updDespacho() {
        extract($_POST);
        //$hdnidClub = $this->input->post("hdnidClub");
        //echo "tenemos: nombre:".$txtNombreInsumo." unidad:".$txtUnidadInsumo;
        //exit;
        $this->despacho_model->set_nDesId($hdnidDespacho_upd);
        $this->despacho_model->set_nDesResponsable($nTipoPersonaUpd);
        $this->despacho_model->set_nCluId($nTipoClubUpd);
        $this->despacho_model->setCDesMes($nDesMesUpd);
        $this->despacho_model->setCDesAnio($nDesAnioUpd);

        //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
        //$resultado = $this->club_model->updClub($hdnidClub);
        $resultado = $this->despacho_model->updDespacho();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function panel_detalleDespacho() {
        $algo = json_decode($_POST["json"]);
        $idDespacho = $algo->nDesId;
        /* echo "id del insumo:".$idDespacho;
          exit; */
        //$data["informacion"] = $this->insumo_model->getDatos($idinsumo);

        $data["Insumo"] = $this->insumo_model->getCboInsumos();
        $data['idDespacho'] = $idDespacho;
        $data["getDespacho"] = $this->despacho_model->getDespacho($data['idDespacho']);
        $data["getDetalleDespacho"] = $this->despacho_model->getDetalleDespacho($data['idDespacho']);

        $this->load->view("despacho/detalle_despacho/addDetalle_view", $data);
    }

    function registrarDetalleDespachoIns() {
        //print_r($_POST);
        extract($_POST);
        $idDespacho_ins = $idDespacho_ins;
        $idproducto = $nTipoInsumo_ins;
        $txtCantidadDespacho_ins = $txtCantidadDespacho_ins;
        $txtObservacionesDespacho_ins = $txtObservacionesDespacho_ins;
        //echo "siendo el producto:".$idproducto." cantidad:".$txtCantidadDespacho_ins." txtObservacionesDespacho_ins".$txtObservacionesDespacho_ins;
        //exit;
        if ($idDespacho_ins == "" or $idproducto == "") {
            echo "inconveniente";
        } else {
            if ($txtCantidadDespacho_ins >= 1) {
                $cant = $this->insumo_model->getCantidadInsumos($idproducto);
                $cantidadActual = $cant[0]["nImsCantidad"];
                if ($cantidadActual >= $txtCantidadDespacho_ins) {
                    $this->insumo_model->set_nImsId($idproducto);
                    $this->insumo_model->set_nImsCantidad($txtCantidadDespacho_ins);
                    //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
                    //$resultado = $this->club_model->updClub($hdnidClub);
                    $resultado1 = $this->insumo_model->ActualizaStock($idDespacho_ins, $txtObservacionesDespacho_ins);
                    if ($resultado1) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                } else {
                    echo "insuficiente";
                }
                //echo "cantidad Actual:".$cantidadActual;
            } else {
                echo "incorrecto";
            }
        }
    }

    function listarDetalleDespachoxIdDespacho() {
        $idDespacho_ins = $this->input->post("idDespacho_ins");
        $data["getDetalleDespacho"] = $this->despacho_model->getDetalleDespacho($idDespacho_ins);
        $this->load->view("despacho/detalle_despacho/qryDetalleDespacho_view", $data);
    }

    function eliminarDetalleDespacho() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->despacho_model->eliminarDetalleDespacho($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }

    /* 10/06 */

    public function panel_updDetalleDespacho() {
        $algo = json_decode($_POST["json"]);
        $iddetalleDespacho = $algo->nDetDesId;

        $data["Insumo"] = $this->insumo_model->getCboInsumos();

        $data["getdatosDetalleDespacho"] = $this->despacho_model->getdatosDetalleDespacho($iddetalleDespacho);

        /**/
        $this->load->view("despacho/detalle_despacho/upd_viewDetalle", $data);
    }

    function updDetalleDespacho() {
        //print_r($_POST);
        extract($_POST);
        $idDetalleDespacho = $idDetalleDespacho;
        $idDespacho_upd = $idDespacho_upd;
        $nTipoInsumo_upd = $nTipoInsumo_upd;
        $txtCantidadDespacho_upd = $txtCantidadDespacho_upd;//nueva
        $txtObservacionesDespacho_upd = $txtObservacionesDespacho_upd;
        if ($idDetalleDespacho == "" or $idDespacho_upd == "" or $nTipoInsumo_upd == "") {
            echo "inconveniente";
        } else {
            //print_r($_POST);
            if ($txtCantidadDespacho_upd >= 1) {
                $cant = $this->insumo_model->getCantidadInsumos($nTipoInsumo_upd);
                $cantidadActual = $cant[0]["nImsCantidad"];
                //Cantidad Anterior de Pedido
                $cantanterior = $this->insumo_model->getCantidadDetalleAnterior($idDetalleDespacho);
                $cantidadanterior = $cantanterior[0]["stockanterior"];
                $nuevoCantidadPedido = $txtCantidadDespacho_upd - $cantidadanterior;
                if ($cantidadActual >= $nuevoCantidadPedido) {
                    //$resultado1 = $this->insumo_model->ActualizaStock($idDespacho_ins,$txtObservacionesDespacho_ins);                    
                    if ($cantidadanterior > $txtCantidadDespacho_upd) {
                        $diferencia = ($cantidadanterior - $txtCantidadDespacho_upd)*-1; //este resultado tengo q sumarle
                        //$diferencia = $diferencia * -1;
                    } else if ($cantidadanterior < $txtCantidadDespacho_upd) {
                        $diferencia = $txtCantidadDespacho_upd - $cantidadanterior; //este resultado tengo q sumarle
                    } else {
                        $diferencia = 0;
                    }
                    $this->insumo_model->set_nImsId($nTipoInsumo_upd);
                    $this->insumo_model->set_nImsCantidad($txtCantidadDespacho_upd);
                    $resultado1 = $this->insumo_model->ActualizaStockxDetalleDespacho($diferencia,$idDetalleDespacho, $txtObservacionesDespacho_upd);

                    if ($resultado1) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                } else {
                    echo "insuficiente";
                }
                //echo "cantidad Actual:".$cantidadActual;
            } else {
                echo "incorrecto";
            }
        }
    }

    /*function registrarDetalleDespachoIns232323() {
        //print_r($_POST);
        extract($_POST);
        $idDespacho_ins = $idDespacho_ins;
        $idproducto = $nTipoInsumo_ins;
        $txtCantidadDespacho_ins = $txtCantidadDespacho_ins;
        $txtObservacionesDespacho_ins = $txtObservacionesDespacho_ins;
        //echo "siendo el producto:".$idproducto." cantidad:".$txtCantidadDespacho_ins." txtObservacionesDespacho_ins".$txtObservacionesDespacho_ins;
        //exit;
        if ($idDespacho_ins == "" or $idproducto == "") {
            echo "inconveniente";
        } else {
            if ($txtCantidadDespacho_ins >= 1) {
                $cant = $this->insumo_model->getCantidadInsumos($idproducto);
                $cantidadActual = $cant[0]["nImsCantidad"];
                if ($cantidadActual >= $txtCantidadDespacho_ins) {
                    $this->insumo_model->set_nImsId($idproducto);
                    $this->insumo_model->set_nImsCantidad($txtCantidadDespacho_ins);
                    //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
                    //$resultado = $this->club_model->updClub($hdnidClub);
                    $resultado1 = $this->insumo_model->ActualizaStock($idDespacho_ins, $txtObservacionesDespacho_ins);
                    if ($resultado1) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                } else {
                    echo "insuficiente";
                }
                //echo "cantidad Actual:".$cantidadActual;
            } else {
                echo "incorrecto";
            }
        }
    }*/

    /* 10/06 */
    //
}

?>