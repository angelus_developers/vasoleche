<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ingreso extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model('despacho_model');
        $this->load->model('ingreso_model');
        $this->load->model('insumo_model');
        $this->load->model('persona_model');
    }

    public function index() {
        //$this->loaders->verificaacceso();
        $this->load->view('layout/header');

        $data["hi"] = "hola";
        $data["title"] = "Gestion de Ingreso";
        $data["modulo"] = "Despacho";
        $data["opcion"] = $data["title"];
        //
        //$data["tipoClub"] = $this->club_model->getCboTipoClub();
        $data["persona"] = $this->persona_model->getCboPersonas();
        $this->load->view("ingreso/panel_view", $data);

        $this->load->view('layout/footer');
    }

    public function registrarIns() {
        //print_r($_POST);
        extract($_POST);
        //echo "tenemos: nombre:".$txtNombreInsumo." unidad:".$txtUnidadInsumo;
        $this->ingreso_model->setNInsResponsable($nTipoPersona);
        $this->ingreso_model->setTInsFechaIngreso($txtFechaIng);
        $resultado = $this->ingreso_model->insIngreso();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function qryIngreso() {
        $data['informacion'] = $this->ingreso_model->qryIngreso();
        $this->load->view("ingreso/qry_view", $data);
    }

    function eliminarIngreso() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->ingreso_model->eliminarIngreso($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }

    public function panel_updIngreso() {
        $algo = json_decode($this->input->post("json"));
        $nIngId = $algo->nIngId;
        $data["persona"] = $this->persona_model->getCboPersonas();
        $data["informacion"] = $this->ingreso_model->getDatosIngreso($nIngId);
        //print_r($data["informacion"]);
        //$this->load->view("despacho/upd_view", $data);
        $this->load->view("ingreso/upd_view", $data);
    }

    public function updIngreso() {

        extract($_POST);

        $this->ingreso_model->setNInsId($hdnidIngreso_upd);
        $this->ingreso_model->setNInsResponsable($nTipoPersonaUpd);
        $this->ingreso_model->setTInsFechaIngreso($txtFechaUpd);

        $resultado = $this->ingreso_model->updIngreso();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }

    /**/

    function panel_detalleIngreso() {
        $algo = json_decode($_POST["json"]);
        $idIngreso = $algo->nIngId;

        $data["Insumo"] = $this->insumo_model->getCboInsumos();
        $data['idIngreso'] = $idIngreso;
        $data['getIngreso'] = $this->ingreso_model->getDatosIngreso($idIngreso);
        $data["getDetalleIngreso"] = $this->ingreso_model->getDetalleIngreso($idIngreso);
        /*
          $data["getDespacho"] = $this->despacho_model->getDespacho($data['idDespacho']);
          $data["getDetalleDespacho"] = $this->despacho_model->getDetalleDespacho($data['idDespacho']);
         * */
        $this->load->view("ingreso/detalle_ingreso/addDetalle_view", $data);
        /* $this->load->view("despacho/detalle_despacho/addDetalle_view", $data); */
    }
    
    function listarDetalleIngresoxIdIngreso(){
        $idDespacho_ins = $this->input->post("idIngreso_ins");
        $data["getDetalleIngreso"] = $this->ingreso_model->getDetalleIngreso($idDespacho_ins);
        //$this->load->view("despacho/detalle_despacho/qryDetalleDespacho_view", $data);
        $this->load->view("ingreso/detalle_ingreso/qryDetalleIngreso_view", $data);
    }

    function registrarDetalleIngresoIns() {
        extract($_POST);
        $idIngreso_ins = $idIngreso_ins;
        $idproducto = $nTipoInsumo_ins;
        $txtCantidadIngreso_ins = $txtCantidadIngreso_ins;
        $txtObservacionesIngreso_ins = $txtObservacionesIngreso_ins;
        //echo "siendo el producto:".$idproducto." cantidad:".$txtCantidadDespacho_ins." txtObservacionesDespacho_ins".$txtObservacionesDespacho_ins;
        //exit;
        if ($idIngreso_ins == "" or $idproducto == "") {
            echo "inconveniente";
        } else {
            if ($txtCantidadIngreso_ins >= 1) {
                //$cant = $this->insumo_model->getCantidadInsumos($idproducto);
                //$cantidadActual = $cant[0]["nImsCantidad"];
                //if ($cantidadActual >= $txtCantidadDespacho_ins) {
                    $this->insumo_model->set_nImsId($idproducto);
                    $this->insumo_model->set_nImsCantidad($txtCantidadIngreso_ins);
                    //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
                    //$resultado = $this->club_model->updClub($hdnidClub);
                    $resultado1 = $this->insumo_model->ActualizaStockxIngreso($idIngreso_ins, $txtObservacionesIngreso_ins);
                    if ($resultado1) {
                        echo 1;
                    } else {
                        echo 0;
                    }
//                } else {
//                    echo "insuficiente";
//                }
                //echo "cantidad Actual:".$cantidadActual;
            } else {
                echo "incorrecto";
            }
        }
    }
    
    
    function eliminarDetalleIngresoQuita(){
        $ncodigo = $this->input->post('ncodigo');
        $cantidad = $this->input->post('cantidad');
        $idinsumo = $this->input->post('idinsumo');
        //echo $ncodigo." ".$cantidad." ".$idinsumo;
        //exit;
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->ingreso_model->eliminarDetalleIngresoQuita($ncodigo,$cantidad,$idinsumo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }
    function eliminarDetalleIngresoAgrega(){
        $ncodigo = $this->input->post('ncodigo');
        $cantidad = $this->input->post('cantidad');
        $idinsumo = $this->input->post('idinsumo');
        //echo $ncodigo." ".$cantidad." ".$idinsumo;
        //exit;
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->ingreso_model->eliminarDetalleIngresoAgrega($ncodigo,$cantidad,$idinsumo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }

    /* 10/06 */
    //
}

?>