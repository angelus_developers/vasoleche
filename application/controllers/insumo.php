<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Insumo extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('insumo_model');
    }

    public function index() {
        //$this->loaders->verificaacceso();
        $this->load->view('layout/header');
        
        $data["hi"] = "hola";
        $data["title"] = "Gestion de Insumos";
        $data["modulo"] = "Insumos";
        $data["opcion"] = $data["title"];
        $this->load->view("insumo/panel_view",$data);
        
        $this->load->view('layout/footer');
    }
    public function registrarIns(){
        extract($_POST);
        //echo "tenemos: nombre:".$txtNombreInsumo." unidad:".$txtUnidadInsumo;
        //exit;
        $this->insumo_model->set_cImsNombre($txtNombreInsumo);
        $this->insumo_model->set_nImsUnidadMedida($txtUnidadInsumo);
        $this->insumo_model->set_tImsFechaVencimiento($txtFechaVencimientoInsumo);
        $this->insumo_model->set_cImsDescripcion($txtDescripcionInsumo);
        $this->insumo_model->set_nImsCantidad($txtCantidadInsumo);
        $resultado = $this->insumo_model->insInsumo();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    
    public function qryInsumo(){
        $data['informacion'] = $this->insumo_model->qryInsumo();
        /*echo "<pre>";
        print_r($datos);
        echo "</pre>";*/
        $this->load->view("insumo/qry_view",$data);
    }
    function eliminarInsumo() {
        $ncodigo = $this->input->post('ncodigo');
        //$datos = $this->mantenedorareas_model->eliminarareas($ncodigo, $estado);
        $datos = $this->insumo_model->eliminarInsumo($ncodigo);
        if ($datos) {
            echo "1";
        } else {
            echo "error";
        }
    }
    
    public function panel_updInsumo(){
        //print_r($_POST["json"]);
        $algo= json_decode($_POST["json"]);
        $idinsumo = $algo->nImsId;
        //print_r($algo->nClubId);
        $data["informacion"] = $this->insumo_model->getDatos($idinsumo);
        //print_r($data);
        //exit;
        
        $this->load->view("insumo/upd_view",$data);
    }
    public function updInsumo(){
        extract($_POST);
        //$hdnidClub = $this->input->post("hdnidClub");
        //echo "tenemos: nombre:".$txtNombreInsumo." unidad:".$txtUnidadInsumo;
        //exit;
        $this->insumo_model->set_nImsId($hdnidInsumo_upd);
        $this->insumo_model->set_cImsNombre($txtupd_NombreInsumo);
        $this->insumo_model->set_nImsUnidadMedida($txtupd_UnidadInsumo);
        $this->insumo_model->set_tImsFechaVencimiento($txtupd_FechaVencimientoInsumo);
        $this->insumo_model->set_cImsDescripcion($txtupd_DescripcionInsumo);
        $this->insumo_model->set_nImsCantidad($txtupd_CantidadInsumo);
        //$resultado = $this->crearNotasPrensa_model->crearNotasPrensaIns($rsvusuario_d);
        //$resultado = $this->club_model->updClub($hdnidClub);
        $resultado = $this->insumo_model->updInsumo();
        if ($resultado) {
            echo 1;
        } else {
            echo 0;
        }
    }
    //
    
    
}
?>