<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Autogenered Developed by @jvinceso */
/* Date : 01-05-2013 19:17:17 */

class Club_model extends CI_Model {

    //Atributos de Clase
    private $nCluId = '';
    private $cCluNombre = '';
    private $cCluDireccion = '';
    private $cCluTelefono = '';
    private $nCluCantBeneficiados = '';
    private $tCluFechaRegistro = '';
    private $cCluEstado = '';
    private $tCluFechaCese = '';
    private $nCluCantidadLeche = '';
    private $nCluCantidadAvena = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    function set_nCluId($nCluId) {
        $this->nCluId = $nCluId;
    }

    function set_cCluNombre($cCluNombre) {
        $this->cCluNombre = $cCluNombre;
    }

    function set_cCluDireccion($cCluDireccion) {
        $this->cCluDireccion = $cCluDireccion;
    }

    function set_cCluTelefono($cCluTelefono) {
        $this->cCluTelefono = $cCluTelefono;
    }

    function set_nCluCantBeneficiados($nCluCantBeneficiados) {
        $this->nCluCantBeneficiados = $nCluCantBeneficiados;
    }

    function set_tCluFechaRegistro($tCluFechaRegistro) {
        $this->tCluFechaRegistro = $tCluFechaRegistro;
    }

    function set_cCluEstado($cCluEstado) {
        $this->cCluEstado = $cCluEstado;
    }

    function set_tCluFechaCese($tCluFechaCese) {
        $this->tCluFechaCese = $tCluFechaCese;
    }

    function set_nCluCantidadLeche($nCluCantidadLeche) {
        $this->nCluCantidadLeche = $nCluCantidadLeche;
    }

    function set_nCluCantidadAvena($nCluCantidadAvena) {
        $this->nCluCantidadAvena = $nCluCantidadAvena;
    }

    //FUNCIONES Get
    function get_nCluId() {
        return $this->nCluId;
    }

    function get_cCluNombre() {
        return $this->cCluNombre;
    }

    function get_cCluDireccion() {
        return $this->cCluDireccion;
    }

    function get_cCluTelefono() {
        return $this->cCluTelefono;
    }

    function get_nCluCantBeneficiados() {
        return $this->nCluCantBeneficiados;
    }

    function get_tCluFechaRegistro() {
        return $this->tCluFechaRegistro;
    }

    function get_cCluEstado() {
        return $this->cCluEstado;
    }

    function get_tCluFechaCese() {
        return $this->tCluFechaCese;
    }

    function get_nCluCantidadLeche() {
        return $this->nCluCantidadLeche;
    }

    function get_nCluCantidadAvena() {
        return $this->nCluCantidadAvena;
    }

    //Obtener Objeto CLUB
    function get_ObjClub($CAMPO) {
        $query = $this->db->query("SELECT * FROM CLUB WHERE CAMPO=?", array($CAMPO));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //CREANDO EL OBJETO
        }
    }

    function insClub() {
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_I_CLUB('".$this->get_cCluNombre()."','".$this->get_cCluDireccion()."','".$this->get_cCluTelefono()."',".$this->get_nCluCantBeneficiados().",".$this->get_nCluCantidadLeche().",".$this->get_nCluCantidadAvena().")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function qryClub() {
        $query = "call USP_CLU_S_CLUB()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function getDatos($idclub){
        $query = "call USP_CLU_S_CLUB_GET('".$idclub."')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function updClub($hdnidClub) {
       //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_U_CLUB('".$this->get_cCluNombre()."','".$this->get_cCluDireccion()."','".$this->get_cCluTelefono()."',".$this->get_nCluCantBeneficiados().",".$this->get_nCluCantidadLeche().",".$this->get_nCluCantidadAvena().",".$hdnidClub.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    function eliminarClub($ncodigo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_CLUB(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function getCboTipoClub() {
        $query = "SELECT nCluId, cCluNombre FROM club c;";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    
    
    /*function guardarObjetivoEstrategico($txtnombreobjestrategico, $cboplanvinculado,$cboAreaResponsableIndiEstrategico) {
//        $query = "insert into objestrategico (NOMBRE,DESCRIPCION,ACTIVO, idplan_FK)
//        values('$txtnombreobjestrategico','',1,'$cboplanvinculado')";
        $query = "call sp_guardarObjetivoEstrategico('$txtnombreobjestrategico',$cboplanvinculado,$cboAreaResponsableIndiEstrategico)";
        $query2 = $this->db->query($query);
        return $query2;
    }
    function QuitaObjEstrategico($ncodigo) {
        //$query = "UPDATE categoria SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idcategoria=$ncodigo";
        $query = "call sp_QuitaObjEstrategico($ncodigo)";
        $query2 = $this->db->query($query);
        return $query2;
    }*/

}

?>