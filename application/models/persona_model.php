<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Autogenered Developed by @jvinceso */
/* Date : 01-05-2013 19:17:17 */

class Persona_model extends CI_Model {

    //Atributos de Clase
    private $nPerId = '';
    private $cPerNombres = '';
    private $cPerApellidoPaterno = '';
    private $cPerApellidoMaterno = '';
    private $cPerDni = '';
    private $cPerDireccion = '';
    private $cPerTelefono = '';
    private $cPerCelular = '';
    private $cPerEstado = '';
    private $tPerFechaRegistro = '';
    private $tPerFechaBaja = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    function set_nPerId($nPerId) {
        $this->nPerId = $nPerId;
    }

    function set_cPerNombres($cPerNombres) {
        $this->cPerNombres = $cPerNombres;
    }

    function set_cPerApellidoPaterno($cPerApellidoPaterno) {
        $this->cPerApellidoPaterno = $cPerApellidoPaterno;
    }

    function set_cPerApellidoMaterno($cPerApellidoMaterno) {
        $this->cPerApellidoMaterno = $cPerApellidoMaterno;
    }

    function set_cPerDni($cPerDni) {
        $this->cPerDni = $cPerDni;
    }

    function set_cPerDireccion($cPerDireccion) {
        $this->cPerDireccion = $cPerDireccion;
    }

    function set_cPerTelefono($cPerTelefono) {
        $this->cPerTelefono = $cPerTelefono;
    }

    function set_cPerCelular($cPerCelular) {
        $this->cPerCelular = $cPerCelular;
    }

    function set_cPerEstado($cPerEstado) {
        $this->cPerEstado = $cPerEstado;
    }

    function set_tPerFechaRegistro($tPerFechaRegistro) {
        $this->tPerFechaRegistro = $tPerFechaRegistro;
    }

    function set_tPerFechaBaja($tPerFechaBaja) {
        $this->tPerFechaBaja = $tPerFechaBaja;
    }

    //FUNCIONES Get
    function get_nPerId() {
        return $this->nPerId;
    }

    function get_cPerNombres() {
        return $this->cPerNombres;
    }

    function get_cPerApellidoPaterno() {
        return $this->cPerApellidoPaterno;
    }

    function get_cPerApellidoMaterno() {
        return $this->cPerApellidoMaterno;
    }

    function get_cPerDni() {
        return $this->cPerDni;
    }

    function get_cPerDireccion() {
        return $this->cPerDireccion;
    }

    function get_cPerTelefono() {
        return $this->cPerTelefono;
    }

    function get_cPerCelular() {
        return $this->cPerCelular;
    }

    function get_cPerEstado() {
        return $this->cPerEstado;
    }

    function get_tPerFechaRegistro() {
        return $this->tPerFechaRegistro;
    }

    function get_tPerFechaBaja() {
        return $this->tPerFechaBaja;
    }

    //Obtener Objeto PERSONA
    function get_ObjPersona($CAMPO) {
        $query = $this->db->query("SELECT * FROM PERSONA WHERE CAMPO=?", array($CAMPO));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //CREANDO EL OBJETO
        }
    }

    //Insertar PERSONA
    function registrarPersonaIns() {
        $query = $this->db->query("CALL USP_CLU_I_PERSONA('" . $this->get_cPerNombres() . "','" . $this->get_cPerApellidoPaterno() . "','" . $this->get_cPerApellidoMaterno() . "','" . $this->get_cPerDni() . "','" . $this->get_cPerDireccion() . "','" . $this->get_cPerTelefono() . "','" . $this->get_cPerCelular() . "')");
        return $query;
        /* 			if ($query->num_rows() > 0){
          $row = $query->row();
          //CREANDO EL OBJETO
          } */
    }

    function getCboPersonas() {//comite
        //$query = "call USP_PER_S_PERSONAS()";
        $query = "call USP_CLU_S_PERSONAS_ACTIVAS()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function qryPersona() {
        $query = "call USP_CLU_S_PERSONAS()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    public function qryPersonas($opt = ''){
        $query = $this->db->query("CALL USP_PER_S('".$opt."')");
        if ($query->num_rows() > 0){
            return  $query->result_array();
        }else{
            return null;
        }
    }    
    function eliminarPersona($ncodigo) {
        
        $query = "call USP_CLU_D_PERSONA(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    function getDatos($idper){
        $query = "call USP_CLU_S_PERSONA_GET('".$idper."')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    
    function updPersona($hdnidper){
        //$query = "call USP_CLU_U_CLUB('".$this->get_cCluNombre()."','".$this->get_cCluDireccion()."','".$this->get_cCluTelefono()."',".$this->get_nCluCantBeneficiados().",".$this->get_nCluCantidadLeche().",".$this->get_nCluCantidadAvena().",".$hdnidClub.")";
        $query = $this->db->query("CALL USP_CLU_U_PERSONA('" . $this->get_cPerNombres() . "','" . $this->get_cPerApellidoPaterno() . "','" . $this->get_cPerApellidoMaterno() . "','" . $this->get_cPerDni() . "','" . $this->get_cPerDireccion() . "','" . $this->get_cPerTelefono() . "','" . $this->get_cPerCelular() . "',".$hdnidper.")");
       // $query = "call USP_CLU_U_PERSONA('".$this->get_cCluNombre()."','".$this->get_cCluDireccion()."','".$this->get_cCluTelefono()."',".$this->get_nCluCantBeneficiados().",".$this->get_nCluCantidadLeche().",".$this->get_nCluCantidadAvena().",".$hdnidClub.")";
        //query2 = $this->db->query($query);
        return $query;
    }
    /*function qryClub() {
        $query = "call USP_CLU_S_CLUB()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }*/
    public function buscaxDniGet(){
        $sql = "CALL USP_PER_S_dni('".$this->get_cPerDni()."');";
        // echo $sql;
        // exit();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0){
            return  $query->result_array();
        }else{
            return null;
        }       
    }    

}

?>