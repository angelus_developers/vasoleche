<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Ingreso_model extends CI_Model {

    //Atributos de Clase
    private $nInsId = '';
    private $nInsResponsable = '';
    private $nInsEstado = '';
    private $tInsFechaIngreso = '';
    
        //private $nDetCroId = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    public function getNInsId() {
        return $this->nInsId;
    }

    public function setNInsId($nInsId) {
        $this->nInsId = $nInsId;
    }

    public function getNInsResponsable() {
        return $this->nInsResponsable;
    }

    public function setNInsResponsable($nInsResponsable) {
        $this->nInsResponsable = $nInsResponsable;
    }

    public function getNInsEstado() {
        return $this->nInsEstado;
    }

    public function setNInsEstado($nInsEstado) {
        $this->nInsEstado = $nInsEstado;
    }

    public function getTInsFechaIngreso() {
        return $this->tInsFechaIngreso;
    }

    public function setTInsFechaIngreso($tInsFechaIngreso) {
        $this->tInsFechaIngreso = $tInsFechaIngreso;
    }

    function insIngreso() {
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_I_INGRESO('" . $this->getNInsResponsable() . "','" . $this->getTInsFechaIngreso() . "')";
        $query2 = $this->db->query($query);
        return $query2;
    }
    /*12/06*/
    function qryIngreso() {
        $query = "call USP_CLU_S_Ingreso()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function eliminarIngreso($ncodigo){
        $query = "call USP_CLU_D_INGRESO(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function getDatosIngreso($nIngId){
        $query = "call USP_CLU_S_INGRESO_GET('".$nIngId."')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function updIngreso (){
        $query = "call USP_CLU_U_INGRESO('".$this->getNInsId()."','".$this->getNInsResponsable()."','".$this->getTInsFechaIngreso()."')";
        // print $query;exit();
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    /*DetalleIngreso*/
    function getDetalleIngreso($idIngreso){
        $query = "call USP_CLU_S_DetalleIngreso_GET($idIngreso)";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function eliminarDetalleIngresoQuita($ncodigo,$cantidad,$idinsumo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_DetalleIngresoQuita(".$ncodigo.",".$cantidad.",".$idinsumo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    function eliminarDetalleIngresoAgrega($ncodigo,$cantidad,$idinsumo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_DetalleIngresoAgrega(".$ncodigo.",".$cantidad.",".$idinsumo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    
}

?>