<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Autogenered Developed by @jvinceso */
/* Date : 01-05-2013 19:17:17 */

class Insumo_model extends CI_Model {

    //Atributos de Clase
    private $nImsId = '';
    private $cImsNombre = '';
    private $nImsUnidadMedida = '';
    private $tImsFechaRegistro = '';
    private $tImsFechaVencimiento = '';
    private $cImsEstado = '';
    private $nImsTipo = '';
    private $cImsDescripcion = '';
    private $nImsCantidad = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    function set_nImsId($nImsId) {
        $this->nImsId = $nImsId;
    }

    function set_cImsNombre($cImsNombre) {
        $this->cImsNombre = $cImsNombre;
    }

    function set_nImsUnidadMedida($nImsUnidadMedida) {
        $this->nImsUnidadMedida = $nImsUnidadMedida;
    }

    function set_tImsFechaRegistro($tImsFechaRegistro) {
        $this->tImsFechaRegistro = $tImsFechaRegistro;
    }

    function set_tImsFechaVencimiento($tImsFechaVencimiento) {
        $this->tImsFechaVencimiento = $tImsFechaVencimiento;
    }

    function set_cImsEstado($cImsEstado) {
        $this->cImsEstado = $cImsEstado;
    }

    function set_nImsTipo($nImsTipo) {
        $this->nImsTipo = $nImsTipo;
    }

    function set_cImsDescripcion($cImsDescripcion) {
        $this->cImsDescripcion = $cImsDescripcion;
    }

    function set_nImsCantidad($nImsCantidad) {
        $this->nImsCantidad = $nImsCantidad;
    }

    //FUNCIONES Get
    function get_nImsId() {
        return $this->nImsId;
    }

    function get_cImsNombre() {
        return $this->cImsNombre;
    }

    function get_nImsUnidadMedida() {
        return $this->nImsUnidadMedida;
    }

    function get_tImsFechaRegistro() {
        return $this->tImsFechaRegistro;
    }

    function get_tImsFechaVencimiento() {
        return $this->tImsFechaVencimiento;
    }

    function get_cImsEstado() {
        return $this->cImsEstado;
    }

    function get_nImsTipo() {
        return $this->nImsTipo;
    }

    function get_cImsDescripcion() {
        return $this->cImsDescripcion;
    }

    function get_nImsCantidad() {
        return $this->nImsCantidad;
    }

    //Obtener Objeto INSUMOS
    function get_ObjInsumos($CAMPO) {
        $query = $this->db->query("SELECT * FROM INSUMOS WHERE CAMPO=?", array($CAMPO));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //CREANDO EL OBJETO
        }
    }

    function insInsumo() {
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_I_INSUMO('" . $this->get_cImsNombre() . "','" . $this->get_nImsUnidadMedida() . "','" . $this->get_tImsFechaVencimiento() . "','" . $this->get_cImsDescripcion() . "'," . $this->get_nImsCantidad() . ")";
        $query2 = $this->db->query($query);
        return $query2;
    }

    function qryInsumo() {
        $query = "call USP_CLU_S_INSUMO()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }

    function eliminarInsumo($ncodigo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_INSUMO(" . $ncodigo . ")";
        $query2 = $this->db->query($query);
        return $query2;
    }

    function getDatos($idinsumo) {
        $query = "call USP_CLU_S_INSUMO_GET('" . $idinsumo . "')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function updInsumo(){
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_U_INSUMO('" . $this->get_nImsId() . "','" . $this->get_cImsNombre() . "','" . $this->get_nImsUnidadMedida() . "','" . $this->get_tImsFechaVencimiento() . "','" . $this->get_cImsDescripcion() . "','" . $this->get_nImsCantidad() .  "')";
        $query2 = $this->db->query($query);
        return $query2;
    }
    // getCboInsumos
    public function getCboInsumos(){
        $query = "call USP_CLU_S_INSUMO()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    public function getCboInsumosAnio(){
        $query = "call USP_S_INSUMOS_CBO()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    /**/
    function getCantidadInsumos($idproducto){
        $query = "call USP_CLU_S_INSUMO_GET($idproducto)";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    /**/
    function ActualizaStock($idDespacho_ins,$txtObservacionesDespacho_ins){
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_U_INSUMO_STOCK2('" . $this->get_nImsId()  . "','" . $this->get_nImsCantidad() . "','" . $idDespacho_ins ."','" . $txtObservacionesDespacho_ins .  "')";
        $query2 = $this->db->query($query);
        return $query2;
    }
    /*12/06*/
    function getCantidadDetalleAnterior($idDetalleDespacho){
        $query = "call USP_CLU_S_DETALLEDESPACHO_cantant_GET($idDetalleDespacho)";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function ActualizaStockxDetalleDespacho($diferencia,$idDetalleDespacho,$txtObservacionesDespacho_upd){
        $query = "call USP_CLU_U_INSUMO_STOCKxDetDespacho('" . $this->get_nImsId(). "','" . $diferencia  . "','" . $this->get_nImsCantidad() . "','" . $idDetalleDespacho ."','" . $txtObservacionesDespacho_upd .  "')";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
//    function updClub($hdnidClub) {
//        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
//        $query = "call USP_CLU_U_CLUB('" . $this->get_cCluNombre() . "','" . $this->get_cCluDireccion() . "','" . $this->get_cCluTelefono() . "'," . $this->get_nCluCantBeneficiados() . "," . $this->get_nCluCantidadLeche() . "," . $this->get_nCluCantidadAvena() . "," . $hdnidClub . ")";
//        $query2 = $this->db->query($query);
//        return $query2;
//    }
    
    /*Ingreso*/
    function ActualizaStockxIngreso($idIngreso_ins,$txtObservacionesIngreso_ins){
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_U_INSUMO_STOCKxIngreso('" . $this->get_nImsId()  . "','" . $this->get_nImsCantidad() . "','" . $idIngreso_ins ."','" . $txtObservacionesIngreso_ins .  "')";
        $query2 = $this->db->query($query);
        return $query2;
    }

}

?>