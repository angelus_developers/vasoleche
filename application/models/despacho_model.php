<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Autogenered Developed by @jvinceso */
/* Date : 01-05-2013 19:17:17 */

class Despacho_model extends CI_Model {

    //Atributos de Clase
    private $nDesId = '';
    private $nDesResponsable = '';
    private $nDesDuracion = '';
    private $nDesEstado = '';
    private $tDesFechaRegistro = '';
    private $nCluId = '';
    private $cDesMes = '';
    private $cDesAnio = '';
    
        //private $nDetCroId = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    function set_nDesId($nDesId) {
        $this->nDesId = $nDesId;
    }
    
    public function getCDesMes() {
        return $this->cDesMes;
    }

    public function setCDesMes($cDesMes) {
        $this->cDesMes = $cDesMes;
    }

    public function getCDesAnio() {
        return $this->cDesAnio;
    }

    public function setCDesAnio($cDesAnio) {
        $this->cDesAnio = $cDesAnio;
    }

    function set_nDesResponsable($nDesResponsable) {
        $this->nDesResponsable = $nDesResponsable;
    }

    function set_nDesDuracion($nDesDuracion) {
        $this->nDesDuracion = $nDesDuracion;
    }

    function set_nDesEstado($nDesEstado) {
        $this->nDesEstado = $nDesEstado;
    }

    function set_tDesFechaRegistro($tDesFechaRegistro) {
        $this->tDesFechaRegistro = $tDesFechaRegistro;
    }

    function set_nCluId($nCluId) {
        $this->nCluId = $nCluId;
    }

    /*function set_nDetCroId($nDetCroId) {
        $this->nDetCroId = $nDetCroId;
    }*/

    //FUNCIONES Get
    function get_nDesId() {
        return $this->nDesId;
    }

    function get_nDesResponsable() {
        return $this->nDesResponsable;
    }

    function get_nDesDuracion() {
        return $this->nDesDuracion;
    }

    function get_nDesEstado() {
        return $this->nDesEstado;
    }

    function get_tDesFechaRegistro() {
        return $this->tDesFechaRegistro;
    }

    function get_nCluId() {
        return $this->nCluId;
    }

    /*function get_nDetCroId() {
        return $this->nDetCroId;
    }*/

    //Obtener Objeto DESPACHO
    function get_ObjDespacho($CAMPO) {
        $query = $this->db->query("SELECT * FROM DESPACHO WHERE CAMPO=?", array($CAMPO));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //CREANDO EL OBJETO
        }
    }
    function insDespacho() {
        //$query = "call sp_guardarIndicadorObjetivoEspecifico($cbo_objetivo_especifico,'$txtdenominacionIndicador','$cbotipometa','$meta','$txtareaformula',$cboAreaResponsable,'$txtarealineabase','$txtf2uente')";
        $query = "call USP_CLU_I_DESPACHO('" . $this->get_nDesResponsable() . "','" . $this->get_nCluId() . "','" . $this->getCDesMes() . "','" . $this->getCDesAnio() . "')";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function qryDespacho() {
        $query = "call USP_CLU_S_Despacho()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function eliminarDespacho($ncodigo){
        $query = "call USP_CLU_D_DESPACHO(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function getDatosDespacho($iddespacho){
        $query = "call USP_CLU_S_DESPACHO_GET('".$iddespacho."')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function updDespacho(){
        $query = "call USP_CLU_U_DESPACHO('".$this->get_nDesId()."','".$this->get_nDesResponsable()."','".$this->get_nCluId()."','".$this->getCDesMes()."','".$this->getCDesAnio()."')";
        // print $query;exit();
        $query2 = $this->db->query($query);
        return $query2;
    }
    /**/
    
    
    
    
    
    
    
    
    
    
    /**/
    function getDespacho($idDespacho){
        $query = "call USP_CLU_S_Despacho_GET($idDespacho)";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function getDetalleDespacho($idDespacho){
        $query = "call USP_CLU_S_DetalleDespacho_GET($idDespacho)";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function eliminarDetalleDespacho($ncodigo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_DetalleDespacho(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    /*12/06*/
    function getdatosDetalleDespacho($iddetalleDespacho){
        $query = "call USP_CLU_S_DetalleDespacho_GET_datos($iddetalleDespacho)";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    /*12/06*/
}

?>