<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Autogenered Developed by @jvinceso */
/* Date : 01-05-2013 19:17:17 */

class Beneficiario_model extends CI_Model {

    //Atributos de Clase
    private $nBenId = '';
    private $cBenPartidaNacimiento = '';
    private $persona_nPerId = '';
    private $cBenSexo = '';
    private $cBenFechaNacimiento = '';
    private $cBenEstado = '';
    private $nPerIdMadre = '';
    private $nClubId = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    function set_nBenId($nBenId) {
        $this->nBenId = $nBenId;
    }

    function set_cBenPartidaNacimiento($cBenPartidaNacimiento) {
        $this->cBenPartidaNacimiento = $cBenPartidaNacimiento;
    }

    function set_persona_nPerId($persona_nPerId) {
        $this->persona_nPerId = $persona_nPerId;
    }

    function set_cBenSexo($cBenSexo) {
        $this->cBenSexo = $cBenSexo;
    }

    function set_cBenFechaNacimiento($cBenFechaNacimiento) {
        $this->cBenFechaNacimiento = $cBenFechaNacimiento;
    }

    function set_cBenEstado($cBenEstado) {
        $this->cBenEstado = $cBenEstado;
    }

    function set_nPerIdMadre($nPerIdMadre) {
        $this->nPerIdMadre = $nPerIdMadre;
    }

    function set_nClubId($nClubId) {
        $this->nClubId = $nClubId;
    }

    //FUNCIONES Get
    function get_nBenId() {
        return $this->nBenId;
    }

    function get_cBenPartidaNacimiento() {
        return $this->cBenPartidaNacimiento;
    }

    function get_persona_nPerId() {
        return $this->persona_nPerId;
    }

    function get_cBenSexo() {
        return $this->cBenSexo;
    }

    function get_cBenFechaNacimiento() {
        return $this->cBenFechaNacimiento;
    }

    function get_cBenEstado() {
        return $this->cBenEstado;
    }

    function get_nPerIdMadre() {
        return $this->nPerIdMadre;
    }

    function get_nClubId() {
        return $this->nClubId;
    }

    //Obtener Objeto BENEFICIARIO
    function get_ObjBeneficiario($CAMPO) {
        $query = $this->db->query("SELECT * FROM BENEFICIARIO WHERE CAMPO=?", array($CAMPO));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //CREANDO EL OBJETO
        }
    }

    function insBeneficiario() {
        //$query = "call USP_CLU_I_BENEFICIARIO('".$this->get_cBenPartidaNacimiento()."',".$this->get_persona_nPerId().",'".$this->get_cBenSexo()."','".$this->get_cBenFechaNacimiento()."',".$this->get_nPerIdMadre().",".$this->get_nClubId().")";
        $query = "call USP_CLU_I_BENEFICIARIO(".$this->get_persona_nPerId().",'".$this->get_cBenSexo()."','".$this->get_cBenFechaNacimiento()."',".$this->get_nPerIdMadre().",".$this->get_nClubId().")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function qryBeneficiario() {
        $query = "call USP_CLU_S_BENEFICIARIO()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    function getDatos($idben){
        $query = "call USP_CLU_S_BENEFICIARIO_GET('".$idben."')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    public function updBeneficiario(){
        //$query = "call USP_CLU_U_CLUB('".$this->get_cCluNombre()."','".$this->get_cCluDireccion()."','".$this->get_cCluTelefono()."',".$this->get_nCluCantBeneficiados().",".$this->get_nCluCantidadLeche().",".$this->get_nCluCantidadAvena().",".$hdnidClub.")";
        
        $query = "call USP_CLU_U_BENEFICIARIO(".$this->get_persona_nPerId().",'".$this->get_cBenSexo()."','".$this->get_cBenFechaNacimiento()."',".$this->get_nPerIdMadre().",".$this->get_nClubId().",".$this->get_nBenId().")";
        //echo $query;
        //exit;
        $query2 = $this->db->query($query);
        return $query2;
    }
    function eliminarBeneficiario($ncodigo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_BENEFICIARIO(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function get_ninosLimite(){
        $query = "call USP_CLU_S_BENEFICIARIOS_APTOS()";
        $query2 = $this->db->query($query);
        return $query2->result_array();
    }

}

?>