<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Autogenered Developed by @jvinceso */
/* Date : 01-05-2013 19:17:17 */

class Comite_model extends CI_Model {

    //Atributos de Clase
    private $nComId = '';
    private $cComTipo = '';
    private $nCluId = '';
    private $nPerId = '';

    //Constructor de Clase
    function __construct() {
        parent::__construct();
    }

    //FUNCIONES Set
    function set_nComId($nComId) {
        $this->nComId = $nComId;
    }

    function set_cComTipo($cComTipo) {
        $this->cComTipo = $cComTipo;
    }

    function set_nCluId($nCluId) {
        $this->nCluId = $nCluId;
    }

    function set_nPerId($nPerId) {
        $this->nPerId = $nPerId;
    }

    //FUNCIONES Get
    function get_nComId() {
        return $this->nComId;
    }

    function get_cComTipo() {
        return $this->cComTipo;
    }

    function get_nCluId() {
        return $this->nCluId;
    }

    function get_nPerId() {
        return $this->nPerId;
    }

    //Obtener Objeto COMITE
    function get_ObjComite($CAMPO) {
        $query = $this->db->query("SELECT * FROM COMITE WHERE CAMPO=?", array($CAMPO));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            //CREANDO EL OBJETO
        }
    }

    function getCboTipoComite() {
        $query = "select nMulIdPadre,cMulDescripcion,nMulOrden,nMulEstado from multitabla where nMulOrden =1 and nMulEstado='A'";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    
    /**/
    function insComite() {
        //$query = "call USP_CLU_I_CLUB('".$this->get_cCluNombre()."','".$this->get_cCluDireccion()."','".$this->get_cCluTelefono()."',".$this->get_nCluCantBeneficiados().",".$this->get_nCluCantidadLeche().",".$this->get_nCluCantidadAvena().")";
        $query = "call USP_CLU_I_Comite('".$this->get_cComTipo()."',".$this->get_nCluId().",".$this->get_nPerId().")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    
    function qryComite() {
        $query = "call USP_CLU_S_COMITE()";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function getDatos($idcom){
        $query = "call USP_CLU_S_COMITE_GET('".$idcom."')";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }
    function updComite($hdnidComite) {
        $query = "call USP_CLU_U_COMITE('".$this->get_cComTipo()."',".$this->get_nCluId().",".$this->get_nPerId().",".$hdnidComite.")";
        /*$query = "UPDATE comite 
        set cComTipo='".$this->get_cComTipo()."' , 
        nCluId=".$this->get_nCluId().", 
        nPerId=".$this->get_nPerId()."
        where nComId=".$hdnidComite;*/
        /*$query = "update comite 
        set cComTipo='".$this->get_cComTipo()."' 
        where nComId=".$hdnidComite;*/
        
        $query2 = $this->db->query($query);
        //$consulta3 = "update movimientos set ActualResponsable='n' where IdSolicitud='$codigosolicitud'";
        return $query2;
    }
    function eliminarComite($ncodigo) {
        //$query = "UPDATE area SET activo = CASE WHEN activo = 1 THEN 0 ELSE 1 END where idAREA=$ncodigo";
        $query = "call USP_CLU_D_COMITE(".$ncodigo.")";
        $query2 = $this->db->query($query);
        return $query2;
    }
    /*
    function getCboTipoClub() {
        $query = "SELECT nCluId, cCluNombre FROM club c;";
        $query2 = $this->db->query($query);
        if ($query2->num_rows() > 0) {
            return $query2->result_array(); //sirve para mandar los datos
        } else {
            return false;
        }
    }*/

}

?>