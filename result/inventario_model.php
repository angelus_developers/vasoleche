<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Inventario_model extends CI_Model {
		//Atributos de Clase
		private $nInvID = '';
		private $tInvFechaIngreso = '';
		private $tInvFechaSalida = '';
		private $nInvCantidad = '';
		private $nImsId = '';
		private $nInvStockMinimo = '';
		private $nInvStockMaximo = '';
		private $nDesId = '';
		private $nInvStock = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nInvID($nInvID){
			$this->nInvID = $nInvID;
		}
		function set_tInvFechaIngreso($tInvFechaIngreso){
			$this->tInvFechaIngreso = $tInvFechaIngreso;
		}
		function set_tInvFechaSalida($tInvFechaSalida){
			$this->tInvFechaSalida = $tInvFechaSalida;
		}
		function set_nInvCantidad($nInvCantidad){
			$this->nInvCantidad = $nInvCantidad;
		}
		function set_nImsId($nImsId){
			$this->nImsId = $nImsId;
		}
		function set_nInvStockMinimo($nInvStockMinimo){
			$this->nInvStockMinimo = $nInvStockMinimo;
		}
		function set_nInvStockMaximo($nInvStockMaximo){
			$this->nInvStockMaximo = $nInvStockMaximo;
		}
		function set_nDesId($nDesId){
			$this->nDesId = $nDesId;
		}
		function set_nInvStock($nInvStock){
			$this->nInvStock = $nInvStock;
		}

		//FUNCIONES Get
		function get_nInvID(){
			return $this->nInvID;
		}
		function get_tInvFechaIngreso(){
			return $this->tInvFechaIngreso;
		}
		function get_tInvFechaSalida(){
			return $this->tInvFechaSalida;
		}
		function get_nInvCantidad(){
			return $this->nInvCantidad;
		}
		function get_nImsId(){
			return $this->nImsId;
		}
		function get_nInvStockMinimo(){
			return $this->nInvStockMinimo;
		}
		function get_nInvStockMaximo(){
			return $this->nInvStockMaximo;
		}
		function get_nDesId(){
			return $this->nDesId;
		}
		function get_nInvStock(){
			return $this->nInvStock;
		}
		//Obtener Objeto INVENTARIO
		function get_ObjInventario($CAMPO){
			$query = $this->db->query("SELECT * FROM INVENTARIO WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>