<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Cronograma_model extends CI_Model {
		//Atributos de Clase
		private $nCroId = '';
		private $nUsuCodigo = '';
		private $tCroFechaRegistro = '';
		private $nCroTotal = '';
		private $cCroEstado = '';
		private $nImsId = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nCroId($nCroId){
			$this->nCroId = $nCroId;
		}
		function set_nUsuCodigo($nUsuCodigo){
			$this->nUsuCodigo = $nUsuCodigo;
		}
		function set_tCroFechaRegistro($tCroFechaRegistro){
			$this->tCroFechaRegistro = $tCroFechaRegistro;
		}
		function set_nCroTotal($nCroTotal){
			$this->nCroTotal = $nCroTotal;
		}
		function set_cCroEstado($cCroEstado){
			$this->cCroEstado = $cCroEstado;
		}
		function set_nImsId($nImsId){
			$this->nImsId = $nImsId;
		}

		//FUNCIONES Get
		function get_nCroId(){
			return $this->nCroId;
		}
		function get_nUsuCodigo(){
			return $this->nUsuCodigo;
		}
		function get_tCroFechaRegistro(){
			return $this->tCroFechaRegistro;
		}
		function get_nCroTotal(){
			return $this->nCroTotal;
		}
		function get_cCroEstado(){
			return $this->cCroEstado;
		}
		function get_nImsId(){
			return $this->nImsId;
		}
		//Obtener Objeto CRONOGRAMA
		function get_ObjCronograma($CAMPO){
			$query = $this->db->query("SELECT * FROM CRONOGRAMA WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>