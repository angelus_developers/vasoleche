<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Club_model extends CI_Model {
		//Atributos de Clase
		private $nCluId = '';
		private $cCluNombre = '';
		private $cCluDireccion = '';
		private $cCluTelefono = '';
		private $nCluCantBeneficiados = '';
		private $tCluFechaRegistro = '';
		private $cCluEstado = '';
		private $tCluFechaCese = '';
		private $nCluCantidadLeche = '';
		private $nCluCantidadAvena = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nCluId($nCluId){
			$this->nCluId = $nCluId;
		}
		function set_cCluNombre($cCluNombre){
			$this->cCluNombre = $cCluNombre;
		}
		function set_cCluDireccion($cCluDireccion){
			$this->cCluDireccion = $cCluDireccion;
		}
		function set_cCluTelefono($cCluTelefono){
			$this->cCluTelefono = $cCluTelefono;
		}
		function set_nCluCantBeneficiados($nCluCantBeneficiados){
			$this->nCluCantBeneficiados = $nCluCantBeneficiados;
		}
		function set_tCluFechaRegistro($tCluFechaRegistro){
			$this->tCluFechaRegistro = $tCluFechaRegistro;
		}
		function set_cCluEstado($cCluEstado){
			$this->cCluEstado = $cCluEstado;
		}
		function set_tCluFechaCese($tCluFechaCese){
			$this->tCluFechaCese = $tCluFechaCese;
		}
		function set_nCluCantidadLeche($nCluCantidadLeche){
			$this->nCluCantidadLeche = $nCluCantidadLeche;
		}
		function set_nCluCantidadAvena($nCluCantidadAvena){
			$this->nCluCantidadAvena = $nCluCantidadAvena;
		}

		//FUNCIONES Get
		function get_nCluId(){
			return $this->nCluId;
		}
		function get_cCluNombre(){
			return $this->cCluNombre;
		}
		function get_cCluDireccion(){
			return $this->cCluDireccion;
		}
		function get_cCluTelefono(){
			return $this->cCluTelefono;
		}
		function get_nCluCantBeneficiados(){
			return $this->nCluCantBeneficiados;
		}
		function get_tCluFechaRegistro(){
			return $this->tCluFechaRegistro;
		}
		function get_cCluEstado(){
			return $this->cCluEstado;
		}
		function get_tCluFechaCese(){
			return $this->tCluFechaCese;
		}
		function get_nCluCantidadLeche(){
			return $this->nCluCantidadLeche;
		}
		function get_nCluCantidadAvena(){
			return $this->nCluCantidadAvena;
		}
		//Obtener Objeto CLUB
		function get_ObjClub($CAMPO){
			$query = $this->db->query("SELECT * FROM CLUB WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>