<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Despacho_model extends CI_Model {
		//Atributos de Clase
		private $nDesId = '';
		private $nDesResponsable = '';
		private $nDesDuracion = '';
		private $nDesEstado = '';
		private $tDesFechaRegistro = '';
		private $nCluId = '';
		private $nDetCroId = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nDesId($nDesId){
			$this->nDesId = $nDesId;
		}
		function set_nDesResponsable($nDesResponsable){
			$this->nDesResponsable = $nDesResponsable;
		}
		function set_nDesDuracion($nDesDuracion){
			$this->nDesDuracion = $nDesDuracion;
		}
		function set_nDesEstado($nDesEstado){
			$this->nDesEstado = $nDesEstado;
		}
		function set_tDesFechaRegistro($tDesFechaRegistro){
			$this->tDesFechaRegistro = $tDesFechaRegistro;
		}
		function set_nCluId($nCluId){
			$this->nCluId = $nCluId;
		}
		function set_nDetCroId($nDetCroId){
			$this->nDetCroId = $nDetCroId;
		}

		//FUNCIONES Get
		function get_nDesId(){
			return $this->nDesId;
		}
		function get_nDesResponsable(){
			return $this->nDesResponsable;
		}
		function get_nDesDuracion(){
			return $this->nDesDuracion;
		}
		function get_nDesEstado(){
			return $this->nDesEstado;
		}
		function get_tDesFechaRegistro(){
			return $this->tDesFechaRegistro;
		}
		function get_nCluId(){
			return $this->nCluId;
		}
		function get_nDetCroId(){
			return $this->nDetCroId;
		}
		//Obtener Objeto DESPACHO
		function get_ObjDespacho($CAMPO){
			$query = $this->db->query("SELECT * FROM DESPACHO WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>