<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Menu_model extends CI_Model {
		//Atributos de Clase
		private $nMenId = '';
		private $nModId = '';
		private $cMenMenu = '';
		private $cMenUrl = '';
		private $cMenOrden = '';
		private $cMenActivo = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nMenId($nMenId){
			$this->nMenId = $nMenId;
		}
		function set_nModId($nModId){
			$this->nModId = $nModId;
		}
		function set_cMenMenu($cMenMenu){
			$this->cMenMenu = $cMenMenu;
		}
		function set_cMenUrl($cMenUrl){
			$this->cMenUrl = $cMenUrl;
		}
		function set_cMenOrden($cMenOrden){
			$this->cMenOrden = $cMenOrden;
		}
		function set_cMenActivo($cMenActivo){
			$this->cMenActivo = $cMenActivo;
		}

		//FUNCIONES Get
		function get_nMenId(){
			return $this->nMenId;
		}
		function get_nModId(){
			return $this->nModId;
		}
		function get_cMenMenu(){
			return $this->cMenMenu;
		}
		function get_cMenUrl(){
			return $this->cMenUrl;
		}
		function get_cMenOrden(){
			return $this->cMenOrden;
		}
		function get_cMenActivo(){
			return $this->cMenActivo;
		}
		//Obtener Objeto MENU
		function get_ObjMenu($CAMPO){
			$query = $this->db->query("SELECT * FROM MENU WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>