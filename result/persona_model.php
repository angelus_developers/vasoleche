<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Persona_model extends CI_Model {
		//Atributos de Clase
		private $nPerId = '';
		private $cPerNombres = '';
		private $cPerApellidoPaterno = '';
		private $cPerApellidoMaterno = '';
		private $cPerDni = '';
		private $cPerDireccion = '';
		private $cPerTelefono = '';
		private $cPerCelular = '';
		private $cPerEstado = '';
		private $tPerFechaRegistro = '';
		private $tPerFechaBaja = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nPerId($nPerId){
			$this->nPerId = $nPerId;
		}
		function set_cPerNombres($cPerNombres){
			$this->cPerNombres = $cPerNombres;
		}
		function set_cPerApellidoPaterno($cPerApellidoPaterno){
			$this->cPerApellidoPaterno = $cPerApellidoPaterno;
		}
		function set_cPerApellidoMaterno($cPerApellidoMaterno){
			$this->cPerApellidoMaterno = $cPerApellidoMaterno;
		}
		function set_cPerDni($cPerDni){
			$this->cPerDni = $cPerDni;
		}
		function set_cPerDireccion($cPerDireccion){
			$this->cPerDireccion = $cPerDireccion;
		}
		function set_cPerTelefono($cPerTelefono){
			$this->cPerTelefono = $cPerTelefono;
		}
		function set_cPerCelular($cPerCelular){
			$this->cPerCelular = $cPerCelular;
		}
		function set_cPerEstado($cPerEstado){
			$this->cPerEstado = $cPerEstado;
		}
		function set_tPerFechaRegistro($tPerFechaRegistro){
			$this->tPerFechaRegistro = $tPerFechaRegistro;
		}
		function set_tPerFechaBaja($tPerFechaBaja){
			$this->tPerFechaBaja = $tPerFechaBaja;
		}

		//FUNCIONES Get
		function get_nPerId(){
			return $this->nPerId;
		}
		function get_cPerNombres(){
			return $this->cPerNombres;
		}
		function get_cPerApellidoPaterno(){
			return $this->cPerApellidoPaterno;
		}
		function get_cPerApellidoMaterno(){
			return $this->cPerApellidoMaterno;
		}
		function get_cPerDni(){
			return $this->cPerDni;
		}
		function get_cPerDireccion(){
			return $this->cPerDireccion;
		}
		function get_cPerTelefono(){
			return $this->cPerTelefono;
		}
		function get_cPerCelular(){
			return $this->cPerCelular;
		}
		function get_cPerEstado(){
			return $this->cPerEstado;
		}
		function get_tPerFechaRegistro(){
			return $this->tPerFechaRegistro;
		}
		function get_tPerFechaBaja(){
			return $this->tPerFechaBaja;
		}
		//Obtener Objeto PERSONA
		function get_ObjPersona($CAMPO){
			$query = $this->db->query("SELECT * FROM PERSONA WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>