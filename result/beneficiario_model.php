<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Beneficiario_model extends CI_Model {
		//Atributos de Clase
		private $nBenId = '';
		private $cBenPartidaNacimiento = '';
		private $persona_nPerId = '';
		private $cBenSexo = '';
		private $cBenFechaNacimiento = '';
		private $cBenEstado = '';
		private $nPerIdMadre = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nBenId($nBenId){
			$this->nBenId = $nBenId;
		}
		function set_cBenPartidaNacimiento($cBenPartidaNacimiento){
			$this->cBenPartidaNacimiento = $cBenPartidaNacimiento;
		}
		function set_persona_nPerId($persona_nPerId){
			$this->persona_nPerId = $persona_nPerId;
		}
		function set_cBenSexo($cBenSexo){
			$this->cBenSexo = $cBenSexo;
		}
		function set_cBenFechaNacimiento($cBenFechaNacimiento){
			$this->cBenFechaNacimiento = $cBenFechaNacimiento;
		}
		function set_cBenEstado($cBenEstado){
			$this->cBenEstado = $cBenEstado;
		}
		function set_nPerIdMadre($nPerIdMadre){
			$this->nPerIdMadre = $nPerIdMadre;
		}

		//FUNCIONES Get
		function get_nBenId(){
			return $this->nBenId;
		}
		function get_cBenPartidaNacimiento(){
			return $this->cBenPartidaNacimiento;
		}
		function get_persona_nPerId(){
			return $this->persona_nPerId;
		}
		function get_cBenSexo(){
			return $this->cBenSexo;
		}
		function get_cBenFechaNacimiento(){
			return $this->cBenFechaNacimiento;
		}
		function get_cBenEstado(){
			return $this->cBenEstado;
		}
		function get_nPerIdMadre(){
			return $this->nPerIdMadre;
		}
		//Obtener Objeto BENEFICIARIO
		function get_ObjBeneficiario($CAMPO){
			$query = $this->db->query("SELECT * FROM BENEFICIARIO WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>