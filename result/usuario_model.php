<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Usuario_model extends CI_Model {
		//Atributos de Clase
		private $nUsuCodigo = '';
		private $nPerId = '';
		private $cUsuUsuario = '';
		private $cUsuClave = '';
		private $cUsuEstado = '';
		private $cUsuTipo = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nUsuCodigo($nUsuCodigo){
			$this->nUsuCodigo = $nUsuCodigo;
		}
		function set_nPerId($nPerId){
			$this->nPerId = $nPerId;
		}
		function set_cUsuUsuario($cUsuUsuario){
			$this->cUsuUsuario = $cUsuUsuario;
		}
		function set_cUsuClave($cUsuClave){
			$this->cUsuClave = $cUsuClave;
		}
		function set_cUsuEstado($cUsuEstado){
			$this->cUsuEstado = $cUsuEstado;
		}
		function set_cUsuTipo($cUsuTipo){
			$this->cUsuTipo = $cUsuTipo;
		}

		//FUNCIONES Get
		function get_nUsuCodigo(){
			return $this->nUsuCodigo;
		}
		function get_nPerId(){
			return $this->nPerId;
		}
		function get_cUsuUsuario(){
			return $this->cUsuUsuario;
		}
		function get_cUsuClave(){
			return $this->cUsuClave;
		}
		function get_cUsuEstado(){
			return $this->cUsuEstado;
		}
		function get_cUsuTipo(){
			return $this->cUsuTipo;
		}
		//Obtener Objeto USUARIO
		function get_ObjUsuario($CAMPO){
			$query = $this->db->query("SELECT * FROM USUARIO WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>