<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*Autogenered Developed by @jvinceso*/
/* Date : 01-05-2013 19:17:17 */
	class Insumos_model extends CI_Model {
		//Atributos de Clase
		private $nImsId = '';
		private $cImsNombre = '';
		private $nImsUnidadMedida = '';
		private $tImsFechaRegistro = '';
		private $tImsFechaVencimiento = '';
		private $cImsEstado = '';
		private $nImsTipo = '';
		private $cImsDescripcion = '';

		//Constructor de Clase
		function __construct(){
			parent::__construct();
		}

		//FUNCIONES Set
		function set_nImsId($nImsId){
			$this->nImsId = $nImsId;
		}
		function set_cImsNombre($cImsNombre){
			$this->cImsNombre = $cImsNombre;
		}
		function set_nImsUnidadMedida($nImsUnidadMedida){
			$this->nImsUnidadMedida = $nImsUnidadMedida;
		}
		function set_tImsFechaRegistro($tImsFechaRegistro){
			$this->tImsFechaRegistro = $tImsFechaRegistro;
		}
		function set_tImsFechaVencimiento($tImsFechaVencimiento){
			$this->tImsFechaVencimiento = $tImsFechaVencimiento;
		}
		function set_cImsEstado($cImsEstado){
			$this->cImsEstado = $cImsEstado;
		}
		function set_nImsTipo($nImsTipo){
			$this->nImsTipo = $nImsTipo;
		}
		function set_cImsDescripcion($cImsDescripcion){
			$this->cImsDescripcion = $cImsDescripcion;
		}

		//FUNCIONES Get
		function get_nImsId(){
			return $this->nImsId;
		}
		function get_cImsNombre(){
			return $this->cImsNombre;
		}
		function get_nImsUnidadMedida(){
			return $this->nImsUnidadMedida;
		}
		function get_tImsFechaRegistro(){
			return $this->tImsFechaRegistro;
		}
		function get_tImsFechaVencimiento(){
			return $this->tImsFechaVencimiento;
		}
		function get_cImsEstado(){
			return $this->cImsEstado;
		}
		function get_nImsTipo(){
			return $this->nImsTipo;
		}
		function get_cImsDescripcion(){
			return $this->cImsDescripcion;
		}
		//Obtener Objeto INSUMOS
		function get_ObjInsumos($CAMPO){
			$query = $this->db->query("SELECT * FROM INSUMOS WHERE CAMPO=?", array($CAMPO));
			if ($query->num_rows() > 0){
				$row = $query->row();
				//CREANDO EL OBJETO
			}
		}
	}
?>