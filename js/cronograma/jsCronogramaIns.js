$(function(){
	$("#btn_generar_cronograma").bind({
		click:function(evt){
			evt.preventDefault();
			// alert("generamos");
			switch( $("#cbo_insumos option:selected").val() ){
				case "x":
				case "":
				case undefined:
					alert("No es un Insumo Valido");
				break;
				default:
					var insumoid = $("#cbo_insumos option:selected").val();
					msgLoading("#c_cbo_insumos");
					$.ajax({
						url:'cronograma/generarCronogramaIns',
						type:'POST',
						cache:false,
						data:{
							insum:insumoid
						},success:function(data){
							$("#grvListadetalle").html(data);
							recargarcombo();
						},error:function(er){
							console.log(er);
						}
					});
				break;
			}
		}
	});
});
function recargarcombo(){
	$.ajax({
		url:'cronograma/comboGet',
		type:'POST',
		cache:false,
		success:function(data){
			$("#c_cbo_insumos").html(data);
		},error:function(er){
			console.log(er);
		}
	});
}