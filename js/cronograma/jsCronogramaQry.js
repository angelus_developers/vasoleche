$(function(){
	$.fn.editable.defaults.mode = 'inline';
	$('#qry_detalle_crono_body a').editable({
		url: 'cronograma/updateDetalle',
		datepicker:{
			language:'es'
		},
		validate: function(value) {
			if($.trim(value) == '') return 'Este Campo es Obligatorio';
		},
		success: function(response, newValue) {
			if(!response.success) cargarListaCronogramas();
		}
	});
});
$(function(){
	var dataTable = {tabla   : "qry_detalle_crono",filas   : 12,JQueryUI : true};
	paginaDataTable(dataTable);
});