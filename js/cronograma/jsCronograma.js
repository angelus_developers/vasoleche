$(function(){
	$(".optEditarCrono").bind({
		click:function(evt){
			evt.preventDefault();
			var tr = $(this).parents('tr');
			var codInsumo = $(tr).data("codins");
			var codcrono = $(tr).data("crono");
			verDetalleCronograma(codcrono,codInsumo);
		}
	});
	$(".optEliminarCrono").bind({
		click:function(evt){
			evt.preventDefault();
		}
	});
});
$(function(){
	var dataTable = {tabla   : "qry_crono",filas:10,JQueryUI : true};
	paginaDataTable(dataTable);
});

function verDetalleCronograma(codCronograma,codInsumo){
	$.ajax({
		url:'cronograma/verDetalleCronograma',
		type:'post',
		cache:false,
		data:{
			codCronograma : codCronograma,
			codInsumo : codInsumo
		},success:function(data){
			$("#modaldetacrono_body").html(data);
			$("#modaldetacrono").modal('show');
		},error:function(er){
			console.log(er);
		}
	});
}