// j=jQuery.noConflict();
$(function() {
    //alert("hey");

    $('#frmComiteUpd').validate({
        //click: function(){
        submitHandler: function(){
            var hdnidComite_upd           = $('#hdnidComite_upd').val();
            var nTipoComiteUpd            = $('#nTipoComiteUpd').val();
            var nTipoClubUpd              = $('#nTipoClubUpd').val();
            var nTipoPersonaUpd           = $('#nTipoPersonaUpd').val();
            var randomnumber              = Math.random()*11;
            //console.log("estado "+nEOIdEstadoObra);
            if(nTipoComiteUpd!='' &&  nTipoClubUpd!='' &&  nTipoPersonaUpd!='' && hdnidComite_upd!=''){
                //msgLoadSave("#msjinserta","#btn_ins_obra");
                //$.post("crearNotasPrensa/crearNotasIns",{//get
                $.post("comite/updComite",{//get
                    opcion                  :'INS',
                    hdnidComite_upd         : hdnidComite_upd,
                    nTipoComiteUpd          : nTipoComiteUpd,
                    nTipoClubUpd            :nTipoClubUpd,
                    nTipoPersonaUpd         :nTipoPersonaUpd,
                    randomnumber            :randomnumber
                }, function(data){
                    //$("#msjClub").html(data);
                    console.log(data);
                    if(data==1){
                        //alert("exito");
                        //mensaje("Operacion realizada con exito.","e");
                        mensaje("En hora buena registro correcto",'e');
                        $(".popedit").dialog("close");
                        cargaComiteListado();
                    }else{
                        alert("error");
                    }
                /*if (data==0){
                        msgLoadSaveRemove("#btn_ins_obra");
                        mensaje("Comuniquese con el administrador!!.","r");
                    }else{
                        msgLoadSaveRemove("#btn_ins_obra");                                                        
                        limpiarForm("#NuevaObra");   
                        mensaje("Operacion realizada con exito.","e");
                    }*/
                }); 
            }else{
                alert("Debe Ingresar todos los campos!!.","a");
                //mensaje("Debe Ingresar el nombre del club!!.","a");
            }   
        },
        rules: {
            nTipoComiteUpd: {
                required    : true
            }
        },
        messages: {
            nTipoComiteUpd: {
                required    : "Seleccione relacion con el club."
            }
        }
    });
});