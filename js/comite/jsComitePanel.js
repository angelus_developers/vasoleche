$(function(){
    $("#tabqry").bind({
        click: function(){
            $.ajax({
                type: "POST",
                //url: "club/qryClub",
                url: "comite/qryComite",
                cache: false,
                success: function(data) {
                    $("#profile").html(data);
                },
                error: function() { 
                    alert("error");
                }              
            });
        }
    });
})
function cargaComiteListado(){
    msgLoading("#profile","Cargando");
    $.ajax({
        type: "POST",
        url: "comite/qryComite",
        cache: false,
        success: function(data) {
            $("#profile").html(data);
        },
        error: function() { 
            alert("error");
        }              
    });
}

function eliminarComite(ncodigo){
    var rdn=Math.random()*11;
    //$.post(ruta+'moduloIndicadorObjEstrategicosc/QuitaIndicadorObjEstrategico.jsp', {
    $.post('comite/eliminarComite', {
        rdn:rdn,
        ncodigo:ncodigo
    }, function(data){
        if (data=="1"){
           cargaComiteListado();
           //alert("Operacion Exitosa");
           mensaje("Se actualizo correctamente",'e');
           //mensaje("Operacion realizada con exito.","e");
        }else{
            alert("Error");
        }
    });
}