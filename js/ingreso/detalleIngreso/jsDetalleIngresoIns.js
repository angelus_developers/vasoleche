// j=jQuery.noConflict();
$(function() {
    //alert("hey");
    $('#frmDetalleIngresoIns').validate({
        submitHandler:function(){
            // alert("DFSDF");
            //console.log("estado "+nEOIdEstadoObra);
            var idIngreso_ins              = $("#idIngreso_ins").val();
            var nTipoInsumo_ins             = $("#nTipoInsumo_ins").val();
            var txtCantidadIngreso_ins     = $("#txtCantidadIngreso_ins").val();
            var txtObservacionesIngreso_ins = $("#txtObservacionesIngreso_ins").val();
            $.ajax({
                //url:'despacho/registrarIns',
                url:'ingreso/registrarDetalleIngresoIns',
                type:'post',
                data:{
                    idIngreso_ins               :idIngreso_ins,
                    nTipoInsumo_ins             :nTipoInsumo_ins,
                    txtCantidadIngreso_ins     :txtCantidadIngreso_ins,
                    txtObservacionesIngreso_ins:txtObservacionesIngreso_ins
                },//me captura todos los datos del formulario
                cache:false,
                success:function(data){
                    console.log(data);
                    if (data==1) {
                        //alert("exito");
                        cleanForm("#frmDetalleIngresoIns");
                        mensaje("Se actualizo el stock",'e');
                        listarDetalleIngresoxIdIngreso(idIngreso_ins);
 
                    }  else if(data=="inconveniente"){
                        alert("Ha ocurrido un inconveniente comuniquese con el administrador");
                    //mensaje("WTF!!!!!",'r');
                    } else{
                        alert("Ha ocurrido un inconveniente comuniquese con el administrador");
                    }
                },
                error:function(error){
                    alert(error);
                }
            });
        },
        rules:{
            nTipoInsumo_ins:{
                required:true
            },
            txtCantidadIngreso_ins:{
                required:true,
                digits:true,
                min:"1"
            },
            txtObservacionesDespacho_ins:{
                required:true
            }
        },
        messages: {
            nTipoInsumo_ins: {
                required    : "Seleccione Insumo."
            },
            txtCantidadIngreso_ins: {
                required    : "Ingrese Cantidad.",
                digits: "solo numeros",
                min:"no alcanza"
            },
            txtObservacionesIngreso_ins: {
                required    : "Ingrese Observaciones."
            }
        }
    // ,debug: true
    });
    
});
function listarDetalleIngresoxIdIngreso(idIngreso_ins){
    $.ajax({
        //url:'despacho/listarDetalleDespachoxIdDespacho',
        url:'ingreso/listarDetalleIngresoxIdIngreso',
        type:'post',
        data:{
            idIngreso_ins              :idIngreso_ins
        },//me captura todos los datos del formulario
        cache:false,
        success:function(data){
            //console.log(data);
            $("#listadoDetalleIngreso").html(data);
        },
        error:function(error){
            alert(error);
        }
    });
}
function eliminarDetalleIngresoQuita(ncodigo,cantidad,idinsumo){
    var rdn=Math.random()*11;
    var ruta= $("#ruta").val();
    console.log("quita");
    //$.post('despacho/eliminarDetalleDespacho', {
    $.post('ingreso/eliminarDetalleIngresoQuita', {
        rdn:rdn,
        ncodigo:ncodigo,
        cantidad:cantidad,
        idinsumo:idinsumo
    }, function(data){
        console.log(data);
        if (data=="1"){
            mensaje("Se actualizo correctamente",'e');
            var idIngreso_ins = $("#idIngreso_ins").val();
            listarDetalleIngresoxIdIngreso(idIngreso_ins);
            
        //mensaje("Operacion realizada con exito.","e");
        }else{
            alert("Error");
        }
    });
}
function eliminarDetalleIngresoAgrega(ncodigo,cantidad,idinsumo){
    var rdn=Math.random()*11;
    var ruta= $("#ruta").val();
    console.log("agrega");
    //$.post('despacho/eliminarDetalleDespacho', {
    $.post('ingreso/eliminarDetalleIngresoAgrega', {
        rdn:rdn,
        ncodigo:ncodigo,
        cantidad:cantidad,
        idinsumo:idinsumo
    }, function(data){
        if (data=="1"){
            mensaje("Se actualizo correctamente",'e');
            var idIngreso_ins = $("#idIngreso_ins").val();
            listarDetalleIngresoxIdIngreso(idIngreso_ins);
        //mensaje("Operacion realizada con exito.","e");
        }else{
            alert("Error");
        }
    });
}