$(function(){
    $("#tabqry").bind({
        click: function(){
            $.ajax({
                type: "POST",
                //url: "club/qryClub",
                url: "beneficiario/qryBeneficiario",
                cache: false,
                success: function(data) {
                    
                    $("#profile").html(data);
                /*if(data.trim()==1){
                        mensaje("Se ha eliminado la obra correctamente!","e");
                        cargarMetas();
                    }else{
                        mensaje("Error inesperado, no se ha podido eliminar la obra!","r");
                    } */       
                },
                error: function() { 
                    alert("error");
                }              
            });
        }
    });
})
function cargaBeneficiarioListado(){
    msgLoading("#profile","Cargando");
    $.ajax({
        type: "POST",
        url: "beneficiario/qryBeneficiario",
        cache: false,
        success: function(data) {
            $("#profile").html(data);
        },
        error: function() { 
            alert("error");
        }              
    });
}

function eliminarBeneficiario(ncodigo){
    var rdn=Math.random()*11;
    var ruta= $("#ruta").val();
    $.post('beneficiario/eliminarBeneficiario', {
        rdn:rdn,
        ncodigo:ncodigo
    }, function(data){
        if (data=="1"){
           mensaje("Se actualizo correctamente",'e');
           cargaBeneficiarioListado();
           
           cargarninoslimite();
        }else{
            alert("Error");
        }
    });
}

function cargarninoslimite(){
$.ajax({
        type: "POST",
        url: "beneficiario/cantEdadNinosLimite",
        cache: false,
        success: function(data) {
            $("#div_ninos_edad_limite").html(data);
        },
        error: function() { 
            alert("error");
        }              
    });
}