// j=jQuery.noConflict();
$(function() {
    //alert("hey");
    $('#frmDetalleDespachoIns').validate({
        submitHandler:function(){
            // alert("DFSDF");
            //console.log("estado "+nEOIdEstadoObra);
            var idDespacho_ins              = $("#idDespacho_ins").val();
            var nTipoInsumo_ins             = $("#nTipoInsumo_ins").val();
            var txtCantidadDespacho_ins     = $("#txtCantidadDespacho_ins").val();
            var txtObservacionesDespacho_ins= $("#txtObservacionesDespacho_ins").val();
            $.ajax({
                //url:'despacho/registrarIns',
                url:'despacho/registrarDetalleDespachoIns',
                type:'post',
                data:{
                    nTipoInsumo_ins             :nTipoInsumo_ins,
                    txtCantidadDespacho_ins     :txtCantidadDespacho_ins,
                    txtObservacionesDespacho_ins:txtObservacionesDespacho_ins,
                    idDespacho_ins              :idDespacho_ins
                },//me captura todos los datos del formulario
                cache:false,
                success:function(data){
                    console.log(data);
                    
                        
                    if (data==1) {
                        //alert("exito");
                        cleanForm("#frmDetalleDespachoIns");
                        mensaje("Se actualizo el stock",'e');
                        listarDetalleDespachoxIdDespacho(idDespacho_ins);
                    //limpiarForm("#frmDespachoIns");
                    //mensaje("En hora buena registro correcto",'e');
                    } else if(data=="insuficiente"){    
                        mensaje("Lo solicitado supera la cantidad actual",'a');
                    } else if(data=="inconveniente"){
                        alert("Ha ocurrido un inconveniente comuniquese con el administrador");
                    //mensaje("WTF!!!!!",'r');
                    } else{
                        alert("Ha ocurrido un inconveniente comuniquese con el administrador");
                    }
                },
                error:function(error){
                    alert(error);
                }
            });
        },
        rules:{
            nTipoInsumo_ins:{
                required:true
            },
            txtCantidadDespacho_ins:{
                required:true,
                digits:true,
                min:"1"
            },
            txtObservacionesDespacho_ins:{
                required:true
            }
        },
        messages: {
            nTipoInsumo_ins: {
                required    : "Seleccione Insumo."
            },
            txtCantidadDespacho_ins: {
                required    : "Ingrese Cantidad.",
                digits: "solo numeros",
                min:"no alcanza"
            },
            txtObservacionesDespacho_ins: {
                required    : "Ingrese Observaciones."
            }
        }
    // ,debug: true
    });
    
    
/*$('#frmDetalleDespachoIns').validate({
        //click: function(){
        submitHandler: function(){
            var nTipoInsumo_ins               = $('#nTipoInsumo_ins').val();
            var txtCantidadDespacho_ins        = $('#txtCantidadDespacho_ins').val();
            var txtObservacionesDespacho_ins   = $('#txtObservacionesDespacho_ins').val();
            var randomnumber                = Math.random()*11;
            //console.log("estado "+nEOIdEstadoObra);
            if(nTipoInsumo_ins!=''){
                //msgLoadSave("#msjinserta","#btn_ins_obra");
                //$.post("crearNotasPrensa/crearNotasIns",{//get
                $.post("despacho/registrarDetalleDespachoIns",{//get
                    opcion                            :'INS',
                    nTipoInsumo_ins                   :nTipoInsumo_ins,
                    txtCantidadDespacho_ins           :txtCantidadDespacho_ins,
                    txtObservacionesDespacho_ins      :txtObservacionesDespacho_ins,
                    randomnumber            :randomnumber
                }, function(data){
                    console.log(data);
                    $("#msjDetalleDespacho").html(data);
                    if(data==1){
                        //alert("exito");
                        mensaje("En hora buena registro correcto",'e');
                        //mensaje("Operacion realizada con exito.","e");
                        //limpiarForm("#frmClubIns");
                    }else{
                        alert("error");
                    }
                }); 
            }else{
                mensaje("Debe Ingresar el nombre del club!!.","a");
            }   
        },
        rules:{
            nTipoInsumo_ins:{
                required:true
            },
            txtCantidadDespacho_ins:{
                required:true
            },
            txtObservacionesDespacho_ins:{
                required:true
            }
        },
        messages: {
            nTipoInsumo_ins: {
                required    : "Seleccione Insumo."
            },
            txtCantidadDespacho_ins: {
                required    : "Ingrese Cantidad."
            },
            txtObservacionesDespacho_ins: {
                required    : "Ingrese Observaciones."
            }
        }
    });*/
});
function listarDetalleDespachoxIdDespacho(idDespacho_ins){
    $.ajax({
        //url:'despacho/registrarIns',
        url:'despacho/listarDetalleDespachoxIdDespacho',
        type:'post',
        data:{
            idDespacho_ins              :idDespacho_ins
        },//me captura todos los datos del formulario
        cache:false,
        success:function(data){
            //console.log(data);
            $("#listadoDetalleDespacho").html(data);
        },
        error:function(error){
            alert(error);
        }
    });
}
function eliminarDetalleDespacho(ncodigo){
    var rdn=Math.random()*11;
    var ruta= $("#ruta").val();
    //$.post('club/eliminarClub', {
    $.post('despacho/eliminarDetalleDespacho', {
        rdn:rdn,
        ncodigo:ncodigo
    }, function(data){
        if (data=="1"){
            mensaje("Se actualizo correctamente",'e');
            var idDespacho_ins = $("#idDespacho_ins").val();
            listarDetalleDespachoxIdDespacho(idDespacho_ins);
        //mensaje("Operacion realizada con exito.","e");
        }else{
            alert("Error");
        }
    });
}