$(function(){
	$("#btn_fndPerson").bind({
		click:function(evt){
			evt.preventDefault();
			dni = $("#txtcPerDni").val();
			if( dni !='' && dni.length==8 ){
				loadBuscarPersonas();
			}else{
				alert("ingresa un dni valido");
			}
		}
	});
	$("#btnRegistrarUsuario").bind({
		click:function(evt){
			evt.preventDefault();
			registrarUsuario();
		}
	})
});
function registrarUsuario(){
	$.ajax({
		url:'usuario/registrarIns',
		cache:false,
		type:'post',
		data:{
			txt_nPerId : $("#txt_nPerId").val(),
			txtUsuario : $("#txtUsuario").val(),
			txtClave : $("#txtClave").val(),
			cboTipoUser : $("#cboTipoUser option:selected").val()
		},success:function(data){
			cleanForm("#frmUsuarioIns");
			if (data=="1") {
				mensaje("En hora buena registro correcto",'e');
			} else{
				mensaje("Houston, Tenemos Problemas!!!!!",'r');
			}
		},error:function(er){
			console.log(er);
		}
	})
}
function loadBuscarPersonas(){
	$.ajax({
		url:'persona/buscarxDni',
		type:'post',
		cache:false,
		data:{
			dni:$("#txtcPerDni").val()
		},success:function(data){
			$("#infopersona").html(data);
		},error:function(er){
			console.log(er);
		}
	});
}