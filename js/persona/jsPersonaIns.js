$(function(){
    
    $('#frmPersonaInsa').validate({
        submitHandler:function(){
            // alert("DFSDF");
            $.ajax({
                url:'persona/registrarIns',
                type:'post',
                data:$("#frmPersonaInsa").serialize(),//me captura todos los datos del formulario
                cache:false,
                success:function(data){
                    cleanForm("#frmPersonaInsa");
                    if (data==1) {
                        //alert("exito");
                        mensaje("En hora buena registro correcto",'e');
                        limpiarForm("#frmPersonaInsa");
                        //mensaje("En hora buena registro correcto",'e');
                    } else{
                        alert("error");
                        //mensaje("WTF!!!!!",'r');
                    }
                },
                error:function(error){
                    alert(error);
                }
            });
        },
        rules:{
            txtcPerNombres:{
                required:true,
                minlength   : 5
            },
            txtcPerApellidoPaterno:{
                required:true,
                minlength   : 5
            },
            txtcPerDni:{
                required:true,
                minlength   : 8,
                digits      : true
            },
            txtcPerDireccion:{
                required:true
            }
        },
        messages: {
            txtcPerNombres: {
                required    : "Ingrese el Nombre de la Persona.",
                minlength   : "Minimo {0} caracteres."
            },
            txtcPerApellidoPaterno: {
                required    : "Ingrese Apellido Paterno.",
                minlength   : "Minimo {0} caracteres."
            },
            txtcPerDni: {
                required    : "Ingrese DNI.",
                minlength   : "Minimo {0} caracteres.",
                digits      : "Ingrese solo numeros."
            },
            txtcPerDireccion: {
                required    : "Ingrese Direccion."
            }
        }
    // ,debug: true
    });
});