$(function(){
    console.log("ddf");
    $('#frmPersonaUpd').validate({
        submitHandler:function(){
            // alert("DFSDF");
            $.ajax({
                //url:'persona/registrarIns',
                url:'persona/updPersona',
                type:'post',
                data:$("#frmPersonaUpd").serialize(),
                cache:false,
                success:function(data){
                    //cleanForm("#frmPersonaInsa");
                    if (data==1) {
                        //alert("exito");
                        mensaje("En hora buena registro correcto",'e');
                        $(".popedit").dialog("close");
                        cargaPersonaListado();
                        //limpiarForm("#frmPersonaUpd");
                        //mensaje("En hora buena registro correcto",'e');
                    } else{
                        alert("error");
                        //mensaje("WTF!!!!!",'r');
                    }
                },
                error:function(error){
                    alert(error);
                }
            });
        },
        rules:{
            txtcPerNombresUpd:{
                required:true,
                minlength   : 4
            },
            txtcPerApellidoPaternoUpd:{
                required:true,
                minlength   : 5
            },
            txtcPerDniUpd:{
                required:true,
                minlength   : 8,
                digits      : true
            },
            txtcPerDireccionUpd:{
                required:true
            }
        },
        messages: {
            txtcPerNombresUpd: {
                required    : "Ingrese el Nombre de la Persona.",
                minlength   : "Minimo {0} caracteres."
            },
            txtcPerApellidoPaternoUpd: {
                required    : "Ingrese Apellido Paterno.",
                minlength   : "Minimo {0} caracteres."
            },
            txtcPerDniUpd: {
                required    : "Ingrese DNI.",
                minlength   : "Minimo {0} caracteres.",
                digits      : "Ingrese solo numeros."
            },
            txtcPerDireccionUpd: {
                required    : "Ingrese Direccion."
            }
        }
    // ,debug: true
    });
});