// j=jQuery.noConflict();
$(function() {
    //alert("hey");

$('#frmClubUpd').validate({
        //click: function(){
        submitHandler: function(){
                var hdnidClub               = $('#hdnidClub_upd').val();
                var txtNombreClub               = $('#txtNombreClub_upd').val();
                var txtDireccionClub            = $('#txtDireccionClub_upd').val();
                var txtTelefonoClub             = $('#txtTelefonoClub_upd').val();
                var txtCantBeneficiariosClub    = $('#txtCantBeneficiariosClub_upd').val();
                var txtCantidadLeche            = $('#txtCantidadLeche_upd').val();
                var txtCantidadAvena            = $('#txtCantidadAvena_upd').val();
                var randomnumber                = Math.random()*11;
                //console.log("estado "+nEOIdEstadoObra);
            if(txtNombreClub!=''){
                //msgLoadSave("#msjinserta","#btn_ins_obra");
                //$.post("crearNotasPrensa/crearNotasIns",{//get
                $.post("club/updClub",{//get
                    opcion                  :'INS',
                    hdnidClub:               hdnidClub,
                    txtNombreClub           :txtNombreClub,
                    txtDireccionClub        :txtDireccionClub,
                    txtTelefonoClub         :txtTelefonoClub,
                    txtCantBeneficiariosClub:txtCantBeneficiariosClub,
                    txtCantidadLeche        :txtCantidadLeche,
                    txtCantidadAvena        :txtCantidadAvena,
                    randomnumber            :randomnumber
                }, function(data){
                    //$("#msjClub").html(data);
                    if(data==1){
                        //alert("exito");
                        mensaje("En hora buena registro correcto",'e');
                        //mensaje("Operacion realizada con exito.","e");
                        $(".popedit").dialog("close");
                        cargaClubListado();
                    }else{
                        alert("error");
                    }
                    /*if (data==0){
                        msgLoadSaveRemove("#btn_ins_obra");
                        mensaje("Comuniquese con el administrador!!.","r");
                    }else{
                        msgLoadSaveRemove("#btn_ins_obra");                                                        
                        limpiarForm("#NuevaObra");   
                        mensaje("Operacion realizada con exito.","e");
                    }*/
                }); 
            }else{
                mensaje("Debe Ingresar el nombre del club!!.","a");
            }   
        },
        rules: {
            txtNombreClub_upd: {
                required    : true,
                minlength   : 5
            },
            txtDireccionClub_upd: {
                required    : true,
                minlength   : 10
            },
            txtTelefonoClub_upd: {
                required    : true,
                minlength   : 6
            },
            txtCantBeneficiariosClub_upd: {
                required    : true,
                digits      : true
            },
            txtCantidadLeche_upd: {
                required    : true,
                digits      : true
            },
            txtCantidadAvena_upd: {
                required    : true,
                digits      : true
            }
        },
        messages: {
            txtNombreClub_upd: {
                required    : "Ingrese el Nombre del Club.",
                minlength   : "Minimo {0} caracteres."
            },
            txtDireccionClub_upd: {
                required    : "Ingrese la Direccion del Club.",
                minlength   : "Minimo {0} caracteres."
            },
            txtTelefonoClub_upd: {
                required    : "Ingrese Telefono del Club.",
                minlength   : "Minimo {0} caracteres."
            },
            txtCantBeneficiariosClub_upd: {
                required    : "Ingrese Cantidad de Beneficiarios.",
                digits      : "Ingrese solo numeros."
            },
            txtCantidadLeche_upd: {
                required    : "Ingrese Cantidad de Leche.",
                digits      : "Ingrese solo numeros."
            },
            txtCantidadAvena_upd: {
                required    : "Ingrese cantidad de Avena.",
                digits      : "Ingrese solo numeros."
            }
        }
    });
});