// j=jQuery.noConflict();
$(function() {
    //alert("hey");
    $('#frmClubIns').validate({
        //click: function(){
        submitHandler: function(){
            var txtNombreClub               = $('#txtNombreClub').val();
            var txtDireccionClub            = $('#txtDireccionClub').val();
            var txtTelefonoClub             = $('#txtTelefonoClub').val();
            var txtCantBeneficiariosClub    = $('#txtCantBeneficiariosClub').val();
            var txtCantidadLeche            = $('#txtCantidadLeche').val();
            var txtCantidadAvena            = $('#txtCantidadAvena').val();
            var randomnumber                = Math.random()*11;
            //console.log("estado "+nEOIdEstadoObra);
            if(txtNombreClub!=''){
                //msgLoadSave("#msjinserta","#btn_ins_obra");
                //$.post("crearNotasPrensa/crearNotasIns",{//get
                $.post("club/insClub",{//get
                    opcion                  :'INS',
                    txtNombreClub           :txtNombreClub,
                    txtDireccionClub        :txtDireccionClub,
                    txtTelefonoClub         :txtTelefonoClub,
                    txtCantBeneficiariosClub:txtCantBeneficiariosClub,
                    txtCantidadLeche        :txtCantidadLeche,
                    txtCantidadAvena        :txtCantidadAvena,
                    randomnumber            :randomnumber
                }, function(data){
                    //$("#msjClub").html(data);
                    if(data==1){
                        //alert("exito");
                        mensaje("En hora buena registro correcto",'e');
                        //mensaje("Operacion realizada con exito.","e");
                        limpiarForm("#frmClubIns");
                    }else{
                        alert("error");
                    }
                }); 
            }else{
                mensaje("Debe Ingresar el nombre del club!!.","a");
            }   
        },
        rules: {
            txtNombreClub: {
                required    : true,
                minlength   : 5
            },
            txtDireccionClub: {
                required    : true,
                minlength   : 10
            },
            txtTelefonoClub: {
                required    : true,
                minlength   : 6
            },
            txtCantBeneficiariosClub: {
                required    : true,
                digits      : true
            },
            txtCantidadLeche: {
                required    : true,
                digits      : true
            },
            txtCantidadAvena: {
                required    : true,
                digits      : true
            }
        },
        messages: {
            txtNombreClub: {
                required    : "Ingrese el Nombre del Club.",
                minlength   : "Minimo {0} caracteres."
            },
            txtDireccionClub: {
                required    : "Ingrese la Direccion del Club.",
                minlength   : "Minimo {0} caracteres."
            },
            txtTelefonoClub: {
                required    : "Ingrese Telefono del Club.",
                minlength   : "Minimo {0} caracteres."
            },
            txtCantBeneficiariosClub: {
                required    : "Ingrese Cantidad de Beneficiarios.",
                digits      : "Ingrese solo numeros."
            },
            txtCantidadLeche: {
                required    : "Ingrese Cantidad de Leche.",
                digits      : "Ingrese solo numeros."
            },
            txtCantidadAvena: {
                required    : "Ingrese cantidad de Avena.",
                digits      : "Ingrese solo numeros."
            }
        }
    });
});