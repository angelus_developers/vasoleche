// j=jQuery.noConflict();
$(function() {
    //alert("hey");
    $('#frmInsumoIns').validate({
        submitHandler:function(){
            // alert("DFSDF");
            $.ajax({
                url:'insumo/registrarIns',
                type:'post',
                data:$("#frmInsumoIns").serialize(),//me captura todos los datos del formulario
                cache:false,
                success:function(data){
                    cleanForm("#frmInsumoIns");
                    if (data==1) {
                        //alert("exito");
                        mensaje("En hora buena registro correcto",'e');
                        limpiarForm("#frmInsumoIns");
                        //mensaje("En hora buena registro correcto",'e');
                    } else{
                        alert("error");
                        //mensaje("WTF!!!!!",'r');
                    }
                },
                error:function(error){
                    alert(error);
                }
            });
        },
        rules:{
            txtNombreInsumo:{
                required:true,
                minlength   : 3
            },
            txtUnidadInsumo:{
                required:true
            },
            txtFechaVencimientoInsumo:{
                required:true
            },
            txtDescripcionInsumo:{
                required:true
            },
            txtCantidadInsumo:{
                required:true
            }
        },
        messages: {
            txtNombreInsumo: {
                required    : "Ingrese el Nombre de la Persona.",
                minlength   : "Minimo {0} caracteres."
            },
            txtUnidadInsumo: {
                required    : "Ingrese Unidad de Medida."
            },
            txtFechaVencimientoInsumo: {
                required    : "Ingrese Fecha de Vencimiento."
            },
            txtDescripcionInsumo: {
                required    : "Ingrese Descripcion."
            },
            txtCantidadInsumo: {
                required    : "Ingrese Cantidad."
            }
        }
    // ,debug: true
    });
});